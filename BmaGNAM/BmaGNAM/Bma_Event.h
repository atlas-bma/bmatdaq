// data holder structure

#ifndef _BMA_Event_
#define _BMA_Event_

#include <vector>

typedef struct BMA_data_s{

  int tubeId;
  std::vector <double> waveform;
  unsigned int time;
} BMA_data;


class BMA_Event {

 private:

  int m_bcid;

  std::vector<BMA_data> m_BoardData;

 public:
 
  BMA_Event() {};
  ~BMA_Event() {};

  void ResetEvent();
  void SetBcid(int bcid);

  void SetBMAdata(int iele, std::vector<double> const& value, unsigned int time);

  int GetBcid() const;

  BMA_data GetBMAdata(int iele) const;

  unsigned int GetNumberOfBMAdata() const;
};

#endif
