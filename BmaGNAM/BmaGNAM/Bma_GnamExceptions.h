#ifndef BMA_GNAMEXCEPTIONS_H
#define BMA_GNAMEXCEPTIONS_H

#include "ers/ers.h"

//#include "gnam/gnamutils/GnamLibException.h"

namespace daq {

  ERS_DECLARE_ISSUE( bmagnam,
                     Exception,
		     ,
		     )


  ERS_DECLARE_ISSUE_BASE( bmagnam,
                          AnyError,
                          bmagnam::Exception,
                          message,
                          ,
                          ((const char *) message))
} 
#ifndef ERS_DEBUG

  #define ERS_DEBUG(N,A) \
   { \
    std::stringstream _ss_; _ss_ << A; \
    daq::bmagnam::AnyError issue(ERS_HERE, _ss_.str().c_str()); \
    ers::debug (issue, N); \
   }
#endif // ERS_DEBUG

#ifndef ERS_INFO
  #define ERS_INFO(A) \
   { \
    std::stringstream _ss_; _ss_ << A; \
    daq::bmagnam::AnyError issue(ERS_HERE, _ss_.str().c_str()); \
    ers::info (issue); \
   }
#endif // ERS_INFO

#ifndef ERS_WARNING
  #define ERS_WARNING(A) \
   { \
    std::stringstream _ss_; _ss_ << A; \
    daq::bmagnam::AnyError issue(ERS_HERE, _ss_.str().c_str()); \
    ers::warning (issue); \
   }
#endif // ERS_WARNING

#ifndef ERS_ERROR
  #define ERS_ERROR(A) \
   { \
    std::stringstream _ss_; _ss_ << A; \
    daq::bmagnam::AnyError issue(ERS_HERE, _ss_.str().c_str()); \
    ers::error (issue); \
   }
#endif // ERS_ERROR

#ifndef ERS_FATAL
  #define ERS_FATAL(A) \
   { \
    std::stringstream _ss_; _ss_ << A; \
    daq::bmagnam::AnyError issue(ERS_HERE, _ss_.str().c_str()); \
    ers::fatal (issue); \
   }
#endif // ERS_FATAL


#endif // BMA_GNAMEXCEPTIONS_H
