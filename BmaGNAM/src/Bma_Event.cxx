#include "BmaGNAM/Bma_Event.h"
#include "BmaGNAM/Bma_GnamExceptions.h"
// GNAM: global include file for libraries
#include "gnam/gnamutils/GnamUtils.h"

void BMA_Event::ResetEvent() {

  m_bcid = -1;

  m_BoardData.clear(); // container for waveform from PMT-Type Lucrod board
}

void BMA_Event::SetBcid(int bcid) { m_bcid = bcid; }

void BMA_Event::SetBMAdata(int chId, std::vector<double> const& values, unsigned int time) {

  BMA_data aChannel;

  aChannel.tubeId   = chId;
  aChannel.waveform = values;
  aChannel.time     = time;

  m_BoardData.push_back(aChannel);
}
BMA_data BMA_Event::GetBMAdata(int it) const { return m_BoardData[it]; }

unsigned int BMA_Event::GetNumberOfBMAdata() const { return m_BoardData.size(); }

int BMA_Event::GetBcid() const { return m_bcid; }
