// Mod Log
// C. Sbarra August 2022
// C. Sbarra 
// Adapt to BMA 
//           
#include <algorithm>
#include <numeric>
#include <bitset>
// IS
#include "is/info.h"
#include "is/infodictionary.h"
#include "is/infodocument.h"
#include "is/inforeceiver.h"
#include "is/infodynany.h"

// mutex
#include <boost/thread/mutex.hpp>
 
// My CLASS
#include "BmaGNAM/Bma_Event.h"
#include "BmaGNAM/Bma_GnamExceptions.h"

#include "DFDebug/DFDebug.h"

// GNAM Histogram filling
#include <fstream>
#include <stdlib.h>
#include <iostream>

// GNAM: global include file for libraries
#include "gnam/gnamutils/GnamUtils.h"
#include "gnam/gnamutils/GnamPAO.h"
#include "gnam/gnamutils/GnamPAO-def.h"
// MY DAL: 
#include "BMAGnamdal/BMAGnamLibrary.h"

// TDAQ: OKS DB entry point
#include "config/Configuration.h"

#include <stdlib.h>
#include <errno.h>

#define LUCROD_NCHANNELS 16

using namespace std;

namespace {

  std::vector <std::string>  m_BoardInputLabel;
  std::vector <unsigned int> m_BoardGroupIndeces;
 
  std::string const GROUPNAME[3] = {"Group1", "Group2", "Empty"}; 

  int const Group1 = 0;
  int const Group2 = 1;
  int const Empty  = 2;

  double const maxBaseSpread = 150;
}

////////////////
// struct
///////////////
struct ShapeInfo {

  double baseline;
  double baseSpread;
  double amplitude;
  double amplitudeFixBase;
  double time;
  double charge1bcid;
  double charge1bcidFixBase;
  double charge3bcid;
  double charge3bcidFixBase;
  double min;
};

////////////////
// functions
///////////////

ShapeInfo    GetShapeInfo     (std::vector<double> const& shape, int idx);
double       GetInt           (std::vector<double> const& shape, int start, int stop, double base);
unsigned int GetEnabledChannel(std::vector<unsigned int> const& GroupIndex, std::string runType);

void DeleteHistos(); 
void FillPhysHistos (const BMA_Event* bma); 
void BookHistos();
void BookPhysHistos();

void CallBack_ISInfoBeam1 (ISCallbackInfo* isc);
void CallBack_ISInfoBeam2 (ISCallbackInfo* isc);
void CallBack_ISInfoSBFlag(ISCallbackInfo* isc);


////////////////
// histograms //
////////////////

static GnamHisto* hCollTimeOffset[LUCROD_NCHANNELS];
static GnamHisto* hCollShape     [LUCROD_NCHANNELS];
static GnamHisto* hCollCharge1   [LUCROD_NCHANNELS];
static GnamHisto* hCollCharge3   [LUCROD_NCHANNELS];
static GnamHisto* hCollAmplitude [LUCROD_NCHANNELS];
static GnamHisto* hCollSignalTime[LUCROD_NCHANNELS];
static GnamHisto* hCollBaseline  [LUCROD_NCHANNELS];
static GnamHisto* hCollBaseRMS   [LUCROD_NCHANNELS];

static GnamHisto* hTimeOffsetFirst   [LUCROD_NCHANNELS];
static GnamHisto* hTimeOffsetSB      [LUCROD_NCHANNELS];
static GnamHisto* hTimeOffset        [LUCROD_NCHANNELS];
static GnamHisto* hBcid              [LUCROD_NCHANNELS];
static GnamHisto* hShape             [LUCROD_NCHANNELS];
static GnamHisto* hCharge1           [LUCROD_NCHANNELS];
static GnamHisto* hCharge3           [LUCROD_NCHANNELS];
static GnamHisto* hAmplitude         [LUCROD_NCHANNELS];
static GnamHisto* hSignalTime        [LUCROD_NCHANNELS];
static GnamHisto* hBaseline          [LUCROD_NCHANNELS];
static GnamHisto* hBaseRMS           [LUCROD_NCHANNELS];
static GnamHisto* hAmpliToChargeRatio[LUCROD_NCHANNELS];

static GnamHisto* hNdata;
static GnamHisto* hBCID;

static std::vector<GnamHisto*> *histolist = NULL;

static std::string  m_bma_run_type;
static int          m_bma_run_number;
static unsigned int m_BoardInputMask = 0;

static int stableBeamFlag = 0;
static std::vector<unsigned int> pairedBunchList;
static std::vector<unsigned int> firstBunchList;
static std::vector<unsigned int> BunchListOR;

// FIXED BASELINES 
double fixedBaseSideA[LUCROD_NCHANNELS] = {0.,0.,0.,0.,0.,0.,0.,0.};
double fixedBaseSideC[LUCROD_NCHANNELS] = {0.,0.,0.,0.,0.,0.,0.,0.};

ISInfoReceiver* isInfoRec = 0; 
boost::mutex my_mutex, my_ORmutex;
std::bitset<3564> beam1, beam2, paired_old, present_old;
bool gotBeam1, gotBeam2;

/////////////////////////////////
// Initialization              //
/////////////////////////////////

extern "C" void initDB(Configuration* confDB, const gnamdal::GnamLibrary* library)
{
  ERS_LOG("Bma_Histo:initDB start");
 
  const BMAGnamdal::BMAGnamLibrary* lib = 
    confDB->cast<BMAGnamdal::BMAGnamLibrary, 
		 gnamdal::GnamLibrary>(library);
  
  if (lib) {

    m_BoardInputLabel   = lib->get_BoardInputNames();
    m_BoardGroupIndeces = lib->get_BoardGroupIndex();
  }
  else {
    
    //cannot initialize!!!
    ERS_REPORT_IMPL(ers::error, ers::Message, "Cannot get conf DB. Exiting",);
    exit(1);
  }  
   
  std::ostringstream msg; 

  msg.str(""); msg << "Input labels: ";
  
  for (unsigned int i=0; i<m_BoardInputLabel.size(); ++i)
    msg << m_BoardInputLabel[i] << " ";

  ERS_INFO(msg.str());
  
  msg.str(""); msg << "Input indeces: ";
  
  for (unsigned int i=0; i<m_BoardGroupIndeces.size(); ++i)
    msg << m_BoardGroupIndeces[i] << " ";
  
  ERS_INFO(msg.str());
}

/////////////////////////////////
// startOfRun                  //
// GNAM entry point startOfRun //
/////////////////////////////////

extern "C" const std::vector<GnamHisto*> * startOfRun(int run, std::string type)
{
  ERS_LOG("startOfRun: Entered");

  m_bma_run_type   = type; // Physics
  m_bma_run_number = run;
  
  ERS_LOG("startOfRun: " << run << " type: " << type);  
  
  m_BoardInputMask = GetEnabledChannel(m_BoardGroupIndeces, m_bma_run_type);
  
  std::bitset<16> BoardInputMask(m_BoardInputMask);
  
  ERS_INFO("Enabled channel mask for LUCROD-BMA board: " << BoardInputMask);
  
  // reset bitset
  present_old.reset();
  paired_old.reset();

  std::string ISInfo_NameA = "LHC.BPTXDaq.BCID_BPTX1_Bunches";
  std::string ISInfo_NameC = "LHC.BPTXDaq.BCID_BPTX2_Bunches";
  std::string ISInfoSBFlag = "LHC.StableBeamsFlag";
  
  ERS_LOG("Subscribing to: " << ISInfo_NameA);
  ERS_LOG("Subscribing to: " << ISInfo_NameC);
  ERS_LOG("Subscribing to: " << ISInfoSBFlag);
  
  isInfoRec = new ISInfoReceiver("initial");
  ISInfoDictionary dict("initial");
  
  try {
    isInfoRec->subscribe(ISInfo_NameA, CallBack_ISInfoBeam1);
    isInfoRec->subscribe(ISInfo_NameC, CallBack_ISInfoBeam2);
    isInfoRec->subscribe(ISInfoSBFlag, CallBack_ISInfoSBFlag);
  }
  catch (daq::is::Exception & ex) {
    ERS_REPORT_IMPL(ers::warning, ers::Message, "Error subscribing to IS",);
  }
  
  if (histolist != NULL) return histolist;
  else                   histolist = new std::vector<GnamHisto*>;
  
  BookHistos();  
  
  ERS_LOG("histolist size: " << histolist->size());
  
  return histolist;
}

////////////////////////////////
// fillHisto                  //
// GNAM entry point fillHisto //
////////////////////////////////

extern "C" void fillHisto(void)
{
  static const BMA_Event* bma = NULL;
  
  GNAM_BRANCH_FILL("BMA_Event", BMA_Event, bma);
  
  if (bma == NULL) { ERS_WARNING("Filling of BMA_Event branch for BMA failed"); return; }

  FillPhysHistos(bma);
}

/* **************** *
 *    prePublish    *
 * **************** */
/* *********************************************************************** *
 * Performs special operations right before the publication of histos; for *
 * instance: fitting procedures.                                           *
 * *********************************************************************** */
extern "C" void prePublish(bool endOfRun) {}

/* ******************* *
 *    customCommand    *
 * ******************* */
/* ****************************************** *
 * Executes custom commands received from OH. *
 * ****************************************** */
extern "C" void customCommand(const GnamHisto* histo, int argc, const char* const* argv) {}

/* *************** *
 *    endOfRun     *
 * *************** */
extern "C" void endOfRun (void)
{
  if (m_bma_run_type == "Physics") {
    
    if (isInfoRec != 0) delete isInfoRec;
    isInfoRec = 0;
  }
  
  ERS_LOG("Bma_Histo:endOfRun start");
  
  // cleanup  
  ERS_ASSERT (histolist != NULL);

  ERS_LOG("deleting histos. histolist size: " << histolist->size());

  for (unsigned int i=0; i<histolist->size(); ++i)
    delete (*histolist)[i];

  delete histolist;
  histolist = NULL;
  
  ERS_LOG("setting all pointers to NULL");
  
  for (int i=0; i<LUCROD_NCHANNELS; i++) {
    
    hBaseline       [i] = NULL;
    hBaseRMS        [i] = NULL;
    hShape          [i] = NULL;
    hCharge1        [i] = NULL;
    hCharge3        [i] = NULL;
    hAmplitude      [i] = NULL;
    hSignalTime     [i] = NULL;
    hTimeOffset     [i] = NULL;
    hTimeOffsetSB   [i] = NULL;
    hTimeOffsetFirst[i] = NULL;
    hBcid           [i] = NULL;

    hCollBaseline  [i]= NULL;
    hCollBaseRMS   [i]= NULL;
    hCollShape     [i]= NULL;
    hCollCharge1   [i]= NULL;
    hCollCharge3   [i]= NULL;
    hCollAmplitude [i]= NULL;
    hCollSignalTime[i]= NULL;
    hCollTimeOffset[i]= NULL;
  }

  hNdata = NULL;
  hBCID  = NULL;

  ERS_LOG("Lucrod_Histo:endOfRun stop");
}

/* ****************************************** */
unsigned int GetEnabledChannel(std::vector<unsigned int> const& GroupIndeces, std::string runType)
/* ****************************************** */
{
  unsigned int enabledChannelMask = 0;
  unsigned int one = 1; 
  
  for (unsigned int i=0; i<GroupIndeces.size(); ++i)
    if (GroupIndeces[i] == Group1 || GroupIndeces[i] == Group2)
      enabledChannelMask |= (one << i);
  
  return enabledChannelMask;
}

/* ****************************************** */
void BookPhysHistos()
/* ****************************************** */
{
  ERS_LOG("BookPhysHistos");

  std::string basename;
  std::string plot;
  std::string name;
  std::string title;
      
  for (int k=0; k<LUCROD_NCHANNELS; ++k) {
    
    if (((m_BoardInputMask>>k)&1) == 1) {

      // events triggered in BCIDs different from the ones with collisions

      basename = "/SHIFT/BMA/" + GROUPNAME[m_BoardGroupIndeces[k]] + "/" + m_BoardInputLabel[k] + "/";

      plot  = "Shape";
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hShape[k] = new GnamHisto(name.c_str(), title.c_str(), 64, 0.5, 64.5, "PAO1" , GnamHisto::FLOATBIN);      
      hShape[k]->GetXaxis()->SetTitle("Signal shape (ADC counts)");
      histolist->push_back(hShape[k]);
      
      plot  = "Charge1bcid";
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hCharge1[k] = new GnamHisto(name.c_str(), title.c_str(), 1000, 0., 24000., "PAO1" , GnamHisto::FLOATBIN);      
      hCharge1[k]->GetXaxis()->SetTitle("Charge (ADC counts*ns)");
      histolist->push_back(hCharge1[k]);
      
      plot  = "Charge3bcid";
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hCharge3[k] = new GnamHisto(name.c_str(), title.c_str(), 1000, 0., 24000., "PAO1" , GnamHisto::FLOATBIN);      
      hCharge3[k]->GetXaxis()->SetTitle("Charge (ADC counts*ns)");
      histolist->push_back(hCharge3[k]);
      
      plot  = "Baseline";
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hBaseline[k] = new GnamHisto(name.c_str(), title.c_str(), 501, -0.5, 500.5, "PAO1" , GnamHisto::FLOATBIN);      
      hBaseline[k]->GetXaxis()->SetTitle("Baseline (ADC counts)");
      histolist->push_back(hBaseline[k]);
      
      plot  = "BaselineSpread";
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hBaseRMS[k] = new GnamHisto(name.c_str(), title.c_str(), 501, -0.5, 500.5, "PAO1" , GnamHisto::FLOATBIN);      
      hBaseRMS[k]->GetXaxis()->SetTitle("Baseline spread (ADC counts)");
      histolist->push_back(hBaseRMS[k]);

      plot  = "Amplitude";
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hAmplitude[k] = new GnamHisto(name.c_str(), title.c_str(), 2001, -0.25, 4000.25, "PAO1" , GnamHisto::FLOATBIN);      
      hAmplitude[k]->GetXaxis()->SetTitle("Amplitude (ADC counts)");
      histolist->push_back(hAmplitude[k]);

      plot  = "AmpliToChargeRatio";
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hAmpliToChargeRatio[k] = new GnamHisto(name.c_str(), title.c_str(), 301, -0.005, 0.3005, "PAO1" , GnamHisto::FLOATBIN);
      hAmpliToChargeRatio[k]->GetXaxis()->SetTitle("Amplitude/Charge");
      histolist->push_back(hAmpliToChargeRatio[k]);
      
      plot  = "SignalTime";
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hSignalTime[k] = new GnamHisto(name.c_str(), title.c_str(), 64, -0.5, 63.5, "PAO1" , GnamHisto::FLOATBIN);      
      hSignalTime[k]->GetXaxis()->SetTitle("Sample (3.125 ns)");
      histolist->push_back(hSignalTime[k]);

      plot  = "BCID"; // Triggered BCID 
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hBcid[k] = new GnamHisto(name.c_str(), title.c_str(), 3564, -0.5, 3563.5, "PAO1" , GnamHisto::FLOATBIN);      
      hBcid[k]->GetXaxis()->SetTitle("BCID number");
      histolist->push_back(hBcid[k]);
      
      plot  = "TimeOffset"; // BCID boundary Time offset
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hTimeOffset[k] = new GnamHisto(name.c_str(), title.c_str(), 8, -0.5, 7.5, "PAO1" , GnamHisto::FLOATBIN);      
      hTimeOffset[k]->GetXaxis()->SetTitle("Sample from BCID boundary (3.125 ns)");
      histolist->push_back(hTimeOffset[k]);

      // Signals triggered in colliding BCID

      plot  = "CollisionShape";
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hCollShape[k] = new GnamHisto(name.c_str(), title.c_str(), 64, 0.5, 64.5, "PAO1" , GnamHisto::FLOATBIN);      
      hCollShape[k]->GetXaxis()->SetTitle("Signal shape (ADC counts)");
      histolist->push_back(hCollShape[k]);
       
      plot  = "CollisionCharge1bcid";
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hCollCharge1[k] = new GnamHisto(name.c_str(), title.c_str(), 1000, 0., 24000., "PAO1" , GnamHisto::FLOATBIN);  
      hCollCharge1[k]->GetXaxis()->SetTitle("Charge (ADC counts*ns)");
      histolist->push_back(hCollCharge1[k]);
 
      plot  = "CollisionCharge3bcid";
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hCollCharge3[k] = new GnamHisto(name.c_str(), title.c_str(), 1000, 0., 23000., "PAO1" , GnamHisto::FLOATBIN);  
      hCollCharge3[k]->GetXaxis()->SetTitle("Charge (ADC counts*ns)");
      histolist->push_back(hCollCharge3[k]);
 
      plot  = "CollisionBaseline";
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hCollBaseline[k] = new GnamHisto(name.c_str(), title.c_str(), 501, -0.5, 500.5, "PAO1" , GnamHisto::FLOATBIN);    
      hCollBaseline[k]->GetXaxis()->SetTitle("Baseline (ADC counts)");
      histolist->push_back(hCollBaseline[k]);

      plot  = "CollisionBaselineSpread";
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hCollBaseRMS[k] = new GnamHisto(name.c_str(), title.c_str(), 501, -0.5, 500.5, "PAO1" , GnamHisto::FLOATBIN);   
      hCollBaseRMS[k]->GetXaxis()->SetTitle("Baseline spread (ADC counts)");
      histolist->push_back(hCollBaseRMS[k]);

      plot  = "CollisionAmplitude";
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hCollAmplitude[k] = new GnamHisto(name.c_str(), title.c_str(), 2001, -0.25, 4000.25,  "PAO1" , GnamHisto::FLOATBIN);      
      hCollAmplitude[k]->GetXaxis()->SetTitle("Amplitude (ADC counts)");
      histolist->push_back(hCollAmplitude[k]);
 
      plot  = "CollisionSignalTime";
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hCollSignalTime[k] = new GnamHisto(name.c_str(), title.c_str(), 64, -0.5, 63.5, "PAO1" , GnamHisto::FLOATBIN);      
      hCollSignalTime[k]->GetXaxis()->SetTitle("Sample (3.125 ns)");
      histolist->push_back(hCollSignalTime[k]);

      plot  = "CollisionTimeOffset"; // BCID boundary Time offset
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hCollTimeOffset[k] = new GnamHisto(name.c_str(), title.c_str(), 8, -0.5, 7.5, "PAO1" , GnamHisto::FLOATBIN);      
      hCollTimeOffset[k]->GetXaxis()->SetTitle("Sample from BCID boundary (3.125 ns)");
      histolist->push_back(hCollTimeOffset[k]);

      plot  = "CollisionTimeOffsetInStableBeam"; // BCID boundary Time offset AND stable beam
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hTimeOffsetSB[k] = new GnamHisto(name.c_str(), title.c_str(), 8, -0.5, 7.5, "PAO1" , GnamHisto::FLOATBIN);      
      hTimeOffsetSB[k]->GetXaxis()->SetTitle("Sample from BCID boundary (3.125 ns)");
      histolist->push_back(hTimeOffsetSB[k]);
      
      plot  = "CollisionTimeOffsetInFilleFirstdBCID"; // BCID boundary Time offset AND filled BCID
      name  = basename + plot; 
      title = m_BoardInputLabel[k] + "_" + plot;
      hTimeOffsetFirst[k] = new GnamHisto(name.c_str(), title.c_str(), 8, -0.5, 7.5, "PAO1" , GnamHisto::FLOATBIN);      
      hTimeOffsetFirst[k]->GetXaxis()->SetTitle("Sample from BCID boundary (3.125 ns)");
      histolist->push_back(hTimeOffsetFirst[k]);
    }      
  }
  
  // just in case we decide to trigger on a given BCID instead of self-triggering
  
  hBCID = new GnamHisto("/SHIFT/BMA/MonitoredBCID", "BMA_BCIDMonitored", 3564, -0.5, 3563.5, "PAO1", GnamHisto::FLOATBIN);
  histolist->push_back(hBCID);
}

/* ****************************************** */
void BookCommonHistos()
/* ****************************************** */
{
  ERS_LOG("BookCommonHistos");

  hNdata = new GnamHisto("/SHIFT/BMA/Ndata", "BMA_NchFound", 10, -0.5, 9.5, "PAO1" , GnamHisto::FLOATBIN);
  histolist->push_back(hNdata);
}

/* ****************************************** */
void BookHistos()
/* ****************************************** */  
{
  ERS_LOG("BookHistos");
  
  BookPhysHistos();  
  BookCommonHistos();
}

/* ****************************************** */
void FillPhysHistos(const BMA_Event* bma)
/* ****************************************** */
{
 
  int       bcidBMA   = bma->GetBcid();
  unsigned int nBMA   = bma->GetNumberOfBMAdata();

  if (hBCID  != NULL) hBCID->Fill(bcidBMA);
  if (hNdata != NULL) hNdata->Fill(nBMA);

  for (unsigned int ch=0; ch<nBMA; ch++) {
    
    BMA_data aBMAdata = bma->GetBMAdata(ch);

    int idx = aBMAdata.tubeId;    
    // self-trigger BCID
    unsigned int tresholdTime = aBMAdata.time - 4; 
    unsigned int bcid = tresholdTime/8;
    unsigned int timeOffset = tresholdTime%8;
  
    if (bcidBMA == -1) 
	bcid = (bcid > 5) ? (bcid-6) : (3563-6+bcid); // correct for fixed offset
    else
	bcid = bcidBMA;   

    if (hBcid[idx] != NULL) hBcid[idx]->Fill(bcid);     
        
    std::vector<double> shape = aBMAdata.waveform;   
    
    ShapeInfo info = GetShapeInfo(shape, idx);

    boost::mutex::scoped_lock l(my_mutex); // avoid the list is changed while in use here

    if (std::binary_search(pairedBunchList.begin(), pairedBunchList.end(), bcid)) {
      
      if (hCollBaseline  [idx] != NULL) hCollBaseline  [idx]->Fill(info.baseline);
      if (hCollBaseRMS   [idx] != NULL) hCollBaseRMS   [idx]->Fill(info.baseSpread);
      if (hCollAmplitude [idx] != NULL) hCollAmplitude [idx]->Fill(info.amplitude);
      if (hCollSignalTime[idx] != NULL) hCollSignalTime[idx]->Fill(info.time);
      if (hCollTimeOffset[idx] != NULL) hCollTimeOffset[idx]->Fill(timeOffset);     

      if (hCollCharge1[idx] != NULL) 
	if (info.baseSpread < maxBaseSpread)
	  hCollCharge1[idx]->Fill(info.charge1bcid);
      
      if (hCollCharge3[idx] != NULL) 
	if (info.baseSpread < maxBaseSpread)
	  hCollCharge3[idx]->Fill(info.charge3bcid);
      
      if (hTimeOffsetSB[idx] != NULL)
	if (stableBeamFlag != 0)
	  hTimeOffsetSB[idx]->Fill(timeOffset);
      
      if (std::binary_search(firstBunchList.begin(), firstBunchList.end(), bcid))
	if (hTimeOffsetFirst[idx] != NULL)
	  hTimeOffsetFirst[idx]->Fill(timeOffset);
      
      if (hCollShape[idx] != NULL)
	for (unsigned int i=0; i<shape.size(); ++i)
	  hCollShape[idx]->SetBinContent(i+1, shape[i]); 
    }
    else {
      
      if (hAmpliToChargeRatio[idx] != NULL)
	if (info.charge1bcid != 0) 
	  hAmpliToChargeRatio[idx]->Fill(info.amplitude/info.charge1bcid);
      
      if (hBaseline  [idx] != NULL) hBaseline  [idx]->Fill(info.baseline);
      if (hBaseRMS   [idx] != NULL) hBaseRMS   [idx]->Fill(info.baseSpread);
      if (hAmplitude [idx] != NULL) hAmplitude [idx]->Fill(info.amplitude);
      if (hSignalTime[idx] != NULL) hSignalTime[idx]->Fill(info.time);
      if (hTimeOffset[idx] != NULL) hTimeOffset[idx]->Fill(timeOffset);      

      if (hCharge1[idx] != NULL)
	if (info.baseSpread < maxBaseSpread)
	  hCharge1[idx]->Fill(info.charge1bcid);
      
      if (hCharge3[idx] != NULL)
	if (info.baseSpread < maxBaseSpread)
	  hCharge3[idx]->Fill(info.charge3bcid);
      
      if (hShape[idx] != NULL) 
	for (unsigned int i=0; i<shape.size(); ++i)
	  hShape[idx]->SetBinContent(i+1, shape[i]); 
    }
  }  
}

///* ****************************************** */
double GetInt(std::vector<double> const& shape, int start, int stop, double base)
///* ****************************************** */
{
  // returns integral with simple bin sums in ADC counts*ns units
  double charge = 0; 

  for (int i=start; i< stop; ++i) {

    charge += shape[i];
    
  }
  
  return 3.125*(charge - base*(stop-start));
}

///* ****************************************** */
ShapeInfo GetShapeInfo(std::vector<double> const& shape, int idx)
///* ****************************************** */
{
  ShapeInfo result;

  result.baseline           = 0;
  result.baseSpread         = 0;
  result.amplitude          = 0;
  result.amplitudeFixBase   = 0;
  result.charge1bcid        = 0;
  result.charge1bcidFixBase = 0;
  result.charge3bcid        = 0;
  result.charge3bcidFixBase = 0;
  result.min                = 0;

  unsigned int vLength = (unsigned int)shape.size();

  if (vLength < 64) { ERS_LOG("*** found uncomplete shape of size " << vLength); return result; }
    
  // this waveform baseline   
  unsigned int baseStart=0, baseSize=16;
  unsigned int baseStop = baseSize;
  
  double basemax = 0;
  double basemin = 4096;
  double base    = 0;
  double base2   = 0;

  for (u_int i=baseStart; i<baseStop; ++i) {

    if (shape[i] < basemin) basemin = shape[i];
    if (shape[i] > basemax) basemax = shape[i];

    base  += shape[i];
    base2 += shape[i]*shape[i];
  }

  base /= baseSize;

  result.baseline   = base;  
  result.baseSpread = basemax-basemin; 
 
  // MAX Signal time, amplitude and charge (calibration) or selected BCID one (physics) 
  int    maxTime      = 0;
  double maxAmplitude = 0.;

  unsigned int sigStart = 0;
  unsigned int sigEnd   = vLength;

  //  if (m_bma_run_type.compare("Physics") == 0) { // look for the maximum around the triggered BCID range
  //    sigStart = 20;
  //    sigEnd   = 35;    
  //  }    

  for (unsigned int i=sigStart; i<sigEnd; ++i) {

    double amplitude = shape[i];

    if (amplitude > maxAmplitude) {

      maxAmplitude = amplitude;
      maxTime      = i;
    }
    
  }
  
  result.amplitude        = maxAmplitude - base;
  result.time             = maxTime;
  
  // charge: gate around maximum (unless outside range - which should NOT happen)
  int start1, stop1, start3, stop3;

  start1 = maxTime - 2;
  stop1  = maxTime + 6;

  if ((stop1 > 64) || (start1 < 0) ) { // no signal, take 8 samples

    start1 = 25;
    stop1  = 33;
  }

  start3 = maxTime -  7;
  stop3  = maxTime + 17;

  if ((stop3 > 64) || (start3 < 0)) { // no signal, take 3 bcid

    start3 = 20;
    stop3  = 44;
  }

  result.charge1bcid = GetInt(shape, start1, stop1, base); // charge 1 bcid(real signals)  
  result.charge3bcid = GetInt(shape, start3, stop3, base); // charge 3 bcid 
  
  return result;
}

/************************************/
void Combine()
/************************************/
{
  std::bitset<3564> paired = beam1 & beam2;
 
  if (paired.any()) {

    if (paired != paired_old) {

      boost::mutex::scoped_lock l(my_mutex);

      pairedBunchList.clear();
      firstBunchList.clear();
      
      if (paired.test(0)) firstBunchList.push_back(0); // if filled, bcid 0 is also a first-in-train/isolated one

      for (unsigned int i=0; i<3500; ++i) {

	if (paired.test(i))                      pairedBunchList.push_back(i);
	if (paired.test(i+1) && !paired.test(i)) firstBunchList.push_back(i+1);
      }

      paired_old = paired;    
    }
  }

  std::bitset<3564> present = beam1 | beam2;

  if (present.any()) {
    
    if (present != present_old) {

      ERS_LOG("BEAM IS PRESENT in " << present.count() << "BCIDs");

      boost::mutex::scoped_lock l(my_ORmutex);

      BunchListOR.clear();

      for (unsigned int i=0; i<present.size(); ++i) {

	if (present.test(i))
	  BunchListOR.push_back(i);
      }
      
      present_old = present;
    }
  }
  
  gotBeam1 = false;
  gotBeam2 = false;
}

/************************************/
void CallBack_ISInfoBeam1(ISCallbackInfo* isc)
/************************************/
{
  if (isc->reason() != is::Deleted) {

    ISInfoDynAny isa;
    isc->value(isa);

    std::vector<float> b = isa.getAttributeValue<std::vector<float> >(6);

    for (unsigned int i=1; i<b.size()-1; ++i)
      if (b[i] != 0)
	beam1.set(i-1);

    gotBeam1 = true;

    if (gotBeam2) Combine();
  }
  else return;
}

/************************************/
void CallBack_ISInfoBeam2(ISCallbackInfo* isc) 
/************************************/
{
  if (isc->reason() != is::Deleted) {

    ISInfoDynAny isa;
    isc->value(isa);
    
    std::vector<float> b = isa.getAttributeValue<std::vector<float> >(6);
    
    for (unsigned int i=1; i<b.size()-1; ++i)
      if (b[i] != 0)
	beam2.set(i-1);

    gotBeam2 = true;
    
    if (gotBeam1) Combine();
  }
  else return;
}

/************************************/
void CallBack_ISInfoSBFlag(ISCallbackInfo* isc) 
/************************************/
{
  if (isc->reason() != is::Deleted) {

    ISInfoDynAny isa;
    isc->value(isa);
    
    int attr_number = (int)isa.getAttributesNumber();
    
    std::cout << " number of attributes in IS info " << attr_number << std::endl;
    
    for (int i= 0; i<attr_number; ++i) {
      
      std::cout << " attribute type: "  << isa.getAttributeType(i) << "\n";
      std::cout << " attribute array? " << isa.isAttributeArray(i) << "\n";
    }
    
    stableBeamFlag = isa.getAttributeValue<int>(1);
  }
  else return;
}
