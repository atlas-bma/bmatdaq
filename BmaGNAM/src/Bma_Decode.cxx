// GNAM
//#define GNAM_DEBUG
#define DEBUG

#include <time.h>
#include <vector>
#include <stdint.h>
#include <stdlib.h>

#include "BmaGNAM/Bma_Event.h"
#include "BmaGNAM/Bma_GnamExceptions.h"

// GNAM includes 
#include "gnam/gnamutils/GnamDynalibs.h"
#include "gnam/gnamutils/GnamBranches.h"
#include "gnam/gnamutils/GnamUtils.h"
// GNAM: DB object corresponding to my library
#include "BMAGnamdal/BMAGnamLibrary.h"

// TDAQ: OKS DB entry point
#include "config/Configuration.h"

// TDAQ: for DB object casting
#include "dal/util.h"

using namespace std;
static unsigned int const event_header_marker = 0x1234cccc; //Signifies the start of an event
static unsigned int const rod_header_marker   = 0xee1234ee; //Signifies the start of a ROD Header
static unsigned int const rod_header_size     = 9;          //Number of dwords in the header, including the header marker
static unsigned int const scope_marker        = 0xabcd;     //Marker for self-triggered events
static unsigned int const wave_marker         = 0xabce;     //Marker for fixed-BCID events

static unsigned int ROD_START_OF_HEADER_MARKER    = 0xee1234ee;
static unsigned int ROD_HEADER_SIZE               = 9;
static unsigned int ROD_TRAILER_SIZE              = 3;
static unsigned int ROD_NUMBER_OF_STATUS_ELEMENTS = 4;
static unsigned int ROD_STATUS_BLOCK_POSITION     = 1;
static unsigned int ROD_FORMAT_VERSION_NUMBER     = 0x03010000;

unsigned int GetDataBlockAddress  (const uint32_t* rod, uint32_t size);
unsigned int GetStatusBlockAddress(const uint32_t* rod, uint32_t size);
unsigned int CheckROD             (const uint32_t* rod, uint32_t size);
uint32_t     ReadBmaData       (const uint32_t* rod, uint32_t size, uint32_t index);

static BMA_Event* bma = NULL;

extern "C" void initDB(Configuration* confDB, const gnamdal::GnamLibrary* library) {

  ERS_INFO("BMA_Decode:initDB start");

  const BMAGnamdal::BMAGnamLibrary* lib = 
    confDB->cast<BMAGnamdal::BMAGnamLibrary, 
		 gnamdal::GnamLibrary>(library);
  
  if (lib) {
    
    ROD_START_OF_HEADER_MARKER    = lib->get_RODStartOfHeaderMarker();
    ROD_HEADER_SIZE               = lib->get_RODHeaderSize();
    ROD_TRAILER_SIZE              = lib->get_RODTrailerSize();
    ROD_NUMBER_OF_STATUS_ELEMENTS = lib->get_RODNumberOfStatusElements();
    ROD_STATUS_BLOCK_POSITION     = lib->get_RODStatusBlockPosition();
    ROD_FORMAT_VERSION_NUMBER     = lib->get_RODFormatVersionNumber();
  }
  else { ERS_INFO("Initialization of LUCROD_Decode library failed"); }
  
  bma = new BMA_Event();

  try {
    GNAM_BRANCH_REGISTER("BMA_Event", bma);
  }
  catch(daq::gnamlib::AlreadyExistingBranch &exc) {
    throw daq::gnamlib::CannotRegisterBranch (ERS_HERE, "BMA_Event");
  }
}

/////////////////////////////
// decode                  //
// GNAM entry point decode //
// called at RUNNING state //
/////////////////////////////
extern "C" void decode(const std::vector<uint32_t const *> *rods,
		       const std::vector<unsigned long int> *sizes,
		       const uint32_t *event, unsigned long int event_size) {
  
  const uint32_t *rod;
  uint32_t rod_size;
 
  bma->ResetEvent();

  uint32_t NumOfRods = rods->size();
  
  //ERS_LOG("Decode: the number of RODs is " << NumOfRods);

  for (uint32_t i=0; i<NumOfRods; ++i) {

    rod = (*rods)[i];
    rod_size = (unsigned int)(*sizes)[i];

    unsigned int rod_errors = CheckROD(rod, rod_size);
  
    if ((rod_errors & 0xffff0000)!=0)
      ERS_WARNING("ROD fragmet has problems, but data consistency not affected -> will be processed");
    
    if ((rod_errors & 0x0000ffff)!=0) {
      
      ERS_ERROR("ROD fragment has errors and data consistency is affected -> Event will be skipped");
      // add a flag in the event definition to know if the event is good or not, 
      // to be checked by the Histo application
      return;
    }
    
    // find out beginning of data elements and number of Lucrod boards (== number of status words)
    
    int number_of_status_elements = (int)rod[rod_size-ROD_TRAILER_SIZE];

    //ERS_LOG("Decode: the number of status elements is " << number_of_status_elements);

    uint32_t data_start_address = GetDataBlockAddress(rod, rod_size);
    
    int j=0;
    
    uint32_t data_address = data_start_address;

    while (j < number_of_status_elements) { // decode data block
      
      u_int value = rod[data_address]; data_address++;    
      
      if ((value>>16) != 0x82) { ERS_WARNING("ROD fragmet has problems. Expecting 0x8200XX and found 0x" << hex << value); }
      else {
	
	//cout << " got 0x" << hex << value << dec << endl;
	
	data_address = ReadBmaData(rod, rod_size, data_address);
      }
      
      ++j;
    }
  }
}

//////////////////////////
// end                  //
// GNAM entry point end //
//////////////////////////
extern "C" void end (void) {

  ERS_INFO("BMA_Decode::end start");
  delete bma;
  ERS_INFO("BMA_Decode::end stop");
}

/////////////////////////////////////////////////////////
//           BmaBlockData                           //
/////////////////////////////////////////////////////////
uint32_t ReadBmaData(const uint32_t* rod, uint32_t size, uint32_t index) {
  
  uint32_t address = index;
  
  //Lucrod header: trigger type
  uint32_t buf      = rod[address]; address++;  
  uint32_t dataType = buf;

  if (dataType == wave_marker) {

    // ERS_LOG("Bma_Decode:: decode: trigger type: fixed bcid");
    // First BCID stored in waveaff (missing in case of self-trigger)
    uint32_t bcid = rod[address]; address++;  

    bma->SetBcid((int32_t)bcid);
  }

  uint32_t nch = rod[address]; address++; // Number of enabled channels
  
  for (uint32_t i=0; i<nch; i++) { // loop on enabled channels
    
    uint32_t time     = 0;    
    uint32_t chHeader = rod[address]; address++; 

    if ((chHeader>>24) == 0xaa) {

      uint32_t chid = chHeader&0xf; // channel index

      std::vector<double> waveform;
   
      for (int i=0; i<64; ++i) { // fill 64-sample vector

	buf = rod[address]; address++;

	waveform.push_back(buf);
      }
      
      if (dataType != wave_marker) { // time information follow the waveform in self-triggered data

        buf  = rod[address]; address++;
        time = buf;
      }
      
      bma->SetBMAdata(chid, waveform, time);
    }
    else if ((chHeader>>24) == 0xff) { // end of data: some channel was missing...
      
      //ERS_WARNING("Missing data in channel " << i);
      
      return address;
    }
    else {
      
      ERS_WARNING("Something wrong. Found 0x" << hex << chHeader << dec << " when expecting 0xaa0000X");
      
      return address;
    }
  }
  
  buf = rod[address]; address++;
  
  if (buf != 0xff000000) { ERS_LOG("Bma_Decode:: readBmaData: error: trailer not found"); }
 
  return address;
}

/////////////////////////////////////////////////////////
//           GetBlockDataAddress                       //
/////////////////////////////////////////////////////////
unsigned int GetDataBlockAddress(const uint32_t *rod, uint32_t size) {
  
  unsigned int address;
  unsigned int header_size               = rod[1];
  unsigned int status_block_position     = rod[size-ROD_TRAILER_SIZE+2];
  unsigned int number_of_status_elements = rod[size-ROD_TRAILER_SIZE];

  if      (status_block_position == 1) address = header_size;
  else if (status_block_position == 0) address = header_size + number_of_status_elements;
  else                                 address = 0;
  
  if (address > size) address = 0;
  
  return address;
}

/////////////////////////////////////////////////////////
// CheckROD                                            //
// checks formatting and consistency of a ROD fragment //
/////////////////////////////////////////////////////////
unsigned int CheckROD(const uint32_t* rod, uint32_t size) {

  unsigned int errors = 0x00000000;

  // ROD header
  uint32_t start_of_header_marker = rod[0];
  uint32_t header_size            = rod[1];
  uint32_t format_version_number  = rod[2];

  // ROD trailer
  uint32_t number_of_status_elements = rod[size-ROD_TRAILER_SIZE];
  uint32_t number_of_data_elements   = rod[size-ROD_TRAILER_SIZE+1];
  uint32_t status_block_position     = rod[size-ROD_TRAILER_SIZE+2];

  // ROD fragment
  uint32_t fragment_size = header_size+number_of_data_elements+number_of_status_elements+ROD_TRAILER_SIZE; 
  
  if (start_of_header_marker != ROD_START_OF_HEADER_MARKER) {
    
    ERS_INFO("ROD start of header marker is: 0x" << hex << start_of_header_marker <<
	     ", but: 0x" << ROD_START_OF_HEADER_MARKER << dec << " is expected");
    
    errors = 0x00000001;
  }
  
  if (header_size != ROD_HEADER_SIZE) {
    
    ERS_INFO("ROD header size is: " << header_size << ", but: " << ROD_HEADER_SIZE << " is expected");
    
    errors = 0x00010000;
  }
  
  if (format_version_number != ROD_FORMAT_VERSION_NUMBER) {
    
    ERS_INFO("ROD format version number is: 0x" << hex << format_version_number <<
	     ", but: 0x" << ROD_FORMAT_VERSION_NUMBER << dec << " is expected");

    errors = 0x00020000;
  }

  if (status_block_position != ROD_STATUS_BLOCK_POSITION){
    
    ERS_INFO("ROD status block position is: " << status_block_position << ", but: " << ROD_STATUS_BLOCK_POSITION << " is expected");
    
    errors = 0x00400000;
  }
  
  if (status_block_position > 1) {

    ERS_INFO("ROD status block position is: " << status_block_position << ", but: 0 or 1 is expected");
    
    errors = 0x00000010;
  }
  
  if (size != fragment_size) {
    
    ERS_INFO("ROD fragment size is: " << size << ", but: " << fragment_size <<
	     " is expected => header size: " << header_size << " + number of data elements: " <<
	     number_of_data_elements << " + number of status elements: " << 
	     number_of_data_elements << " + trailer size: " << ROD_TRAILER_SIZE);
    
    errors = 0x00000020;
  }
  
  return errors;
}
