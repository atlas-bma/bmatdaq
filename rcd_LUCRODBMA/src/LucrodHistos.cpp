/********************************************************/
/*							*/
/*** C 2007 - The software with that certain something **/
/*							*/
/********************************************************/

#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include <ctime>
#include "ers/ers.h"
#include <cmath>
#include <cstring>

#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "DFSubSystemItem/ConfigException.h"
#include "ROSModules/ModulesException.h"

#include "rcd_LUCRODBMA/LucrodHistos.h"

#include <sys/types.h>
#include <sys/socket.h>

// Variable to be used for tracing and debugging, for example: 
enum { DFDB_RCDDETECTOR = 30000 };

using namespace ROS;
using namespace std;

namespace {

  int         const NBINS = 500; 
  std::string const GROUPNAME[3] = {"Group1", "Group2", "Empty"}; 
}

/**********************/
LucrodHistos::LucrodHistos(sampleCollection& collection):m_sampleCollection(collection) 
/**********************/ 
{
  m_oh_provider = nullptr;
}

/******************************************/
LucrodHistos::~LucrodHistos()
/*******************************************/
{
}

/***********************************************************************/
void LucrodHistos::setup(DFCountedPointer<Config> configuration, unsigned short chLumiMask, unsigned short chScopeMask, unsigned short fpgaMask) 
/************************************************************************/
{ 
  ERS_LOG("LucrodHistos:: setup entered");

  m_boardName               = configuration->getString("BoardName");
  m_lumiEnabledChannelMask  = chLumiMask;   
  m_scopeEnabledChannelMask = chScopeMask;   
  m_fpgaMask                = fpgaMask; 
  m_inputNames              = configuration->getVector<std::string>("InputNames");
  m_inputGroupIndeces       = configuration->getVector<unsigned int>("InputGroupIndeces");

  ERS_LOG("LucrodHistos:: setup done");
}

/**************************************/
void LucrodHistos::connect(OHRootProvider* oh_provider)
/**************************************/
{
  m_oh_provider = oh_provider;

  if (!m_oh_provider) ERS_REPORT_IMPL(ers::error, ers::Message, Form("LucrodHistos::%s::connect OH provider points to 0. Cannot publish", m_boardName.c_str()),);
}

/**************************************/
void LucrodHistos::prepareForRun()
/**************************************/
{
  resetLBHistos(); 
  resetTimeHistos(); 
  resetSoRHistos(); 
  resetAccumulators(); 
}

/*************************************************/
void  LucrodHistos::resetLBHistos()
/*************************************************/ 
{
  for (int i=0; i<NLUCRODIN; ++i)
    hChBcidHits[i]->Reset();
}

/************************************************/
void  LucrodHistos::resetTimeHistos()
/*************************************************/ 
{
  for (int i=0; i<NLUCRODIN; ++i)
    hChHits[i]->Reset();
  
  if (hOrbit) hOrbit->Reset();
}

/*************************************************/
void  LucrodHistos::resetSoRHistos()
/*************************************************/ 
{
  for (int i=0; i<NLUCRODIN; ++i) {

    hChBaseline[i]->Reset();    
    hChThresholds[i]->Reset();    
    hDataChannel[i]->Reset();    
  }
  
  hLBtime->Reset();
}

/****************************************************/
void LucrodHistos::resetAccumulators()
/****************************************************/
{
  //reset arrays for dataChannel histos

  m_sampleCollection.numberOfEvents = 0;

  for (int i=0; i<NLUCRODIN; i++) {
    for (int j=0; j<NHDATA; j++) {    

      m_sampleCollection.sum [i][j] = 0;
      m_sampleCollection.sum2[i][j] = 0;
    }
  }
}

/***********************************************************************/
void LucrodHistos::updateGroupIndeces(unsigned short chMask, unsigned short fpgaMask, std::vector<unsigned int> indeces) 
/************************************************************************/
{ 
  ERS_LOG(Form("LucrodHistos::%s::updateGroupIndeces", m_boardName.c_str()));

  m_lumiEnabledChannelMask = chMask;   
  m_fpgaMask               = fpgaMask; 
  m_inputGroupIndeces      = indeces;
}

/****************************************************/
std::unique_ptr<TH1D> LucrodHistos::bookBcidHisto(std::string hname, std::string hlabel)
/****************************************************/
{
  std::unique_ptr<TH1D> h(new TH1D(hname.c_str(), hname.c_str(), NBCID, -0.5, NBCID-0.5));

  h->GetXaxis()->SetTitle(hlabel.c_str());

  return h;
}

/****************************************************/
std::unique_ptr<TH1D> LucrodHistos::bookTimeHisto(std::string hname, std::string hlabel)
/****************************************************/
{
  std::unique_ptr<TH1D> h(new TH1D(hname.c_str(), hname.c_str(), NBINS, -0.5, NBINS-0.5));

  h->GetXaxis()->SetTitle(hlabel.c_str());

  return h;
}

/****************************************************/
void LucrodHistos::bookHistos()
/****************************************************/
{
  ERS_LOG(Form("LucrodHistos::%s::bookHistos", m_boardName.c_str()));

  for (int i=0; i<NLUCRODIN; ++i) {

    std::string const  pname = m_inputNames[i];
    std::ostringstream hname;
    std::string        hlabel;

    hname.str("");
    hname << "Channel_" << i << "_hits";
    hlabel = pname + " - BCID";
    
    hChBcidHits[i] = bookBcidHisto(hname.str(), hlabel);    

    hname.str("");
    hname << "Channel_ "<< i << "_hitsHistory";
    hlabel = pname + " - Lumi Block";
    
    hChHits[i] = bookTimeHisto(hname.str(), hlabel);

    hname.str(""); 
    hname << "Channel_" << i << "_meanShape";
    hlabel = pname + " - ADC counts/sample";   
    
    hDataChannel[i].reset(new TH1D(hname.str().c_str(), hname.str().c_str(), NHDATA, -0.5, NHDATA-0.5));
    hDataChannel[i]->GetXaxis()->SetTitle(hlabel.c_str());

    hname.str(""); 
    hname << "Channel_" << i << "_baseline";
    hlabel = pname + " - ADC counts";
    
    hChBaseline[i].reset(new TH1D(hname.str().c_str(), hname.str().c_str(), 501, -0.5, 500.5));
    hChBaseline[i]->GetXaxis()->SetTitle(hlabel.c_str());

    hname.str("");
    hname << "Channel_" << i << "_threshold";
    hlabel = pname + " - ADC counts";
    
    hChThresholds[i].reset(new TH1D(hname.str().c_str(), hname.str().c_str(), 501, -0.5, 500.5));
    hChThresholds[i]->GetXaxis()->SetTitle(hlabel.c_str());
  }
  
  hOrbit = bookTimeHisto("orbit_counts",       "Lumi Block");
  
  std::string hname = "LBHandleTime";

  hLBtime.reset(new TH1D(hname.c_str(), hname.c_str(), 2001, -999.5, 3000.5));

  hLBtime->GetXaxis()->SetTitle("time (ms)");  
}

/*************************************************************/
void LucrodHistos::fillOrbitHisto(int ibin, unsigned int orbit)
/*************************************************************/
{
  hOrbit->SetBinContent(ibin, orbit);
}

/*************************************************************/
void LucrodHistos::fillChLBHitHistos(int ich, std::array<uint32_t, NBCID> const & counts)
/************************************************************/
{
  int ihbin = 1;
  
  for (auto it=counts.begin(); it != counts.end(); ++it) {
    
    hChBcidHits[ich]->SetBinContent(ihbin, *it);
    
    ++ihbin;
  }
}

/*************************************************************/
void LucrodHistos::fillChTimeHitHistos(int ich, int ibin, long long totHits)
/************************************************************/
{
  hChHits[ich]->SetBinContent(ibin, totHits);
}

/*************************************************************/
void LucrodHistos::fillDataChannelHistos()
/************************************************************/
{
  float events = m_sampleCollection.numberOfEvents;

  for (int i=0; i<NLUCRODIN; i++) {
    for (int j=0; j<NHDATA; j++) {

      float value = m_sampleCollection.sum[i][j]/events;

      hDataChannel[i]->SetBinContent(j+1, value);
    }
  }
}

/*************************************************************/
void LucrodHistos::fillChBaselineHistos(int ich, double base)
/************************************************************/
{
  hChBaseline[ich]->Fill(base);
}

/*************************************************************/
void LucrodHistos::fillChThresholdHistos(int ich, double thresh)
/************************************************************/
{
  hChThresholds[ich]->Fill(thresh);
}

/*************************************************************/
void LucrodHistos::fillLBtimeHisto(double time)
/************************************************************/
{
  hLBtime->Fill(time);
}

/**********************************************************************/
void LucrodHistos::publishLBHistos(u_int lbid)
/************************************************************************/
{
  for (int ifpga=0; ifpga<NCHFPGA; ++ifpga) {

    int         ich   = getFpgaChannel(ifpga);
    std::string group = getFpgaGroup  (ifpga);

    if (!((m_lumiEnabledChannelMask >> ich) & 0x1)) continue; // check if the channel belongs to list of enabled channels
 
    try {     

      std::ostringstream name; name << "/SHIFT/" << group << "/BLOCK/HITS/ch_" << ich;
      
      m_oh_provider->publish(*hChBcidHits[ich], name.str().c_str(), lbid);
    }
    catch (daq::oh::Exception &e) {

      ostringstream text; text << "LucrodHistos::" << m_boardName << "::publishLBHistos ERROR publishing BCID-aware hit histos for LumiBlock" << lbid;

      ERS_REPORT_IMPL(ers::error, ers::Message, text.str(),);
    }    
  }
  
  try { // publish here even if for expert, because it is updated once per LB
     
    std::ostringstream name; name << "/EXPERT/LBHandleTime";
    
    m_oh_provider->publish(*hLBtime, name.str().c_str(), 0);
  }
  catch (daq::oh::Exception &e) {

    ostringstream text; text << "LucrodHistos::" << m_boardName << "::publishLBHistos ERROR publishing LBtime histos for LumiBlock" << lbid;

    ERS_REPORT_IMPL(ers::error, ers::Message, text.str(),);
  }    
}

/**********************************************************************/
void LucrodHistos::publishLBSumHistos(u_int lbid, std::string name, TH1D* histo)
/************************************************************************/
{
  try { m_oh_provider->publish(*histo, name.c_str(), lbid); }
  catch (daq::oh::Exception &e) {

    ostringstream text; text << "LucrodHistos::" << m_boardName << "::publishLBSumHistos ERROR publishing BCID-aware SUM/OR for LumiBlock" << lbid;

    ERS_REPORT_IMPL(ers::error, ers::Message, text.str(),);
  }    
}

/**********************************************************************/
void LucrodHistos::publishTimeHistos(int version)
/************************************************************************/
{
  for (int ifpga=0; ifpga<NCHFPGA; ++ifpga) { // check if the channel belongs to list of enabled channels

    int         ich   = getFpgaChannel(ifpga);
    std::string group = getFpgaGroup  (ifpga);

    if (!((m_lumiEnabledChannelMask >> ich) & 0x1)) continue;
        
    try {     
      std::ostringstream name; name << "/SHIFT/" << group << "/TIME/HITS/Ch_" << ich;
      
      m_oh_provider->publish(*hChHits[ich], name.str().c_str(), version);
    } 
    catch (daq::oh::Exception &e) {
      
      ostringstream text; text << "LucrodHistos::" << m_boardName << "::publishTimeHistos ERROR publishing hit histo version" << version;

      ERS_REPORT_IMPL(ers::error, ers::Message, text.str(),);
    }     
  }
  
  try { // common for all groups     

    std::string name ="/SHIFT/ALL/TIME/OrbitCounts";
    
    m_oh_provider->publish(*hOrbit, name.c_str(), version);
  } 
  catch (daq::oh::Exception &e) {

    ostringstream text; text << "LucrodHistos::" << m_boardName << "::publishTimeHistos ERROR publishing orbit histo version" << version;

    ERS_REPORT_IMPL(ers::error, ers::Message, text.str(),);
  }    
}

/**********************************************************************/
void LucrodHistos::publishExpertHistos()
/************************************************************************/
{
  for (int ich=0; ich<NLUCRODIN; ++ich) {
    
    int         index = m_inputGroupIndeces[ich];    
    std::string group = GROUPNAME[index];
    
    try { // baseline: all channels

      std::ostringstream name; name << "/EXPERT/" << group << "/BASELINES/Ch_" << ich;

      m_oh_provider->publish(*hChBaseline[ich], name.str().c_str(), 0);
    } 
    catch (daq::oh::Exception& ex) {

      ostringstream text; text << "LucrodHistos::" << m_boardName << "::publishExpertHistos ERROR publishing baseline histos";

      ERS_REPORT_IMPL(ers::warning, ers::Message, text.str(),);
    }
    
    if (((m_scopeEnabledChannelMask>>ich)&1) == 1) { // average shape can only be for channels with enabled readout to local stream.  
      
      try { // Waveforms     
	
	std::ostringstream name; name << "/EXPERT/" << group << "/WAVEFORM/Ch_" << ich;

	m_oh_provider->publish(*hDataChannel[ich], name.str().c_str(), 0);
      }
      catch (daq::oh::Exception& ex) {

	ostringstream text; text << "LucrodHistos::" << m_boardName << "::publishExpertHistos ERROR publishing DataChannel histos";

	ERS_REPORT_IMPL(ers::warning, ers::Message, text.str(),);
      } 
      
      try { // Thresholds             
	
	std::ostringstream name; name << "/EXPERT/" << group << "/THRESHOLDS/Ch_" << ich;

	m_oh_provider->publish(*hChThresholds[ich], name.str().c_str(), 0);
      }
      catch (daq::oh::Exception& ex) {
	
	ostringstream text; text << "LucrodHistos::" << m_boardName << "::publishExpertHistos ERROR publishing DataChannel histos";

	ERS_REPORT_IMPL(ers::warning, ers::Message, text.str(),);
      } 
    }
  }    
}

/*************************************************************/
int  LucrodHistos::getFpgaChannel(int ifpga)
/*************************************************************/
{  
  return ((m_fpgaMask>>ifpga)&1) == 1 ? 2*ifpga : 2*ifpga+1;
}

/*************************************************************/
std::string const LucrodHistos::getFpgaGroup(int ifpga)
/*************************************************************/
{  
  std::string const group = ((m_fpgaMask>>ifpga)&1) == 1 ?  
    GROUPNAME[m_inputGroupIndeces[2*ifpga]] : GROUPNAME[m_inputGroupIndeces[2*ifpga+1]];

  return group;  
}
