/********************************************************/
/*							*/
/* Date: October 2014    				*/ 
/* Author: C. Sbarra					*/
/*							*/
/***************************************************** **/

#include <iostream>
#include "ROSUtilities/ROSErrorReporting.h"
#include "DFDebug/DFDebug.h"
#include "DFSubSystemItem/Config.h"
#include "rcd_LUCRODBMA/vLucrod.h"
#include "rcd_LUCRODBMA/DataChannelLucrodBCID.h"
#include "ers/ers.h"

using namespace ROS;
using namespace std;

/**********************************************************************************************/
DataChannelLucrodBCID::DataChannelLucrodBCID(u_int                    channelId, 
					     u_int                    channelIndex, 
					     u_int                    rolPhysicalAddress,
					     u_long                   vmeBaseVA, 
					     DFCountedPointer<Config> configuration, 
					     boost::mutex&            mutex, 
					     sampleCollection&        collection, 
					     unsigned short           enabledChannelMask) :

  SingleFragmentDataChannel(channelId, channelIndex, rolPhysicalAddress, configuration),
  m_channelId              (channelId),
  m_channel_number         (rolPhysicalAddress),
  m_mutex                  (mutex),
  m_sampleCollection       (collection)
/**********************************************************************************************/
{ 
  m_vLUCROD = reinterpret_cast< LUCRODBoard_regs_t * >(vmeBaseVA);  // (virtual) base address of the channel

  m_min_wait = 999999;
  m_max_wait = 0;

  // determine how many channels are enabled in this run

//m_enabledChannelMask = configuration->getUInt("EnabledChannelMask");
  m_enabledChannelMask = enabledChannelMask; // Channel selection for local stream: init-value = 0x5555

  m_nchannels = 0;

  for (unsigned int ifpga=0; ifpga<NCHFPGA; ++ifpga) {

    int ich1 = ifpga*2;
    int ich0 = ifpga*2 + 1;

    if (((m_enabledChannelMask>>ich1) & 1) == 1) { m_read1[ifpga] = true; ++m_nchannels; }
    else                                           m_read1[ifpga] = false;
 
    if (((m_enabledChannelMask>>ich0) & 1) == 1) { m_read0[ifpga] = true; ++m_nchannels; } 
    else                                           m_read0[ifpga] = false; 
  }

  std::ostringstream msg; msg << "DataChannelLucrodBCID::DataChannelLucrodBCID list of enabled channels: ";
    
  for (unsigned int i=0; i<8; ++i) {
    
    if (m_read1[i]) msg << i*2   << " ";
    if (m_read0[i]) msg << i*2+1 << " ";
  }

  ERS_LOG(msg.str());
  ERS_LOG("DataChannelLucrodBCID::DataChannelLucrodBCID channel mask is 0x" << HEX(m_enabledChannelMask) << "; channel number is " << m_nchannels);
  ERS_LOG("DataChannelLucrodBCID::DataChannelLucrodBCID channelId is "      << channelId                 << "; rolPhysAdd is "     << m_channel_number);
}

/*********************************/
DataChannelLucrodBCID::~DataChannelLucrodBCID() 
/*********************************/
{
}

/************************************************************************************************/
int DataChannelLucrodBCID::getNextFragment(u_int* buffer, int max_size, u_int* status, unsigned long pciAddress)
/************************************************************************************************/
{
  u_int fsize = 0;    
  u_int* bufPtr = buffer;        

  *bufPtr++ = m_channelId; // write the channel number
  fsize += 4;

  *bufPtr++ = 0xabce; // insert a Header 
  fsize += 4;

  *bufPtr++  = (unsigned int) m_vLUCROD->CHRegs[0].waveFormBcid; // write the current BCID (take the one in FPGA 0 for the time being) 
  fsize += 4;

  *bufPtr++ = m_nchannels; // write the number of channels to be read
  fsize += 4;
  
  for (int ifpga=0; ifpga<NCHFPGA; ++ifpga) { // loop over the FPGA and read waveform (the enabled one for the time being)
    
    if (m_read1[ifpga] || m_read0[ifpga]) { // select enabled channels 

      unsigned int ich1 = ifpga*2;
      unsigned int ich0 = ifpga*2+1;

      unsigned short status = m_vLUCROD->CHRegs[ifpga].status;

      unsigned short allEmpty =  status & 5;
      unsigned int timeout    = 0;

      while (allEmpty == 5) { // wait for non-empty FIFO

	++timeout;
	
	if (timeout > 10000) break;
	
	status = m_vLUCROD->CHRegs[ifpga].status;

	allEmpty = status & 5;
      }
 
      if (timeout < m_min_wait) m_min_wait = timeout;
      if (timeout > m_max_wait) m_max_wait = timeout;

      if (timeout < 100000) {  
	
	if (m_read1[ifpga]) {
	
	  *bufPtr++ = 0xaa000000 | ich1;
	  fsize += 4;
	  
	  for (int i=0; i<NHDATA; ++i) { // store data 
	    
	    unsigned int data = (unsigned int)m_vLUCROD->CHRegs[ifpga].waveForm1_fifo;
	    
	    m_sampleCollection.sum [ich1][i] +=  (double)data;   
	    m_sampleCollection.sum2[ich1][i] += ((double)data)*((double)data);   
	    
	    *bufPtr++ = data; 
	    fsize += 4;
	  }
	}	
	
	if (m_read0[ifpga]) {

	  *bufPtr++ = 0xaa000000 | (ifpga*2+1);
	  fsize += 4;

	  for (int i=0; i<NHDATA; ++i) { // store data 

	    unsigned int data = m_vLUCROD->CHRegs[ifpga].waveForm0_fifo;
	    
	    m_sampleCollection.sum [ich0][i] +=  (double)data;   
	    m_sampleCollection.sum2[ich0][i] += ((double)data)*((double)data);   
	    
	    *bufPtr++ = data;
	    fsize += 4;
	  }
	}

	m_vLUCROD->CHRegs[ifpga].control |= (1<<9); usleep(1); // clear fifos
	m_vLUCROD->CHRegs[ifpga].control &= ~(1<<9);
      }
      else ERS_LOG("DataChannelLucrodBCID::getNextFragment timeout on FPGA " << ifpga << " status: 0x" << HEX(status));
    }    
  }    

  *bufPtr++ = 0xff000000; // add a trailer
  fsize += 4;
  
  m_sampleCollection.numberOfEvents++;
  
  { // Reenable waveform;
    boost::mutex::scoped_lock l(m_mutex);
    
    m_vLUCROD->MainRegs.globalCMD = 1;
    m_vLUCROD->MainRegs.globalCMD = 0;
  }

  *status = S_OK;

  unsigned int size = 4*(m_nchannels*65+5);
  
  if (size != fsize) { // check data

    ERS_LOG("DataChannelLucrodBCID::getNextFragment Data size smaller than expected: " << size << " versus " << fsize);

    ERS_REPORT_IMPL(ers::warning,
		    ers::Message, "DataChannelLUCRODBCID::getNextFragment Data size smaller than expected: " << size << " versus " << fsize,); 

    *status = S_TIMEOUT;
  }    
  
  return fsize; // bytes
}

/*************************************************/
DFCountedPointer<Config> DataChannelLucrodBCID::getInfo() 
/*************************************************/
{
  DFCountedPointer<Config> info = Config::New();
  
  info->set("Number of enabled channels",                  m_nchannels);
  info->set("Min. number of polling cycles before DREADY", m_min_wait);
  info->set("Max. number of polling cycles before DREADY", m_max_wait);
  
  return(info);
}
