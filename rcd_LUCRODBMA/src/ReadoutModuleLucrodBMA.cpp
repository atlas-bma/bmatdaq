/**********************************************************/
/*							  */
/* Date: 15 October 2014				  */ 
/* Author: C. Sbarra    	       			  */
/*                                                        */
/* read accumulators ONLY for enabled channels            */
/* while keeping 4 channels/group for OLC/LUMAT           */
/* This to allow disabling reading of DEAD PMT            */
/* from local stream with no effects on other aspects     */
/* 2019:                                                  */
/* add user command to disable/enable channel on the fly  */
/* histograms and OLC names contain sensor NAMES          */
/* PMT names in each group published to IS                */
/* BEWARE: at least one channel/fpga when scope enabled   */
/* 2020: adapt to Channel FW version 2E                   */
/*							  */
/**********************************************************/

#include <fstream>
#include <iostream>
#include <ctime>
#include <algorithm>
#include <ctype.h>
#include "ers/ers.h"

#include "DFDebug/DFDebug.h"
#include "DFSubSystemItem/ConfigException.h"
#include "ROSModules/ModulesException.h"
#include "rcc_time_stamp/tstamp.h"

// LatencyValue 
#include "dal/TriggerConfiguration.h"
#include "RunControl/Common/OnlineServices.h"

#include "rcd_LUCRODBMA/ReadoutModuleLucrodBMA.h"
#include "rcd_LUCRODBMA/DataChannelLucrodScope.h"
#include "rcd_LUCRODBMA/DataChannelLucrodBCID.h"
#include "rcd_LUCRODBMA/ModuleLucrodBMAInfoNamed.h"
#include "rcd_LUCRODBMA/LucrodBMABaselineInfoNamed.h"
#include "rcd_LUCRODBMA/BMASensorNamesInfoNamed.h"

#include "TTCInfo/LB_IS.h"
#include "TTCInfo/LumiRawNamed.h"
#include "TTCInfo/LumiRawPerBunchNamed.h"

#include <sys/types.h>
#include <sys/socket.h>

using namespace ROS;
using namespace std;

namespace {

  static unsigned int const NBINS = 500;

  int const Group1 = 0;
  int const Group2 = 1;
  int const EMPTY  = 2;

 std::string const GROUPNAME[3] = {"Group1", "Group2", "Empty"}; 
}

/**********************/
ReadoutModuleLucrodBMA::ReadoutModuleLucrodBMA() : m_lucrodHistos(m_sampleCollection)
/**********************/
{ 
  m_AtlasAlive     = false;
  m_histo_server   = "Histogramming";  //this should be a configurable parameter, not hard-coded...
  m_is_server      = "Monitoring";
  m_runType        = "Physics"; // assume Physics at startup
  m_oh_provider    = 0;
  m_setupDone      = 0;
  m_runStarted     = 0;
  m_configured     = 0;
  m_connected      = 0;
  m_atlasLumiBlock = 0;

  for (int i=0; i<NCHFPGA; ++i) m_fpgaChSEU[i] = 0;

  m_fpgaVSEU     = 0;      
  m_group1Status = 0;
  m_group2Status = 0;

  initializeIsData();

  // IS stuff
  m_partitionName = getenv("TDAQ_PARTITION");
  m_ipcpartition  = m_partitionName;
  m_is_receiver   = new ISInfoReceiver(m_ipcpartition);

  IPCPartition ipcP     = "initial";
  IPCPartition ipcATLAS = "ATLAS"; 

  m_is_receiverInitial = new ISInfoReceiver(ipcP);
  m_is_receiverAtlas   = new ISInfoReceiver(ipcATLAS);

  m_LumiBlockProviderName = "RunParams.LumiBlock";
}

/******************************************/
ReadoutModuleLucrodBMA::~ReadoutModuleLucrodBMA() noexcept 
/*******************************************/
{  
  //if (m_runType != "Physics") 
  sleep(1); // slow down closing operation in calib runs (avoid rc-is2cool complaints)
  
  while (m_dataChannels.size() > 0) {

    DataChannel* channel = m_dataChannels.back();

    m_dataChannels.pop_back();

    delete channel; // we created them
  }

  delete m_is_receiver;
  delete m_is_receiverInitial;
  delete m_is_receiverAtlas;
}

/***********************************************************************/
void ReadoutModuleLucrodBMA::setup(DFCountedPointer<Config> configuration) 
/************************************************************************/
{
  ERS_LOG("RMLucrod::" << m_boardName << "::setup Entered");

  m_configuration = configuration;
  m_is_server     = configuration->getString("ISServerName");
  m_histo_server  = configuration->getString("OHServerName");

  ERS_INFO("RMLucrod:: histogram server set to " << m_histo_server);
  m_boardName         = m_configuration->getString("BoardName");  
  m_inputNames        = m_configuration->getVector<std::string>("InputNames");
  m_inputGroupIndeces = m_configuration->getVector<unsigned int>("InputGroupIndeces");

  if (m_inputGroupIndeces.size() != NLUCRODIN) {

    std::ostringstream msg; msg << "RMLucrod::" << m_boardName << " input channel size is " << m_inputGroupIndeces.size() << " instead of 16. Bad configuration!";
    
    ERS_REPORT_IMPL(ers::error, ers::Message, msg.str(), );
  }

  setEnabledChannelMask(); // deduce from InputGroupIndeces: m_scopeEnabledChannelMask, m_lumiEnabledChannelMask

  ERS_LOG("RMLucrod::" << m_boardName << "::setup lumiEnabledChannelMask:  0x" << std::hex << m_lumiEnabledChannelMask);
  ERS_LOG("RMLucrod::" << m_boardName << "::setup scopeEnabledChannelMask: 0x" << std::hex << m_scopeEnabledChannelMask);
  
  storeChannelInEachGroup(); // this is used when reading lumi data and filling histograms
   
  std::ostringstream grp1, grp2;

  grp1 << "RMLucrod::" << m_boardName << "::setup list of channels belonging to sensor group 1:";
  grp2 << "RMLucrod::" << m_boardName << "::setup list of channels belonging to sensor group 2:";
  
  for (unsigned int i=0; i<m_grp1Ch.size(); ++i) grp1 << " " << m_grp1Ch[i];  
  for (unsigned int i=0; i<m_grp2Ch.size(); ++i) grp2 << " " << m_grp2Ch[i];
  
  ERS_INFO(grp1.str());
  ERS_INFO(grp2.str());
  
  // This tells the FW which input has to be used in each FPGA to make OR's and SUMs (actually sums via SW). 
  // For FPGAs where no channel is enabled (dead PMT), a default is used; 
  // thresholds are then set to 4095 so that no hits will ever be counted and affect the OR/SUMs.
  setFpgaChannelMask();
  
  ERS_LOG("RMLucrod::" << m_boardName << "::setup FPGAChannelMask is 0x" << std::hex << m_fpgaChannelMask);
  
  int m_latencyValue = 0;

  if (m_partitionName.compare("ATLAS") == 0) { 

    try {

      const daq::core::TriggerConfiguration* trigConf = daq::rc::OnlineServices::instance().getPartition().get_TriggerConfiguration();

      m_latencyValue = trigConf->get_LatencyValue();

      ERS_LOG("RMLucrod::" << m_boardName << "::setup **** Got Latency Value of " << m_latencyValue);
    }
    catch(daq::config::Exception &ex){

      std::ostringstream msg; msg << "RMLucrod::" << m_boardName << "::setup Error getting Latency Value from TriggerConfiguration object: " << ex;

      ERS_REPORT_IMPL(ers::error, ers::Message, msg.str(), );
    }
  }

  m_internalClock = m_configuration->getBool("InternalClock"); // this is to allow for standalone tests (no external clock)
  
  m_lucrodBoard.setup (m_configuration, m_latencyValue);
  m_lucrodHistos.setup(m_configuration, m_lumiEnabledChannelMask, m_scopeEnabledChannelMask, m_fpgaChannelMask);
  
  m_scopeEnabled = m_configuration->getUInt("ScopeEnabled");
  m_waveFormBcid = m_configuration->getUInt("BC2WaveFormFifo");

  ERS_INFO("RMLucrod::" << m_boardName << "::setup ScopeEnabled: " << m_scopeEnabled << " (0:fixed BCID trigger, 1:self-trigger) ");
  
  if (!m_scopeEnabled)
    ERS_INFO("RMLucrod::" << m_boardName << "::setup BC2WaveFormFifo: " << m_waveFormBcid << " (triggered BCID)");
  
  m_id                 = m_configuration->getInt("channelId", 0);
  m_rolPhysicalAddress = m_configuration->getInt("ROLPhysicalAddress", 0);

  ERS_LOG("RMLucrod::" << m_boardName << "::setup channelId = " << m_id << "; ROLPhysicalAddress = " << m_rolPhysicalAddress);
  
  m_setupDone = 1;
  
  ERS_LOG(m_boardName << "::setup Done");
}

/**********************************************/
void ReadoutModuleLucrodBMA::configure(const daq::rc::TransitionCmd&)
/**********************************************/
{
  { 
    boost::mutex::scoped_lock l(m_mutex);
    
    m_lucrodBoard.configure(m_lumiEnabledChannelMask, m_scopeEnabledChannelMask, m_fpgaChannelMask);
  }
  
  u_long ptr = m_lucrodBoard.getVmeVirtualPtr(); // create the needed data channel

  if (m_scopeEnabled == 1) {

    ERS_LOG("RMLucrod::" << m_boardName << "::configure creating self-triggering DataChannel");

    DataChannelLucrodScope* channel = new DataChannelLucrodScope(m_id, 0, m_rolPhysicalAddress, ptr, m_configuration, m_sampleCollection, m_scopeEnabledChannelMask);

    m_dataChannels.push_back(channel);
  }
  else {

    ERS_LOG("RMLucrod::" << m_boardName << "::configure creating BCID DataChannel");

    DataChannelLucrodBCID* channel = new DataChannelLucrodBCID(m_id, 0, m_rolPhysicalAddress, ptr, m_configuration, m_mutex, m_sampleCollection , m_scopeEnabledChannelMask);

    m_dataChannels.push_back(channel);
  }
  
  m_lucrodHistos.bookHistos(); 

  std::string provider_name = "BMA_Lucrod";

  ERS_LOG("RMLucrod::" << m_boardName << "::configure creating " << provider_name << " ROOT provider");

  if (m_oh_provider == 0) {

    try { m_oh_provider = new OHRootProvider(m_ipcpartition, m_histo_server, provider_name.c_str()); } 
    catch(daq::oh::ProviderAlreadyExist &) { ERS_REPORT_IMPL(ers::error, ers::Message, "RMLucrod::" << m_boardName << "::configure Error creating OH provider",); } 
    catch(daq::oh::RepositoryNotFound   &) { ERS_REPORT_IMPL(ers::error, ers::Message, "RMLucrod::" << m_boardName << "::configure Error creating OH provider",); } 
    catch(daq::oh::Exception            &) { ERS_REPORT_IMPL(ers::error, ers::Message, "RMLucrod::" << m_boardName << "::configure Error creating OH provider",); }
  }

  ERS_LOG("RMLucrod::" << m_boardName << "::configure subscribing to LumiBlock IS info");

  try { m_is_receiver->subscribe(m_LumiBlockProviderName, &ReadoutModuleLucrodBMA::callback_LB, this); }
  catch(daq::is::Exception & ex) { ERS_INFO("RMLucrod::" << m_boardName << "::configure Error subscribing to LB info"); }

  ERS_LOG("RMLucrod::" << m_boardName << "::configure subscribing to ATLAS LumiBlock IS info");

  try { m_is_receiverAtlas->subscribe(m_LumiBlockProviderName, &ReadoutModuleLucrodBMA::callback_ATLASLB, this); }
  catch(daq::is::Exception & ex) { ERS_INFO("RMLucrod::" << m_boardName << "::configure Error subscribing to ATLAS LB info"); }
  
  m_configured  = 1;
  
  ERS_LOG("RMLucrod::" << m_boardName << "::configure: Done");
}

/******************************************/
void ReadoutModuleLucrodBMA::unconfigure(const daq::rc::TransitionCmd&)
/******************************************/
{
  ERS_LOG("RMLucrod::" << m_boardName << "::unconfigure deleting data channels"); // delete the datachannel created at configure!!

  while (m_dataChannels.size() > 0) {

    DataChannel* channel = m_dataChannels.back();

    m_dataChannels.pop_back();
    
    delete channel;
  }

  // stop accumulators
  {
    boost::mutex::scoped_lock l(m_mutex);   

    m_lucrodBoard.unconfigure();
  }
  
  ERS_LOG("RMLucrod::" << m_boardName << "::unconfigure ATLAS partition. Unsubscribing from LumiBlock");
  
  if (m_AtlasAlive) m_is_receiverAtlas->unsubscribe(m_LumiBlockProviderName);
  else              m_is_receiver->unsubscribe     (m_LumiBlockProviderName);
  
  sleep(1); //slow down closing operation (avoid rc-is2cool complaints)
    
  delete m_oh_provider;

  m_oh_provider = 0;
  m_configured  = 0;

  ERS_LOG("RMLucrod::" << m_boardName << "::unconfigure Done");
}

/**************************************/
void ReadoutModuleLucrodBMA::connect(const daq::rc::TransitionCmd&)
/**************************************/
{
  ERS_LOG("RMlucrod::" << m_boardName << "::connect Entered");

  { 
    boost::mutex::scoped_lock l(m_mutex);

    m_lucrodBoard.connect();  
  }

  m_lucrodHistos.connect(m_oh_provider); 
  
  m_connected = 1;

  ERS_LOG("RMLucrod::" << m_boardName << "::connect Done");
}

/*****************************************/
void ReadoutModuleLucrodBMA::disconnect(const daq::rc::TransitionCmd&)
/*****************************************/
{
  ERS_LOG("RMlucrod::" << m_boardName << "::disconnect Entered");

  boost::mutex::scoped_lock l(m_mutex);

  m_lucrodBoard.disconnect();

  sleep(1); //slow down closing operation (avoid rc-is2cool complaints)

  m_connected = 0;

  ERS_LOG("RMLucrod::" << m_boardName << "::disconnect Done");
}    

/********************************************/    
void ReadoutModuleLucrodBMA::prepareForRun(const daq::rc::TransitionCmd&)
/********************************************/
{    
  ERS_LOG("RMlucrod::" << m_boardName << "::prepareForRun Entered");

  m_runNumber = m_runParams->getInt   ("runNumber"); 
  m_runType   = m_runParams->getString("runType"); 

  ERS_LOG("RMLucrod::" << m_boardName << "::prepareForRun Run Number: " << m_runNumber);
  ERS_LOG("RMLucrod::" << m_boardName << "::prepareForRun Run Type: "   << m_runType);
  
  m_lucrodHistos.prepareForRun(); 

  initializeIsData();

  sleep(5); // Avoid trigger start before EB is ready!!!!

  {
    boost::mutex::scoped_lock l(m_mutex);
    
    for(int i=0; i<NCHFPGA; ++i)
      m_lucrodBoard.cleanupAndRestartFpgaCh(i);

    m_lucrodBoard.cleanupAndRestartFpgaV();    
    m_lucrodBoard.prepareForRun();
  }

  m_runStarted = 1;   

  ERS_LOG("RMlucrod::" << m_boardName << "::prepareForRun Done");
}

/**************************************/
void ReadoutModuleLucrodBMA::stopDC(const daq::rc::TransitionCmd&)
/**************************************/
{
  ERS_LOG("RMlucrod::" << m_boardName << "::stopDC Entered");

  boost::mutex::scoped_lock l(m_mutex);

  m_lucrodBoard.stopDC();  

  publishBaselineInfo();
  
  m_runStarted = 0;        
  
  ERS_LOG("RMlucrod::" << m_boardName << "::stopDC Done");
}

/**************************/
void ReadoutModuleLucrodBMA::user(const daq::rc::UserCmd& command) 
/**************************/
{
  std::string              cmd  = command.commandName();
  std::vector<std::string> args = command.commandParameters();

  unsigned int nArgs = args.size();
  
  ERS_INFO("RMLucrodBMA::" << m_boardName << "::user Received command: " << cmd << " from run control with " << nArgs << " arguments");
  
  if (cmd == "SETTHRESHOLDS") {
    
    if (nArgs < 1) ERS_INFO("RMLucrod::" << m_boardName << "::user command syntax: SETTHRESHOLDS value");
    else {

      std::stringstream ss(args[0]);

      unsigned short thresh; ss >> thresh;
      
      ERS_INFO("RMLucrodBMA::" << m_boardName << "::user Setting sensor thresholds to " << thresh);

      boost::mutex::scoped_lock l(m_mutex);	

      m_lucrodBoard.setThresholds(thresh);	
    }
  }
  else if (cmd == "MOVEHITFINEDELAY") {
    
    if (nArgs != 3) ERS_INFO("RMLucrod::" << m_boardName << "::user command syntax: MOVEHITFINEDELAY ch# UP/DOWN VALUE");      
    else {
      
      std::stringstream s1(args[0]); int chNum; s1 >> chNum;      
      std::stringstream s2(args[2]); int shift; s2 >> shift;      
      
      if (args[1]=="DOWN") shift *= -1.;
      
      unsigned short delay = m_lucrodBoard.getChHitFineDelay((unsigned short)chNum);
      
      delay += shift;
      
      m_lucrodBoard.setChHitFineDelay((unsigned short)chNum, delay);
    }
  }   
  else if (cmd == "MOVEFINEDELAY") {
    
    if (nArgs != 3) ERS_INFO("RMLucrod::" << m_boardName << "::user command syntax is MOVEFINEDELAY INPUT# UP/DOWN VALUE");      
    else {

      std::stringstream s1(args[0]); int number; s1 >> number;      
      std::stringstream s2(args[2]); int shift;  s2 >> shift;      

      if (args[1]=="DOWN") shift *= -1.;

      int fpga = number/2;

      ERS_INFO("RMLucrod::" << m_boardName << "::user request to shift fine delay of sensor # " << number << " in fpga" << fpga << " by " << shift);
      
      unsigned short now = m_lucrodBoard.getFineDelay((unsigned short)fpga);
      
      // Which is the used channel? channel 0 --> bits 8-11; channel 1--> bits 12-15
      // The code identifies the enabled channel and operates on that one --> to be eventually changed

      unsigned short newDelay = ((m_fpgaChannelMask>>fpga)&0x1) ? (now >> 12)&0xf : (now >> 8) & 0xf;
      
      ERS_INFO("RMLucrod::" << m_boardName << "::user sensor in fpga "<< fpga << " fine delay reads 0x" << HEX(now) << "; current delay is " << newDelay);
      
      newDelay += shift;
      
      if (newDelay > 7) ERS_INFO("RMLucrod::" << m_boardName << "::user Requested fine delay " << newDelay << " out of valid range is 0-7. Doing nothing");
      else {
	
	unsigned short val = ((m_fpgaChannelMask>>fpga)&0x1) ?  (newDelay<<12) | (now & 0xfff) : (newDelay<<8) | (now & 0xf0ff);
	
	ERS_INFO("RMLucrod::" << m_boardName << "::user Setting fine delay of fpga " << fpga << " to new value " << HEX(val));
	
	m_lucrodBoard.setFineDelay((unsigned short)fpga, val);
      }  
    } 
  }  
  else if (cmd == "MOVEORBITDELAY") {

    if (nArgs != 2) ERS_INFO("RMLucrod::" << m_boardName << "::user command syntax: MOVEORBITDELAY UP/DOWN VALUE");
    else {
      
      std::stringstream ss(args[1]);
      short shift;
      ss >> shift;
      
      unsigned short currentChDelay = m_lucrodBoard.getChOrbitDelay();
      unsigned short newChDelay     = currentChDelay;

      if (args[0]=="UP")   { newChDelay += shift; }
      if (args[0]=="DOWN") { newChDelay -= shift; }   

      if (newChDelay < 3560) {

	ERS_INFO("RMLucrodBMA::" << m_boardName << "::user "
		 << " current ChOrbitDelay="     << currentChDelay 
		 << "; new values: ChOrbitDelay=" << newChDelay);
	
	m_lucrodBoard.setChOrbitDelay(newChDelay);
      }
      else ERS_INFO("RMLucrodBMA::" << m_boardName << "::user requested new values out of range: ChOrbitDelay=" << newChDelay  << "; Doing nothing...");
    }
  }  
  else if (cmd == "ReloadMainFpgaFW") {

    ERS_INFO("RMLucrodBMA::" << m_boardName << "::user command syntax: ReloadMainFpgaFW");
      
    boost::mutex::scoped_lock l(m_mutex);
      
    m_lucrodBoard.reloadFpgaVFW();
  }  
  else if (cmd == "ReloadChannelFpgaFW") {

    if (nArgs != 1) ERS_INFO("RMLucrod::" << m_boardName << "::user command syntax: ReloadMainFpgaFW fpga(0/2/4/6)");

    std::stringstream ss(args[0]);
    unsigned short ifpga;
    ss >> ifpga;
    
    ERS_INFO("RMLucrodBMA::" << m_boardName << "::user Reloading ch fpga FW in fpga " << ifpga << " and " << ifpga+1);
    
    boost::mutex::scoped_lock l(m_mutex);
    
    m_lucrodBoard.reloadFpgaChFW(ifpga);
  }
  else if (cmd == "SetBcid4Trigger") { 

    if (nArgs != 1) ERS_INFO("RMLucrodBMA::" << m_boardName << "::user command syntax: SetBcid4Trigger BCIDvalue");
    else {

      std::stringstream ss(args[0]);
      unsigned int bcid;
      ss >> bcid;

      ERS_INFO("RMLucrodBMA::" << m_boardName << "::user BCID for waveform will be set to " << bcid);

      boost::mutex::scoped_lock l(m_mutex);

      m_lucrodBoard.setBcid4Waveform(bcid);
    } 
  } 
  else if (cmd == "SetFadcGain") {
    
    if (nArgs != 2) ERS_INFO("Uncorrect number of parameters. Use SetFadcGain [fpga#] [gain]");
    else {
      
      std::stringstream s1(args[0]); int fpga; s1 >> fpga;
      std::stringstream s2(args[1]); int gain; s2 >> gain;

      if (fpga < 0)        ERS_INFO("FPGA number cannot be negative: " << fpga);
      if (fpga >= NCHFPGA) ERS_INFO("FPGA number must be smaller than " << NCHFPGA << ": " << fpga);
      
      ERS_INFO("Setting FadcGain: " << gain << " for fpga: " << fpga);
        
      m_lucrodBoard.setFadcGain(fpga, gain);       
    }
  }
  else ERS_INFO("RMLucrodBMA::" << m_boardName << "::user unknown command");
}

/**************************/
void ReadoutModuleLucrodBMA::storeChannelInEachGroup()
/**************************/
{
  m_grp1Ch.clear();
  m_grp2Ch.clear();

  for (unsigned int i=0; i<m_inputGroupIndeces.size(); ++i) {
    
    if      (m_inputGroupIndeces[i] == Group1) m_grp1Ch.push_back(i);
    else if (m_inputGroupIndeces[i] == Group2) m_grp2Ch.push_back(i);
  }
}

/**************************/
void ReadoutModuleLucrodBMA::handleEndOfLB(unsigned int lumiBlock)
/**************************/
{
  ERS_LOG("RMLucrod::" << m_boardName << "::handleENDofLB " << lumiBlock); // Here we read all fifos from electronics and fill histograms 
  
  unsigned int htimebin = (lumiBlock % NBINS); // bin to be filled for time histos, version for BCID-wise ones (first bin is LB 0)

  htimebin += 1; // adjust to root needs
  
  if (htimebin == 1) {
    
    ERS_INFO("RMLucrod::" << m_boardName << "::handleENDofLB htimebin " << htimebin << " lumiBlock " << lumiBlock << ". Resetting Time histos");

    m_lucrodHistos.resetTimeHistos();
  }
  
  // number of orbits in this LB
  m_isData.orbitsInLB = m_lucrodBoard.getOrbitCounts(m_group1Status, m_group2Status);
  
  // prevent divide by zero
  if (m_isData.orbitsInLB == 0) m_isData.orbitsInLB = 1;
  
  m_lucrodHistos.fillOrbitHisto(htimebin, m_isData.orbitsInLB);

  // Read HITs for ENABLED channels in each group

  ERS_LOG(" RMLucrodBMA:: channels in group1: " << m_grp1Ch.size() << "; group 2 " << m_grp2Ch.size());

  for (u_int j=0; j<m_grp1Ch.size(); ++j) {

    long long totHits = m_lucrodBoard.getChannelHits(m_grp1Ch[j], m_isData.chHit[m_grp1Ch[j]]);
    
    m_lucrodHistos.fillChLBHitHistos  (m_grp1Ch[j], m_isData.chHit[m_grp1Ch[j]]);        
    m_lucrodHistos.fillChTimeHitHistos(m_grp1Ch[j], htimebin, totHits);
  } 
 
  for (u_int j=0; j<m_grp2Ch.size(); ++j) {

    long long totHits = m_lucrodBoard.getChannelHits(m_grp2Ch[j], m_isData.chHit[m_grp2Ch[j]]);

    m_lucrodHistos.fillChLBHitHistos  (m_grp2Ch[j], m_isData.chHit[m_grp2Ch[j]]);        
    m_lucrodHistos.fillChTimeHitHistos(m_grp2Ch[j], htimebin, totHits);
  }  
}

/**************************/
void ReadoutModuleLucrodBMA::callback_ATLASLB(ISCallbackInfo* iscb)
/**************************/
{
  LumiBlock lumiInfo;
  
  iscb->value(lumiInfo);
 
  m_runNumber = lumiInfo.RunNumber;

  unsigned int lumiBlock = lumiInfo.LumiBlockNumber;
  
  ERS_INFO("RMLucrodBMA:: Received ATLAS LB callback for LB " << lumiBlock);
  
  if (lumiBlock < 2) { 
    
    ERS_INFO("RMLucrodBMA:: Wait for ATLAS LB 3 before switching to ATLAS LBs");
    
    return;
  }
  
  if (!m_AtlasAlive) { //unsubscribe from local LB info

    if (m_atlasLumiBlock < 2) { 
      
      ERS_INFO("RMLucrodBMA:: Wait for LB 3 before switching to ATLAS LBs");
      
      return;
    }    
    
    m_AtlasAlive = true;

    ERS_INFO("RMLucrodBMA:: Unsuscribing from partiton_bma LB callback");
    
    m_is_receiver->unsubscribe(m_LumiBlockProviderName);
  }

  //reset LB value to ATLAS's
  m_atlasLumiBlock = (lumiBlock > 0) ? lumiBlock-1 : 0;

  tstamp ts1, ts2;
  boost::mutex::scoped_lock l(m_mutex);
  ts_clock(&ts1);
  m_lucrodBoard.changeAccFifo(); // Change accumulator and orbit FIFO in all channels 

  usleep(90);

  if (m_atlasLumiBlock > 0) {

    handleEndOfLB             (m_atlasLumiBlock); // read registers and fill histograms
    publishRawLumiPerBunchInfo(m_atlasLumiBlock); // publish to IS/OH (this takes about 100 msec) 

    int version = m_atlasLumiBlock/NBINS;

    m_lucrodHistos.publishLBHistos  (m_atlasLumiBlock);
    m_lucrodHistos.publishTimeHistos(version);
  }

  ts_clock(&ts2);

  float delta = ts_duration(ts1, ts2)*1000;

  m_lucrodHistos.fillLBtimeHisto(delta);

  initializeIsData();

  //reset SEU/status flag (assuming it affected the current LB and was recovered automatically)
  m_fpgaVSEU     = 0;
  m_group1Status = 0;
  m_group2Status = 0;

  for (int i=0; i!=NCHFPGA; ++i)
    m_fpgaChSEU[i] = 0;      
}

/**************************/
void ReadoutModuleLucrodBMA::callback_LB(ISCallbackInfo* iscb)
/**************************/
{
  // as soon as the LB is closed we need to read raw data from fifos, fill histograms, and publish raw data to IS for OLC
  
  LumiBlock lumiInfo;
  
  iscb->value(lumiInfo);    
  
  m_runNumber = lumiInfo.RunNumber;
  
  unsigned int lumiBlock = lumiInfo.LumiBlockNumber;
  
  ERS_INFO("RMLucrodBMA:: Received LB callback for LB " << lumiBlock);

  m_atlasLumiBlock = (lumiBlock > 0) ? lumiBlock-1 : 0;   
  
  tstamp ts1, ts2;
  boost::mutex::scoped_lock l(m_mutex);
  ts_clock(&ts1);
  m_lucrodBoard.changeAccFifo(); // Change accumulator and orbit FIFO in all channels 

  usleep(90);

  if (m_atlasLumiBlock > 0) {
        
    handleEndOfLB             (m_atlasLumiBlock); // read registers and fill histograms
    publishRawLumiPerBunchInfo(m_atlasLumiBlock); // publish to IS/OH (this takes about 100 msec) 
    
    int version = m_atlasLumiBlock/NBINS;

    m_lucrodHistos.publishLBHistos  (m_atlasLumiBlock);
    m_lucrodHistos.publishTimeHistos(version);
  }
  
  ts_clock(&ts2); 

  float delta = ts_duration(ts1, ts2)*1000;

  m_lucrodHistos.fillLBtimeHisto(delta);

  initializeIsData();

  //reset SEU/status flag (assuming it affected the current LB and was recovered automatically)
  m_fpgaVSEU     = 0;
  m_group1Status = 0;
  m_group2Status = 0;

  for (int i=0; i!=NCHFPGA; ++i)
    m_fpgaChSEU[i] = 0;      
}

/**************************/
void ReadoutModuleLucrodBMA::initializeIsData()
/**************************/
{
  m_isData.orbitsInLB = 1;

  for(int i=0; i<NLUCRODIN; ++i) m_isData.chHit[i].fill(0);
}

/***********************************/
void ReadoutModuleLucrodBMA::checkSEU()
/***********************************/
{
  { // lock HW resources
    
    boost::mutex::scoped_lock l(m_mutex);

    if (m_lucrodBoard.FpgaVSEU()) { // need FW version 0x1B on
      
      m_fpgaVSEU = 1;      
      
      ERS_REPORT_IMPL(ers::warning, ers::Message, "RMLucrod::" << m_boardName << "::checkSEU FW reloaded in Main FPGA: probable SEU. Recovering...",);

      m_lucrodBoard.configureFpgaV(m_fpgaChannelMask);
      m_lucrodBoard.move2ExternalClock();

      ERS_REPORT_IMPL(ers::warning, ers::Message, "RMLucrod::" << m_boardName << "::checkSEU Cleaning up and restarting MAIN FPGA FW",);

      m_lucrodBoard.cleanupAndRestartFpgaV();
    }
    
    for (unsigned short ifpga=0; ifpga!=NCHFPGA; ++ifpga) {

      if (m_lucrodBoard.FpgaChSEU(ifpga)) {
	
	m_fpgaChSEU[ifpga] = 1;      
	
	ERS_REPORT_IMPL(ers::warning, ers::Message, "RMLucrod::" << m_boardName << "::checkSEU Detected FW reload in FpgaCH " << ifpga << ". Recovering ...",);  
	
	m_lucrodBoard.configureFpgaCh        (ifpga, m_scopeEnabledChannelMask, m_lumiEnabledChannelMask);
	m_lucrodBoard.cleanupAndRestartFpgaCh(ifpga);
      }
    }  
  }  
}

/***********************************/
void ReadoutModuleLucrodBMA::publish()
/***********************************/
{
  if (!m_runStarted) return;
  
  checkSEU();
  
  m_lucrodHistos.fillDataChannelHistos(); // Monitor of the selected data channel

  publishLucrodInfo();          
  
  { // Expert histos
    boost::mutex::scoped_lock l(m_mutex);
    
    m_lucrodBoard.checkBusError();
    
    if ((!m_internalClock) && (m_fpgaVSEU = 0)) m_lucrodBoard.checkClock(); 
    
    for (int ich=0; ich<NLUCRODIN; ++ich) { // Read all baselines and evaluate thresholds
      
      unsigned int baseline = m_lucrodBoard.getChBaseline(ich);
      
      m_lucrodHistos.fillChBaselineHistos(ich, (double)baseline);
      
      //CS adjust for inverted polarity: threshold on signal decay
      double threshold = (double)m_lucrodBoard.getChThreshold(ich) - (double)baseline;
      
      m_lucrodHistos.fillChThresholdHistos(ich, threshold);
    }
  }
  
  m_lucrodHistos.publishExpertHistos();  
}

/***********************************************************/
DFCountedPointer < Config > ReadoutModuleLucrodBMA::getInfo()
/**********************************************************/
{
  DFCountedPointer<Config> info = Config::New();
  
  DEBUG_TEXT(DFDB_RCDDETECTOR, 15, "RMLucrod::" << m_boardName << "::getInfo: Done");
  
  return(info);
}

/* ****************************************** */
void ReadoutModuleLucrodBMA::setFpgaChannelMask()
/* ****************************************** */
{
  m_fpgaChannelMask = 0; // 1-bit per FPGA (8 meaningful); by default send fpga channel 0 to FPGAV (lucrod ODD channels: 1, 3, 5, ...)

  if (m_lumiEnabledChannelMask != 0) {

    unsigned int enabledChannelMask = m_lumiEnabledChannelMask; // local copy 
    unsigned int one = 1;
    
    for (int i=0; i<NCHFPGA; ++i) {  

      unsigned int thisFpga = enabledChannelMask & 3; // store bits relevant for current FPGA
      
      if      (thisFpga == 1) m_fpgaChannelMask |= (one<<i); // send channel 1 to FPGAV
      else if (thisFpga == 3)
        ERS_REPORT_IMPL(ers::error, ers::Message, 
			"RMLucrod::" << m_boardName << "::setFpgaChannelMask Two LUMI enabled channels in same FPGA: please check DB setting " << thisFpga << "!!",);
      
      enabledChannelMask = enabledChannelMask >> 2;
    }
  }
}

/* ****************************************** */
void ReadoutModuleLucrodBMA::prepareSensorInfo()
/* ****************************************** */
{
  m_Group2Names.clear();
  m_Group1Names.clear();
  m_Group1Fpga.clear();
  m_Group2Fpga.clear();
  
  for (unsigned int i=0; i<m_inputGroupIndeces.size(); ++i) {
    
    if      (m_inputGroupIndeces[i] == Group1)  { m_Group1Names.push_back(m_inputNames[i]); m_Group1Fpga.push_back(int(i/2)); }
    else if (m_inputGroupIndeces[i] == Group2)  { m_Group2Names.push_back(m_inputNames[i]); m_Group2Fpga.push_back(int(i/2)); }
  }
}

/* ****************************************** */
void ReadoutModuleLucrodBMA::setEnabledChannelMask()
/* ****************************************** */
{
  unsigned short lumiEnabledChannelMask  = 0;
  unsigned short scopeEnabledChannelMask = 0;
  unsigned short one = 1; 

  for (unsigned int i=0; i<m_inputGroupIndeces.size(); ++i) {
    
    if (m_inputGroupIndeces[i] == Group1 || m_inputGroupIndeces[i] == Group2) {
      
      lumiEnabledChannelMask  |= (one<<i);
      scopeEnabledChannelMask |= (one<<i);	  
    }
  }
    
  m_lumiEnabledChannelMask  = lumiEnabledChannelMask;
  m_scopeEnabledChannelMask = scopeEnabledChannelMask;
}

/***********************************************/
std::vector<double> ReadoutModuleLucrodBMA::arrayToVector(std::array<double, NBCID> const & data)
/***********************************************/
{
  std::vector<double> rawData;

  for (auto it=data.begin(); it!=data.end(); it++)
    rawData.push_back(*it);
  
  return(rawData);  
}

/***********************************************/
std::vector<double> ReadoutModuleLucrodBMA::arrayToVector(std::array<uint32_t, NBCID> const & data)
/***********************************************/
{
  std::vector<double> rawData;
  
  for (auto it=data.begin(); it!=data.end(); it++)
    rawData.push_back(*it);
  
  return(rawData);  
}

/***********************************************/
void ReadoutModuleLucrodBMA::publishRawLumiPerBunchInfo(unsigned int lumiBlock)
/***********************************************/
{
//ERS_LOG("RMLucrod::" << m_boardName << "::publishRawLumiPerBunchInfo");
  
  unsigned int statusG1 = m_group1Status;
  unsigned int statusG2 = m_group2Status;

  std::string Grp1N = "_Group1";
  std::string Grp2N = "_Group2";

  // HitSum, Event, ChargeSum
  unsigned int status = m_fpgaVSEU | statusG1;
  unsigned int nch    = m_grp1Ch.size();

  for (unsigned int i=0; i!=nch; ++i)
    status |= m_fpgaChSEU[m_grp1Ch[i]/2];

  status = m_fpgaVSEU | statusG2;
  nch    = m_grp2Ch.size();

  for (unsigned int i=0; i!=nch; ++i)
    status |= m_fpgaChSEU[m_grp2Ch[i]/2];

  // individual PMTs 
  for (unsigned int j=0; j<m_grp1Ch.size(); ++j) {

    status = m_fpgaChSEU[m_grp1Ch[j]/2];

    std::string infoName = "LucrodBMALumiAlgoHit" + m_inputNames[m_grp1Ch[j]] + "_" + m_boardName;
    handleRawLumiPerBunchInfo(lumiBlock, infoName, arrayToVector(m_isData.chHit[m_grp1Ch[j]]), status, 1);
  }

  for (unsigned int j=0; j<m_grp2Ch.size(); ++j) {
  
    status = m_fpgaChSEU[ m_grp2Ch[j]/2];

    std::string infoName = "LucrodBMALumiAlgoHit" + m_inputNames[m_grp2Ch[j]] + "_" + m_boardName;
    handleRawLumiPerBunchInfo(lumiBlock, infoName, arrayToVector(m_isData.chHit[m_grp2Ch[j]]), status, 1);
  }
}

/***********************************************/
void ReadoutModuleLucrodBMA::handleRawLumiPerBunchInfo(unsigned int lumiBlock, std::string IsInfoName, std::vector<double> const& counts , unsigned int status, unsigned int numberOfSensors)
/***********************************************/
{
  const std::string entryname = m_is_server + "." + IsInfoName;
  
  LumiRawPerBunchNamed is_entry(m_ipcpartition, entryname);

  if (m_isData.orbitsInLB == 0xffffffff) is_entry.Quality = 1; // surely BAD!
  else                                   is_entry.Quality = status;
  
  is_entry.RunNumber     = m_runNumber;
  is_entry.LBN           = lumiBlock;
  is_entry.ChannelMask   = numberOfSensors;
  is_entry.NumberOfTurns = m_isData.orbitsInLB;
  is_entry.RawData       = counts;

  ostringstream text;

  try{ is_entry.checkin(); }
  catch(daq::is::Exception& ex) {
  
    text << "RMLucrod::" << m_boardName << "::handleRawLumiPerBunchInfo Could not publish to IS. DAQ Exception";
    ERS_REPORT_IMPL(ers::error, ers::Message, text.str(),);
  }
  catch(CORBA::SystemException& ex) {

    text << "RMLucrod::" << m_boardName << "::handleRawLumiPerBunchInfo Could not publish to IS. CORBA Exception";
    ERS_REPORT_IMPL(ers::error, ers::Message, text.str(),);
  }
  catch(std::exception& ex) {
    
    text << "RMLucrod::" << m_boardName << "::handleRawLumiPerBunchInfo Could not publish  to IS. STD Exception";
    ERS_REPORT_IMPL(ers::error, ers::Message, text.str(),);
  }
  catch(...) {
 
    text << "RMLucrod::" << m_boardName << "::handleRawLumiPerBunchInfo Could not publish to IS. Unknown exception";
    ERS_REPORT_IMPL(ers::error, ers::Message, text.str(),);
  }
}


/***********************************************/
void ReadoutModuleLucrodBMA::publishBaselineInfo()
/***********************************************/
{
  const std::string entryname = m_is_server + "." + m_boardName + "Baselines";

  LucrodBMABaselineInfoNamed is_entry(m_ipcpartition, entryname); 

  for (int i=0; i<NLUCRODIN; i++) {

    is_entry.BaselineMean.push_back(m_lucrodHistos.getBaseMean(i));
    is_entry.BaselineRMS.push_back (m_lucrodHistos.getBaseRms (i));
  }  

  try{ is_entry.checkin(); }
  catch(daq::is::Exception&     ex) { ERS_INFO("RMLucrod::" << m_boardName << "::publishBaselineInfo Could not publish baseline to IS. DAQ Exception"); }
  catch(CORBA::SystemException& ex) { ERS_INFO("RMLucrod::" << m_boardName << "::publishBaselineInfo Could not publish baseline to IS. CORBA Exception"); }
  catch(std::exception&         ex) { ERS_INFO("RMLucrod::" << m_boardName << "::publishBaselineInfo Could not publish baselines to IS. STD Exception"); }
  catch(...)                        { ERS_INFO("RMLucrod::" << m_boardName << "::publishBaselineInfo Could not publish baselines to IS. Unknown exception"); }
  
  is_entry.BaselineMean.clear();
  is_entry.BaselineRMS.clear();
}

/***********************************************/
void ReadoutModuleLucrodBMA::publishLucrodInfo()
/***********************************************/
{
  // IS stuff -- the IS value
  const std::string entryname = m_is_server + "." + m_boardName + "LucrodRegisters";
  
  ModuleLucrodBMAInfoNamed is_entry(m_ipcpartition, entryname);
  
  { // Fill Board information from registers
    boost::mutex::scoped_lock l(m_mutex);
    m_lucrodBoard.fillISInfo(m_boardName, is_entry);
  }
  
  is_entry.InputList               = m_inputNames;
  is_entry.ScopeEnabledChannelMask = m_scopeEnabledChannelMask;
  is_entry.LumiEnabledChannelMask  = m_lumiEnabledChannelMask;
  is_entry.ScopeEnabled            = m_scopeEnabled;

  ostringstream text; text << "RMLucrod::" << m_boardName << "::publishLucrodInfo Could not publish to IS.";
  
  try { is_entry.checkin(); } 
  catch(daq::is::Exception&     ex) { text << " DAQ Exception";     ERS_REPORT_IMPL(ers::error, ers::Message, text.str(),); } 
  catch(CORBA::SystemException& ex) { text << " CORBA Exception";   ERS_REPORT_IMPL(ers::error, ers::Message, text.str(),); } 
  catch(std::exception&         ex) { text << " STD Exception";     ERS_REPORT_IMPL(ers::error, ers::Message, text.str(),); } 
  catch(...)                        { text << " Unknown exception"; ERS_REPORT_IMPL(ers::error, ers::Message, text.str(),); }  
}

//FOR THE PLUGIN FACTORY
extern "C" {
  extern ReadoutModule* createReadoutModuleLucrodBMA();
}

ReadoutModule* createReadoutModuleLucrodBMA(){
  return (new ReadoutModuleLucrodBMA());
}
