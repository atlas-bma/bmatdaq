/********************************************************/
/*							*/
/* Date: October 2014    				*/ 
/* Author: C. Sbarra					*/
/*							*/
/* February 2020 adapt for FW version 2e                */
/***************************************************** **/

#include <iostream>
#include "ROSUtilities/ROSErrorReporting.h"
#include "DFDebug/DFDebug.h"
#include "DFSubSystemItem/Config.h"
#include "rcd_LUCRODBMA/vLucrod.h"
#include "rcd_LUCRODBMA/DataChannelLucrodScope.h"
#include "ers/ers.h"

using namespace ROS;
using namespace std;

/**********************************************************************************************/
DataChannelLucrodScope::DataChannelLucrodScope(u_int                    channelId, 
					       u_int                    channelIndex, 
					       u_int                    rolPhysicalAddress,
					       u_long                   vmeBaseVA, 
					       DFCountedPointer<Config> configuration,
					       sampleCollection&        collection, 
					       unsigned short           enabledChannelMask) :

  SingleFragmentDataChannel(channelId, channelIndex, rolPhysicalAddress, configuration),
  m_channelId              (channelId),
  m_channel_number         (rolPhysicalAddress),
  m_sampleCollection       (collection)
/**********************************************************************************************/
{ 
  m_vLUCROD = reinterpret_cast< LUCRODBoard_regs_t * >(vmeBaseVA);  // (virtual) base address of the channel

  m_min_wait = 999999;
  m_max_wait = 0;
 
  // determine how many channels we need to read in this run

  m_enabledChannelMask = enabledChannelMask;

  m_nchannels = 0;

  ostringstream msg;

  for (int ich=0; ich<16; ++ich) {

    int readit = (m_enabledChannelMask>>ich) & 1;
    
    if (readit == 1) { ++m_nchannels; msg << " Channel " << ich << " is enabled" << endl; }
  }
  
  ERS_LOG(msg.str());
  ERS_LOG("DataChannelLucrodScope::DataChannelLucrodScope channel mask is 0x" << HEX(m_enabledChannelMask) << "; channel number is " << m_nchannels);
  ERS_LOG("DataChannelLucrodScope::DataChannelLucrodScope channelId is "      << channelId                 << "; rolPhysAdd is "     << m_channel_number);
}

/*********************************/
DataChannelLucrodScope::~DataChannelLucrodScope() 
/*********************************/
{
}

/************************************************************************************************/
int DataChannelLucrodScope::getNextFragment(u_int* buffer, int max_size, u_int* status, unsigned long pciAddress)
/************************************************************************************************/
{
  u_int fsize = 0;
  u_int* bufPtr = buffer;
  vector<int> late;

  *bufPtr++ = m_channelId; // write the channel number
  fsize += 4;

  *bufPtr++ = 0xabcd; // insert a Header 
  fsize += 4;

  *bufPtr++ = m_nchannels; // write the number of channels to be read
  fsize += 4;
  
  for (int ich=0; ich<16; ++ich) { // loop over channels
    
    unsigned int ifpga = ich/2;
    unsigned int fifo = (ich%2 == 0) ?  1 : 0;

    if (((m_enabledChannelMask>>ich)&1) == 1) { // select enabled channels

      unsigned int timeout = 0;
      unsigned short value = (fifo == 0) ? m_vLUCROD->CHRegs[ifpga].selfTrig & 1 : (m_vLUCROD->CHRegs[ifpga].selfTrig & 2)>>1; 

      while (value == 0) { // wait for trigger
        
	timeout++;     
        
	if (timeout == 10000) { late.push_back(ich); break; }

        value = (fifo == 0) ? (m_vLUCROD->CHRegs[ifpga].selfTrig) & 1 : (m_vLUCROD->CHRegs[ifpga].selfTrig & 2)>>1 ;
      }
      
      if (timeout < m_min_wait) m_min_wait = timeout;
      if (timeout > m_max_wait) m_max_wait = timeout;
      
      if (timeout < 10000) {  
        
        m_vLUCROD->CHRegs[ifpga].control &= (fifo == 0) ? 0xfffe : 0xfffd; // block the buffer
        
        *bufPtr++ = 0xaa000000 | ich; // always read the triggering channel (neglect the other)
        fsize += 4;

        if (fifo == 0) { // fill the readout buffer

	  for (int i=0; i<NHDATA; ++i) {

	    unsigned int data = m_vLUCROD->CHRegs[ifpga].scope0_fifo;

	    m_sampleCollection.sum[ich][i] +=  (double)data;   

	    *bufPtr++ = data;
	    fsize += 4;
	  }
	}
	else {

	  for (int i=0; i<NHDATA; ++i) {

	    unsigned int data = m_vLUCROD->CHRegs[ifpga].scope1_fifo;
	    
	    m_sampleCollection.sum[ich][i] += (double)data;   
	    
	    *bufPtr++ = data;
	    fsize += 4;
	  }
	}
	
	*bufPtr++ = (fifo == 0) ? m_vLUCROD->CHRegs[ifpga].selfTrigger0Time : m_vLUCROD->CHRegs[ifpga].selfTrigger1Time;
        fsize += 4;       
        
        m_vLUCROD->CHRegs[ifpga].control |= (fifo == 0) ? 1 : 2; // reenable trigger
      }      
    }
  }

  *bufPtr++ = 0xff000000; // add a trailer 
  fsize += 4;

  m_sampleCollection.numberOfEvents++;

  unsigned int size = 4*(m_nchannels*66 + 4); // check the size: should be 66*nch+4 words

  if (size == fsize) *status = S_OK;
  else {
    
    ostringstream text; text << "DataChannelLucrodScope::getNextFragment Data size smaller than expected. Missing channels: ";
    
    for (unsigned int i=0; i<late.size(); ++i)
      text << late[i] << " "; 
    
    //ERS_REPORT_IMPL(ers::warning, ers::Message, text.str(),); 
    *status = S_TIMEOUT;
  }    

  return fsize; // bytes
}

/*************************************************/
DFCountedPointer<Config> DataChannelLucrodScope::getInfo() 
/*************************************************/
{
  DFCountedPointer<Config> info = Config::New();
  
  info->set("Number of enabled channels",                  m_nchannels);
  info->set("Min. number of polling cycles before DREADY", m_min_wait);
  info->set("Max. number of polling cycles before DREADY", m_max_wait);
  
  return(info);
}
