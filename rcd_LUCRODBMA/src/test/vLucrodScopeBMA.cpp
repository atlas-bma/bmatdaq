/************************************************************************/
/*                                                                      */
/* This is a simple prototyping program for the LUCROD BOARD             */
/*                                                                      */
/* Date:   5 Sept 2014                                                  */
/* Author: C. Sbarra                                                    */
/*                                                                      */
 
#include <sys/time.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <csignal>
#include <unistd.h>
#include <vector>
#include <math.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "cmem_rcc/cmem_rcc.h"
#include "rcc_time_stamp/tstamp.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "rcd_LUCRODBMA/vLucrod.h"

/**************/
/* Prototypes */
/**************/

std::vector<unsigned short> readRBF( std::string filename);
void checkADC();
void reloadChFirmWare(std::string filename);
void loadChannelFlash( unsigned short fpga);
void handleLBchange();
void readHitOR();
void readHitSum();
void readOneFpgaHits(int ifpga);
void readChHits();
void read1ChCharge(int ifpga);
void readChCharge();
void setChDefaults();
void dumpStatusWords();
void baselineSubDisable();
void baselineSubEnable();
void readTotalCharge();
void testTotalChargeBT();
void VMEMonitor();
void changeFifo();
void globalCommand(unsigned short code);
void setLemoOutput();
void showSelectedClock();
void setClock();
void setRegister();
void getRegister();
void mainFPGA_dump();
void VMEFPGA_dump();

void dumpChOffsets();
void dumpChThresholds();
void dumpChGains();
void dumpOutCopyGains();
void dumpOutCopyOffsets();
void setChOffset();
void setChThreshold();
void setChGain();
void setOutCopyGain();
void setOutCopyOffset();

int ttcrq();
void getFirmwareVersion();
void readOneOrbitCharge();
void readFifo();
void readWaveForm();
void VMEFlashTest();
void mainFlashTest();
void mainFlashSectorErase();
void chFlashTest();
void chFlashSlowTest();
void chFlashSectorRead();
void hitOrSumMenu();
void channelOutputMenu();
void totalChargeMenu();
void flashMenu();
void eraseVMEFlash();
int readVMEFlash();
void writeVMEFlash( std::string filename);
void eraseMainFlash();
int readMainFlash();
void writeMainFlash(std::string filename);
void eraseChFlash();
int readChFlash( unsigned short fpga, std::string rbfFilename);
void writeChFlash(std::string filename);
void dumpChSettings();
void setCh2Lumat();
void setSimuHit2Lumat();
void shiftChClockPhase();
void selfTrigger();
void selfTriggerFPGA();
void writeChFlashDatum(unsigned short byte, int fpga);
void writeMainFlashDatum(unsigned short byte);
void writeVMEFlashDatum(unsigned int byte);
unsigned short readChFlashDatum(int fpga);
unsigned short readMainFlashDatum();
unsigned int readVMEFlashDatum();

using namespace std;

/***********/
/* Globals */
/***********/

volatile LUCRODBoard_regs_t * vLUCROD;
uint32_t vLUCROD_base;
unsigned long m_cmem_desc_in_paddr;
unsigned long m_cmem_desc_in_vaddr;
unsigned int nFifoChanges;
  
/*****************************/
int main(int argc, char* argv[])
/*****************************/
{

  nFifoChanges = 0;
  unsigned int ret;

  ret = ts_open(1, TS_DUMMY);       //Open the library that we will use to measure time inervals
  if (ret){
    rcc_error_print(stdout, ret);   //This is a function of the ATLAS rcc_error package. 
    exit(-2);
  }

  // first of all, open the library that handles memory allocation to
  // prepare for VME block transfer

  ret = CMEM_Open();                
  if (ret) {
    rcc_error_print(stdout, ret);
    exit(-3);
  } 
 
  //Get a buffer for block read transfers
  int cmem_desc_in;
  ret = CMEM_SegmentAllocate(0x10000, (char *)"DMA_BUFFER", &cmem_desc_in);     
  if (ret) {
    rcc_error_print(stdout, ret);
    exit(-4);
  }

  ret = CMEM_SegmentPhysicalAddress(cmem_desc_in, &m_cmem_desc_in_paddr);
  if (ret){
    rcc_error_print(stdout, ret);
    exit(-4);
  }

  ret = CMEM_SegmentVirtualAddress(cmem_desc_in, &m_cmem_desc_in_vaddr);
  if (ret) {
    rcc_error_print(stdout, ret);
    exit(-4);
  }
  cout<<" address of m_cmem_desc_in_vaddr is 0x"<<std::hex<< m_cmem_desc_in_vaddr <<std::dec<<"\n";
  cout<<" address of m_cmem_desc_in_paddr is 0x"<<std::hex<< m_cmem_desc_in_paddr <<std::dec<<"\n";
  cout<<" DMA buffer (0x10000 bytes) starts at PCI address 0x"<< HEX(m_cmem_desc_in_paddr)<<"\n";


  unsigned int opt = VME_A24;
  cout<<" VME_A24 = "<<VME_A24<<"\n";
  cout<<" VME_A32 = "<<VME_A32<<"\n";
  cout<<"choose one\n";
  cin>>opt;
  u_int havebase = 0;
  vLUCROD_base = 0xA20000;
  int fun = 1;
  char c;
  // initialize the struct: address, window size, address_modifier, options
  VME_MasterMap_t master_map = {vLUCROD_base, 0x00001000, opt, 0};
 
  while ((c = getopt(argc, argv, "db:")) != -1)
  {
    switch (c)
    {
      case 'b':
      {
        vLUCROD_base  = strtol(optarg, 0, 16);
        havebase = 1;
      }
      break;
      case 'd': 
	cout<<"debug mode not ready\n"; 
	break;                  
      default:
        printf("Invalid option %c\n", c);
        printf("Usage: %s  [options]: \n", argv[0]);
        printf("Valid options are ...\n");
        printf("-b <VME base address>: The hexadecimal A24 base address of the VLUCROD\n");
        printf("-d:                        Dump registers and exit\n");
        printf("\n");
        exit(-1);
    }
  }
 
  if (!havebase)
  {
    cout<< "Enter the VMEbus base address of the VLUCROD: 0x \n";
    cin >> std::hex >> vLUCROD_base >> std::dec;
    cout<<" you selected BA 0x"<<HEX(vLUCROD_base)<<"\n";
    cout<<"Enter 1 to continue, 0 to stop\n";
    cin>>opt;
    if(opt == 0) return 0;
  }
  master_map.vmebus_address = vLUCROD_base;
 
  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  std::cout<<"VME_Open returned no errors"<<std::endl;
 
  int handle;
  ret = VME_MasterMap(&master_map, &handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  std::cout<<"VME_MasterMap returned no errors"<<std::endl;
 
  unsigned long value;
  ret = VME_MasterMapVirtualLongAddress(handle, &value);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  } 
  std::cout<<"VME_MasterMapVirtualAddress returned no errors. handle= 0x"<<std::hex<<handle<<" value = 0x"<<value<<std::dec<<"\n";
 
  vLUCROD = (LUCRODBoard_regs_t *) value;

//  //reset board
//  vLUCROD->MainRegs.control |= 0x4; 
//  usleep(1);
//
  //TTCrq clock: set it when present
  int i = ttcrq();
  if(i == 0){
    vLUCROD->MainRegs.control &= 0xfffe;
  }
  else{
    sleep(1);
    i = ttcrq();
    if(i == 0){
      vLUCROD->MainRegs.control &= 0xfffe;
    }
    else{
      cout<<"BEWARE: Still on INTERNAL clock...\n";
    }
  }


  //Stop and Clear all accumulators
  globalCommand(4);  // stop accumulators 
  globalCommand(5);  // clear hits, local charge and charge sum accumulators
  usleep(30);
  globalCommand(6);  // clear waveaff and 1-orbit charge fifo
 
  //select even channels for hit or/sum in FPGAV
  vLUCROD->MainRegs.ChOrSumEnabledMask = 0xff;

  //select channel 1 for fpga charges to be evaluated and sent to FPGAM and DISABLE baseline subtraction
  for(int i=0; i<8; ++i){
    vLUCROD->CHRegs[i].control |= 0x1008;
  }

  //set channel defaults (offsets, gain, threshols)
  //setChDefaults();
  dumpChGains();
  dumpChThresholds();
  dumpChOffsets();

  // make sure che clock is the requested one
  showSelectedClock();

  //start bcid-wise accumulators on all fpga
  globalCommand(2); 
  nFifoChanges = 1;

  cout<<"All BCID-wise accumulators are running, baseline subtraction disabled for charge. \n";  
  while (fun != 0){
  cout<<" Select an option:\n";  
    cout<<"\n";
    cout<<"   0 Exit\n";  
    cout<<"   1 Read Firmware Version\n";  
    cout<<"   2 check and Configure TTCrq\n";    
    cout<<"   3 Dump main FPGA registers\n";    
    cout<<"   4 Set lemo output\n";    
    cout<<"   5 Dump channel thresholds\n";    
    cout<<"   50 set channel thresholds\n";    
    cout<<"   6 Dump FADC gain \n";    
    cout<<"   60 set FADC gain \n";    
    cout<<"   7 Dump FADC offsets \n";    
    cout<<"   70 set FADC offsets \n";    
    cout<<"   8 Dump output copy offsets \n";    
    cout<<"   80 set output copy offsets \n";    
    cout<<"   9 Dump output copy gains \n";    
    cout<<"   90 Set output copy gains \n";    
    cout<<"  10 show selected clock \n";    
    cout<<"  11 select clock \n";    
    cout<<"  12 read one orbit charge \n";    
    cout<<"  13 read Fifo (loop on bcids)\n";    
    cout<<"  14 read waveform \n";    
    cout<<"  15 read fpgach charge \n";    
    cout<<"  16 flashMenu \n";    
    cout<<"  17 Reset Board (all registers to default) \n";    
    cout<<"  18 Dump status words \n";    
    cout<<"  19 disable baseline subtraction in channels \n";    
    cout<<"  20 enable baseline subtraction in channels \n";           
    cout<<"  21 Dump channel settings \n";    
    cout<<"  22 Enable hits to LUMAT \n";     
    cout<<"  23 Select channels to be sent to LUMAT \n";    
    cout<<"  24 Set simulation parameters \n";    
    cout<<"  25 read fpgach hits \n";    
    cout<<"  26 Shift clock phase by 45 degrees in CH fpga \n";    
    cout<<"  27 self Trigger \n"; 
    cout<<"  28 dump VME fpga registers\n";    
    cout<<"  29 changeFifo\n";    
    cout<<"  30 VMEMonitor\n";    
    cout<<"  31 Total Charge Menu (FPGAM)\n";    
    cout<<"  32 SetChannelDefaults\n";    
    cout<<"  33 Hit OR/SUM Menu (FPGAV)\n";    
    cout<<"  34 ShowBaselines\n";
    cout<<"  35 Channel Output Menu\n";
    cout<<"  39 handle LB change\n"; 
    cout<<"  40 reload FW of all channel FPGA \n"; 
    cout<<"  41 Check ADC \n"; 
    cout<<"make your choice :\n";   

    cin>>fun;
    cout<<" you chose option "<<fun<<"\n";

    switch (fun){
      
    case 1:
      getFirmwareVersion();
      break;

    case 2:
      {
	int i = ttcrq();
	cout <<i<<"\n";
	break;      
      }
    case 3:
      mainFPGA_dump();
      break;

    case 4:
      setLemoOutput();
      break;

    case 5:
      dumpChThresholds();
      break;

    case 50:
      setChThreshold();
      break;

    case 6:
      dumpChGains();
      break;

    case 60:
      setChGain();
      break;

    case 7:
      dumpChOffsets();
      break;

    case 70:
      setChOffset();
      break;

    case 8:
      dumpOutCopyOffsets();
      break;

    case 80:
      setOutCopyOffset();
      break;

    case 9:
      dumpOutCopyGains();
      break;

    case 90:
      setOutCopyGain();
      break;

    case 10:
      showSelectedClock();
      break;

    case 11:
      setClock();
      break;

    case 12:
      readOneOrbitCharge();
      break;

    case 13:
      readFifo();
      break;

    case 14:
      readWaveForm();
      break;

    case 15:
      readChCharge();
      break;

    case 25:
      readChHits();
      break;

    case 16:
      flashMenu();
      break;

    case 17:   
      {    
	vLUCROD->MainRegs.control |= 0x4;  // reset of all registers to default
	sleep(1);
        cout<<"Reset done\n";
	break;
      }
    case 18:
      dumpStatusWords();
      break;

    case 19:
      baselineSubDisable();
      break;

    case 20:
      baselineSubEnable();
      break;

    case 21:
      dumpChSettings();
      break;

    case 22:
      vLUCROD->MainRegs.lnkRW5 |= 0x200;
      break;

    case 23:
      setCh2Lumat();
      break;

    case 24:
      setSimuHit2Lumat();
      break;

    case 26:
      shiftChClockPhase();
      break;
      
    case 27:
      {
	int opt = 0;
	cout<<" enter 1 for a single channel, 0 for both\n";
	cin>>opt;
	if (opt == 1) 
	  selfTrigger();
	else
	  selfTriggerFPGA();
	break;
      }
    case 28:
      VMEFPGA_dump();
      break;

    case 29:
      changeFifo();
      break;

    case 30:
      VMEMonitor();
      break;

    case 31:
      totalChargeMenu();
      break;

    case 32:
      setChDefaults();
      break;

    case 33:
      hitOrSumMenu();
      break;

    case 34:
      {

	for(int i=0; i<8; ++i){
	  unsigned int baseStart = vLUCROD->CHRegs[i].baselineDef & 0xfff;
	  unsigned int baseSize = vLUCROD->CHRegs[i].baselineDef >> 12;
	  cout<<"baseline evaluated from BCID "<< baseStart <<" over "<<pow(2, baseSize) <<" bcids\n";
	  int ich0 = i*2 + 1;
	  int ich1 = i*2;
	  unsigned short b0 = vLUCROD->CHRegs[i].ch0Baseline;
	  unsigned short b1 = vLUCROD->CHRegs[i].ch1Baseline;
	  cout<<" ch "<<ich0<<" baseline is "<<b0<<"\n";
	  cout<<" ch "<<ich1<<" baseline is "<<b1<<"\n";
	}
	break;
      }

    case 35:
      channelOutputMenu();
      break;
      
    case 39:
      handleLBchange();
      break;

    case 40:
      {
	std::string rbfFilename;
	cout<<"Enter rbf filename\n";
	cin >> rbfFilename;
	cout<<" selected rbf file is "<<rbfFilename<<"\n";
	reloadChFirmWare(rbfFilename);
	break;
      }

    case 41:
      checkADC();
      break;
      
    default:
      break;
    }
  }
  
  ret = VME_MasterUnmap(handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
 
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);

  ret = CMEM_SegmentFree(cmem_desc_in);    //Return the first buffer
  if (ret) 
    rcc_error_print(stdout, ret);

  ret = CMEM_Close();                      //Close the memory allocation library
  if (ret)  
    rcc_error_print(stdout, ret);
     
  ret = ts_close(TS_DUMMY);                //Close the time stamping library  
  if (ret)
    rcc_error_print(stdout, ret);

  return 0;
}

/*****************/
void reloadChFirmWare( std::string rbfFilename)
/*****************/
{
  cout<<"Verifying integrity of FLASH FW, then loading it...\n";

  for(unsigned short ifpga = 0; ifpga<4 ; ++ifpga){
    int ok = readChFlash(ifpga*2, rbfFilename);
    if(ok == 0){
      loadChannelFlash(ifpga);
    }
    else{
      cout<<"FW in flash does not correspond to file. Doing nothing\n";
    }
  }

  cout<<"FW reload done. Please reset the board (opt 17)\n";
}

/*****************/
void checkADC()
/*****************/
{
  unsigned short gain = 0x600;
  cout<<"Enter ADC gain: 0x\n";
  cin>>std::hex>>gain;
  cout<<"selected value is 0x"<<std::hex<<gain<< "\n";

  // repeat for all FPGAa
  unsigned short ifpga = 0;
  while (ifpga < 8){
    cout<<" Analyzing FPGA"<< ifpga<<":\n";
    //set gain to minimum, offset = 0 for both channels
    vLUCROD->CHRegs[ifpga].ch1FadcGain = gain;
    vLUCROD->CHRegs[ifpga].ch0FadcGain = gain;
    vLUCROD->CHRegs[ifpga].ch0FadcOffset = 0x410;
    vLUCROD->CHRegs[ifpga].ch1FadcOffset = 0x410;
    sleep(1);
    std::vector <unsigned short> ch0, ch1;

    //select channel 0
    cout<<"Channel0 gain is 0x"<<std::hex<< vLUCROD->CHRegs[ifpga].ch0FadcGain<<", initial offset 0x"<<
      vLUCROD->CHRegs[ifpga].ch0FadcOffset <<std::dec<<"\n";
    vLUCROD->CHRegs[ifpga].control &= 0xEFFF;  
    unsigned short offset =  vLUCROD->CHRegs[ifpga].ch0FadcOffset;
    while (offset < 0xb81){
      unsigned short val = vLUCROD->CHRegs[ifpga].fadc_read;
      ch0.push_back(val);
      offset += 0x10; 
      vLUCROD->CHRegs[ifpga].ch0FadcOffset = offset;
      usleep(1000);
    }
    //select channel 1
    cout<<"Channel1 gain is 0x"<<std::hex<<vLUCROD->CHRegs[ifpga].ch1FadcGain<<", initial offset 0x"<<
      vLUCROD->CHRegs[ifpga].ch1FadcOffset <<std::dec<<"\n";
    vLUCROD->CHRegs[ifpga].control |= 0x1000;
    offset =  vLUCROD->CHRegs[ifpga].ch1FadcOffset;
    while (offset <0xb81){
      unsigned short val = vLUCROD->CHRegs[ifpga].fadc_read; 
      ch1.push_back(val);
      offset += 0x10; 
      vLUCROD->CHRegs[ifpga].ch1FadcOffset = offset;
      usleep(1000);
    }
    //ch0, ch1 ought to show a linear ramp
    std::cout<< "FPGA "<<ifpga<<" Channel 0:\n";
    for (unsigned int i = 0; i< ch0.size(); ++i){
      std::cout<<ch0[i]<<" ";
    }
    std::cout<< "\nFPGA "<<ifpga<<" Channel 1:\n";
    for (unsigned int i = 0; i< ch1.size(); ++i){
      std::cout<<ch1[i]<<" ";
    }
    std::cout<<"\n";
    ++ifpga;
  }
}

/*****************/
void setChDefaults()
/*****************/
{
  // offsets, gains, thresholds

  for(int i=0; i<8;++i){
    vLUCROD->CHRegs[i].ch0FadcGain = 0x600;
    vLUCROD->CHRegs[i].ch1FadcGain = 0x600;
    vLUCROD->CHRegs[i].ch0FadcOffset = 0x460;
    vLUCROD->CHRegs[i].ch1FadcOffset = 0x460;
    vLUCROD->CHRegs[i].ch0OutGain = 0x600;
    vLUCROD->CHRegs[i].ch1OutGain = 0x600;
    vLUCROD->CHRegs[i].ch0OutOffset = 0x800;
    vLUCROD->CHRegs[i].ch1OutOffset = 0x800;
    unsigned short val = 150;
    vLUCROD->CHRegs[i].ch0FadcThreshold = val;
    vLUCROD->CHRegs[i].ch1FadcThreshold = val;
  }
}
/*****************/
void channelOutputMenu()
/*****************/
{
  int fun = 100;
  while (fun != 0){
    cout << "\n";
    cout << "Select an option:\n";  
    cout<<"  0 return\n";    
    cout<<"  1 set Gain of all channels\n";    
    cout<<"  2 set Gain of a single channels\n";    
    cout<<"  3 set offset of all channels\n";    
    cout<<"  4 set offset of a single channels\n";    
    cout<<"make your choice :\n";   
    cin>>fun;
    switch(fun){

    case 1:
      {
	unsigned short gain = 0;
	cout<<"Enter Gain 0x\n";
	cin >> std::hex >> gain >> std::dec;
        cout<<" writing the selected the value 0x"<<HEX(gain)<<"\n";
	for(int i = 0; i<8; ++i){
	  vLUCROD->CHRegs[i].ch1OutGain = gain;
	  vLUCROD->CHRegs[i].ch0OutGain = gain;
	}
	break;
      } 
    case 2:
      {
	unsigned short gain = 0;
	cout<<"Enter Gain 0x\n";
	cin >> std::hex >> gain >> std::dec;
	cout<<"Enter Channel\n";
	unsigned short ich = 0;
        cin>>ich;
	if(ich%2 == 0){
	  vLUCROD->CHRegs[ich/2].ch1OutGain = gain;
	  cout<<" reading back 0x"<<HEX(vLUCROD->CHRegs[ich/2].ch1OutGain)<<"\n";
	}
	else{
	  vLUCROD->CHRegs[ich/2].ch0OutGain = gain;	
	  cout<<" reading back 0x"<<HEX(vLUCROD->CHRegs[ich/2].ch0OutGain)<<"\n";
	}
	break;
      } 
    case 3:
      {
	unsigned short offset = 0;
	cout<<"Enter Offset 0x\n";
	cin >> std::hex >> offset >> std::dec;
        cout<<" writing the selected the value 0x"<<HEX(offset)<<"\n";
	for(int i = 0; i<8; ++i){
	  vLUCROD->CHRegs[i].ch1OutOffset = offset;
	  vLUCROD->CHRegs[i].ch0OutOffset = offset;
	}
	break;
      }
     case 4:
      {
	unsigned short offset = 0;
	cout<<"Enter offset 0x\n";
	cin >> std::hex >> offset >> std::dec;
	cout<<"Enter Channel\n";
	unsigned short ich=0;
        cin>>ich;
	if(ich%2 == 0){
	  vLUCROD->CHRegs[ich/2].ch1OutOffset = offset;
	  cout<<" reading back 0x"<<HEX(vLUCROD->CHRegs[ich/2].ch1OutOffset)<<"\n";
	}
	else{
	  vLUCROD->CHRegs[ich/2].ch0OutOffset = offset;	
	  cout<<" reading back 0x"<<HEX(vLUCROD->CHRegs[ich/2].ch0OutOffset)<<"\n";
	}
	break;
      } 
    }
  }
}
/*****************/
void hitOrSumMenu()
/*****************/
{
  int fun = 100;
  while (fun != 0){
    cout << "\n";
    cout << "Select an option:\n";  
    cout<<"  0 return\n";    
    cout<<"  1 Dump channel mask\n";    
    cout<<"  2 Set ChannelMask\n";    
    cout<<"  3 Set HIT OR/SUMO rbit Delay\n";    
    cout<<"  4 Clear bcid-wise Fifos\n";    
    cout<<"  5 Read Hit OR fifos \n";    
    cout<<"  6 Read Hit Sums  \n";    
    cout<<"make your choice :\n";   
    cin>>fun;
    switch(fun){

    case 1:
      cout<<" Hit OR/Sum Enabled Channel mask 0x"<<HEX(vLUCROD->MainRegs.ChOrSumEnabledMask)<<"\n";
      break;
     
    case 2:
      {
	cout<<"Enable ch1 (1) or 0 (0) in each fpga:\n";
	unsigned int mask = 0;
	for(int i = 0; i<8; ++i){
	  unsigned int opt = 0;
	  cout<<"FPGA "<<i<<" ?\n";
	  cin>> opt;
	  mask |= (opt<<i);
	}
	cout<<"Selected Mask is 0x"<<HEX(mask)<<"\n";
        vLUCROD->MainRegs.ChOrSumEnabledMask = mask;
	break;
      }

    case 3:
      {
	unsigned int opt = 0;
	cout<<" Enter delay\n";
	cin>>opt;
	vLUCROD->MainRegs.HitOrSumOrbitDelay = opt;
	break;
      }

    case 4:
      globalCommand(5);
      break;

    case 5:
      readHitOR();
      break;

    case 6:
      readHitSum();
      break;


    default:
      break;
    }
  }
}
/*****************/
void readHitOR()
/*****************/
{
  //read
  unsigned int OrbitCountsLSW = vLUCROD->MainRegs.orbCountLSW;
  unsigned int OrbitCountsMSW = vLUCROD->MainRegs.orbCountMSW;
  unsigned int OrbitCounts = OrbitCountsLSW | (OrbitCountsMSW<<16);
  cout<<" Orbit counts are: ="<<OrbitCounts<<"\n"; 
  unsigned int Grp1HitOr[3564];
  unsigned int Grp2HitOr[3564];
  double max1 = -1.;
  double max2 = -1.;
  double min1 = 1000000000.;
  double min2 = 1000000000.;
  unsigned int bcid1 = 0;
  unsigned int bcid2 = 0;
  unsigned int minbcid1 = 0;
  unsigned int minbcid2 = 0;
  for(int i= 0; i<3564; ++i){
    unsigned int LSW =  vLUCROD->MainRegs.grp1hitOR;
    unsigned int MSW =  vLUCROD->MainRegs.grp1hitOR;
    if( (MSW>>15) != 1 )cout<<" No marker for BCID "<<i<<" reading hitOR gruppo 1 (LSW = 0x"<<HEX(LSW)<<
      " MSW = 0x"<<HEX(MSW)<<")\n";
    Grp1HitOr[i] = LSW | ((MSW&0x7fff)<<16);
    LSW =  vLUCROD->MainRegs.grp2hitOR;
    MSW =  vLUCROD->MainRegs.grp2hitOR;
    if( (MSW>>15) != 1 )cout<<" No marker for BCID "<<i<<" reading hitOR gruppo 2\n";
    Grp2HitOr[i] = LSW | ((MSW&0x7fff)<<16);
    if(Grp1HitOr[i] > max1){
      max1 = (double)Grp1HitOr[i];
      bcid1 = i;
    }
    if(Grp2HitOr[i] > max2){
      max2 = (double)Grp2HitOr[i];
      bcid2 = i;
    }
    if(Grp1HitOr[i] < min1){
      min1 = (double)Grp1HitOr[i];
      minbcid1 = i;
    }
    if(Grp2HitOr[i] < min2){
      min2 = (double)Grp2HitOr[i];
      minbcid2 = i;
    }
  }
  cout<<" Maximum in group 1 is "<< max1 <<" in bcid "<<bcid1<<"\n";
  cout<<" Maximum in group 2 is "<< max2 <<" in bcid "<<bcid2<<"\n";
  cout<<" Minimum in group 1 is "<< min1 <<" in bcid "<<minbcid1<<"\n";
  cout<<" Minimum in group 2 is "<< min2 <<" in bcid "<<minbcid2<<"\n";
  
}
/*****************/
void readHitSum()
/*****************/
{
  unsigned int OrbitCountsLSW = vLUCROD->MainRegs.orbCountLSW;
  unsigned int OrbitCountsMSW = vLUCROD->MainRegs.orbCountMSW;
  unsigned int OrbitCounts = OrbitCountsLSW | (OrbitCountsMSW<<16);
  cout<<" Orbit counts are: ="<<OrbitCounts<<"\n"; 
  unsigned int Grp1HitSum[3564];
  unsigned int Grp2HitSum[3564];
  double max1 = -1.;
  double max2 = -1.;
  double min1 = 1000000000.;
  double min2 = 1000000000.;
  unsigned int bcid1 = 0;
  unsigned int bcid2 = 0;
  unsigned int minbcid1 = 0;
  unsigned int minbcid2 = 0;
  for(int i= 0; i<3564; ++i){
    unsigned int LSW =  vLUCROD->MainRegs.grp1hitSum;
    unsigned int MSW =  vLUCROD->MainRegs.grp1hitSum;
    if( (MSW>>15) != 1 )cout<<" No marker for BCID "<<i<<" reading hitSum gruppo 1\n";
    Grp1HitSum[i] = LSW | ((MSW&0x7fff)<<16);
    LSW =  vLUCROD->MainRegs.grp2hitSum;
    MSW =  vLUCROD->MainRegs.grp2hitSum;
    if( (MSW>>15) != 1 )cout<<" No marker for BCID "<<i<<" reading hitSum gruppo 2\n";
    Grp2HitSum[i] = LSW | ((MSW&0x7fff)<<16);
    if(Grp1HitSum[i] > max1){
      max1 = (double)Grp1HitSum[i];
      bcid1 = i;
    }
    if(Grp2HitSum[i] > max2){
      max2 = (double)Grp2HitSum[i];
      bcid2 = i;
    }
    if(Grp1HitSum[i] < min1){
      min1 = (double)Grp1HitSum[i];
      minbcid1 = i;
    }
    if(Grp2HitSum[i] < min2){
      min2 = (double)Grp2HitSum[i];
      minbcid2 = i;
    }
  }
  cout<<" Maximum in group 1 is "<< max1 <<" in bcid "<<bcid1<<"\n";
  cout<<" Maximum in group 2 is "<< max2 <<" in bcid "<<bcid2<<"\n";
  cout<<" Minimum in group 1 is "<< min1 <<" in bcid "<<minbcid1<<"\n";
  cout<<" Minimum in group 2 is "<< min2 <<" in bcid "<<minbcid2<<"\n";
}
/*****************/
void totalChargeMenu()
/*****************/
{
  int fun = 100;
  while (fun != 0){
    cout << "\n";
    cout << "Select an option:\n";  
    cout<<"  1 Stop Charge acquision\n";    
    cout<<"  2 Enable Charge acquision\n";    
    cout<<"  3 Change Fifo\n";    
    cout<<"  4 Clear bcid-wise Fifos\n";    
    cout<<"  5 Read total Charge   (1 work/read)\n";    
    cout<<"  6 Read total Charge (block transfer)\n";    
    cout<<"  7 Disable Baseline subtraction\n";    
    cout<<"  8 Enable  test pattern\n";    
    cout<<"  9 Disable  test pattern\n";    
    cout<<" 10 Dump deserialiser debug words\n";    
    cout<<" 11 Dump sums of charges of groups of 4 fpga\n";    
    cout<<" 12 Select channel (0/1) for charge sum\n";    
    cout<<" 13 dump charge sum sent to FPGAM\n";    
    cout<<"make your choice :\n";   
    cin>>fun;
    switch(fun){

    case 1:
      globalCommand(4);
      break;

    case 2:
      globalCommand(2);
      break;

    case 3:
      changeFifo();	
      break;    

    case 4:
      globalCommand(5);
      break;

    case 5:      
      readTotalCharge();
      break;

    case 6:      
      testTotalChargeBT();
      break;
	
    case 7:
      baselineSubDisable();
      break;
      
    case 8:{
      for(int i=0; i<8; ++i){
        unsigned short enable = 0x2000;
	vLUCROD->CHRegs[i].control |= enable;
	cout<<" control word  reads 0x"<<HEX(vLUCROD->CHRegs[i].control)<<"\n";
      }
      break;
    }
 
    case 9:{
      for(int i=0; i<8; ++i){
        unsigned short enable = 0x2000;
	vLUCROD->CHRegs[i].control &= ~enable;
	cout<<" control word  reads 0x"<<HEX(vLUCROD->CHRegs[i].control)<<"\n";
      }
      break;
    }
    case 10:{
      cout<<"Serialiser debug 0 reads 0x"<<HEX(vLUCROD->VMERegs.serialDebug0)<<"\n";
      cout<<"Serialiser debug 1 reads 0x"<<HEX(vLUCROD->VMERegs.serialDebug1)<<"\n";
      cout<<"Serialiser debug 2 reads 0x"<<HEX(vLUCROD->VMERegs.serialDebug2)<<"\n";
      cout<<"Serialiser debug 3 reads 0x"<<HEX(vLUCROD->VMERegs.serialDebug3)<<"\n";
      cout<<"Serialiser debug 4 reads 0x"<<HEX(vLUCROD->VMERegs.serialDebug4)<<"\n";
      cout<<"Serialiser debug 5 reads 0x"<<HEX(vLUCROD->VMERegs.serialDebug5)<<"\n";
      cout<<"Serialiser debug 6 reads 0x"<<HEX(vLUCROD->VMERegs.serialDebug6)<<"\n";
      cout<<"Serialiser debug 7 reads 0x"<<HEX(vLUCROD->VMERegs.serialDebug7)<<"\n";
      break;
    }
    case 11:{
      cout<<"charge sum 1 reads 0x"<<HEX(vLUCROD->VMERegs.chargeDbg1)<<"\n";
      cout<<"charge sum 2 0x"<<HEX(vLUCROD->VMERegs.chargeDbg2)<<"\n";
      break;
    }
      
    case 12:{
      unsigned short ch1= 0x1000; 
      cout<<"Select channel to be sent to charge sum (0/1)\n";
      unsigned short opt;
      cin>>opt;
      (opt == 1) ? vLUCROD->VMERegs.control |= ch1 : vLUCROD->VMERegs.control &= ~ch1;  
      break;
    }
    
    case 13:{
      for(int i=0; i<7; ++i){
	cout<<" charge sent to fpgaM from FPGACH "<<i<<" is 0x"<<HEX(vLUCROD->CHRegs[i].oneBCIDCharge)<<" ("<<vLUCROD->CHRegs[i].oneBCIDCharge<<")\n";	
      }
      break;
      }

    default:
      break;
    }
  }
}
/*****************/
void baselineSubDisable()
/*****************/
{
  for(int i=0; i<7; ++i){
    vLUCROD->CHRegs[i].control |= 0x8;
    cout<<" control word after switching bit 3 on reads 0x"<<HEX(vLUCROD->CHRegs[i].control)<<"\n";
  }
}
/*****************/
void baselineSubEnable()
/*****************/
{
  for(int i=0; i<7; ++i){
    vLUCROD->CHRegs[i].control &= 0xfff7;
    cout<<" control word after switching bit 3 on reads 0x"<<HEX(vLUCROD->CHRegs[i].control)<<"\n";
  }
}
/*****************/
void testTotalChargeBT()
/*****************/
{
  tstamp ts1, ts2;
  //read
  unsigned int OrbitCounts1 = vLUCROD->VMERegs.orbitCount1;
  unsigned int OrbitCounts2 = vLUCROD->VMERegs.orbitCount2;
  cout<<" Orbit counts are: 1="<<OrbitCounts1<<" and 2="<<OrbitCounts2<<"\n"; 
  
  //read the buffer via block transfer now
  unsigned int sizeReq = 32; // 3565*4*2 bytes = 28512 ma non sta in un blocco solo!
  //unsigned int addr  = vLUCROD_base+0xe34;  
  //unsigned int addr  = 0xa00e60;  
  //unsigned int addr  = 0xa00e38;  
  unsigned int addr  = vLUCROD_base+0xe60;
  bool goon = true;
  while (goon){
    cout<<"enter number of bytes to be trasferred\n";
    cin>>sizeReq;
    unsigned int nbcid = sizeReq/16;
    sizeReq = nbcid*16;
    
    cout<<" trying to read "<<sizeReq<<" bytes from address 0x"<<HEX(addr)<<"\n";
    
    VME_BlockTransferList_t blist;
    blist.number_of_items = 1;
    blist.list_of_items[0].vmebus_address       = addr;
    blist.list_of_items[0].system_iobus_address = m_cmem_desc_in_paddr;   
    blist.list_of_items[0].size_requested       = sizeReq;               
    blist.list_of_items[0].control_word         = VME_DMA_D32R;        
    
    //read buffer    
    ts_clock(&ts1); 
    unsigned int ret = VME_BlockTransfer(&blist, 1000);
    ts_clock(&ts2); 
    float delta = ts_duration(ts1, ts2);
    cout<<"Delay of D32 BLT read = "<<delta*1000000 << " usecs\n";   
    if (ret != VME_SUCCESS){
      VME_ErrorPrint(ret);
      cout<<"VME_DMAERR is "<<VME_DMAERR<<"\n";
      if (ret == VME_DMAERR){
	printf("Block read:\n");
	cout<<"Status: "<<blist.list_of_items[0].status_word<<"\n";
	cout<<"Bytes remaining: "<<blist.list_of_items[0].size_remaining<<"\n";
      }
    }  
    //buffer content
    unsigned int *data_ptr;
    data_ptr = (unsigned int *)m_cmem_desc_in_vaddr;
    for(int i=0; i<3564; ++i){
      uint64_t datum = data_ptr[2*i+1]&0xff;
      datum = (datum<<32) | data_ptr[2*i];
      cout<<" BCID "<<i<<" tot = 0x"<<HEX(datum)<<" LSB 0x"<<HEX(data_ptr[2*i])<<" MSB 0x"<<HEX(data_ptr[2*i+1])<<"\n";	    	    
    }	
    
    cout<<" continue ? 1=yes, 0=n0)\n";
    cin>>goon;
    
  }
  
}
/*****************/
void readTotalCharge()
/*****************/
{	

  //baselines 
  std::vector <double> b0, b1;
  for(int i=0; i<8; ++i){
    unsigned short v0 = vLUCROD->CHRegs[i].ch0Baseline;
    unsigned short v1 = vLUCROD->CHRegs[i].ch1Baseline;
    b0.push_back( (double)v0 );
    b1.push_back( (double)v1 );
  }
  
  //orbit Counts
  unsigned int OrbitCounts1 = vLUCROD->VMERegs.orbitCount1;
  unsigned int OrbitCounts2 = vLUCROD->VMERegs.orbitCount2;
  cout<<" \n *** SUPERCHARGE Orbit counts are: 1="<<OrbitCounts1<<" and 2="<<OrbitCounts2<<"\n"; 

  std::vector <double> charge1, charge2;
  unsigned int nErrQ1=0;
  unsigned int nErrQ2=0;

  std::vector <unsigned int> nZero1;
  std::vector<unsigned int> nZero2;

  unsigned long long  q11, q12, q21, q22;
  
  double min1 = 1.e+9;
  double min2 = 1.e+9;
  double max1 = 0;
  double max2 = 0;

  int minbcid1 = 0;
  int minbcid2 = 0;
  int maxbcid1 = 0;
  int maxbcid2 = 0;

  for(int i=0; i<3564; ++i){

    q11 = vLUCROD->VMERegs.chargeSum1;
    q12 = vLUCROD->VMERegs.chargeSum1;
    q21 = vLUCROD->VMERegs.chargeSum2;
    q22 = vLUCROD->VMERegs.chargeSum2;

    if( q12>>31 != 1){
      nErrQ1++;
    }
    if( q22>>31 != 1){
      nErrQ2++;
    }
    
    double qtot1 = (double)(q11 | (q12&0xff)<<32);
    charge1.push_back(qtot1);

    double qtot2 = q21 + (q22&0xff)*4294967296.;
    charge2.push_back(qtot2);

    if(qtot1 == 0){
      nZero1.push_back(i);
    }
    else{

      if(min1 > qtot1){
	min1 = qtot1;
	minbcid1 = i;
      }
      if(max1 < qtot1){
      max1 = qtot1;
      maxbcid1 = i;
      }
    }
      
    if(qtot2 == 0){
      nZero2.push_back(i);
    }
    else{
      if(min2 > qtot2){
	min2 = qtot2;
	minbcid2 = i;
      }
      
      if(max2 < qtot2){
	max2 = qtot2;
	maxbcid2 = i;
      }

    }
  }

  if( (nErrQ1>0) || (nErrQ2 > 0))
    cout<<" missing markers in gruppo 1:"<<nErrQ1<<"; gruppo 2:"<<nErrQ2<<"\n";

  if( (nZero1.size() > 0) || (nZero2.size() > 0)){
    cout<<" ***BCID with 0 charge in group 1:"<<nZero1.size()<<"; gruppo 2:"<<nZero2.size()<<"\n";
    for(unsigned int si=0; si < nZero1.size(); ++si){
      cout<<nZero1[si]<<" ";
    }
    cout<<"\n";
    for(unsigned int si= 0; si < nZero2.size(); ++si){
      cout<<nZero2[si]<<" ";
    }
    cout<<"\n";

  }

  // compare max, min to estimated based on baseline
  //expected charges
  double Qest1 = (b1[0]+b1[1]+b1[2]+b1[3])*8.*OrbitCounts1;
  double Qest2 = (b1[4]+b1[5]+b1[6]+b1[7])*8.*OrbitCounts1;
 
  cout<<" total charge gruppo 1 max is "<<max1<<" found in bcid "<<maxbcid1<<"\n";
  cout<<" total charge gruppo 2 max is "<<max2<<" found in bcid "<<maxbcid2<<"\n";
  cout<<" total charge gruppo 1 min is "<<min1<<" found in bcid "<<minbcid1<<"\n";
  cout<<" total charge gruppo 2 min is "<<min2<<" found in bcid "<<minbcid2<<"\n";
  cout<<" expected charges from baseline are: gruppo 1: "<<Qest1<<"; gruppo 2: "<<Qest2<<"\n";
      
}
 
/*****************/
void flashMenu()
/*****************/
{

  int fun = 100;
  std::string filename;

  while (fun != 0){
    cout << "\n";
    cout << "Select an option:\n";  
    cout<<"  0 Back to main menu \n";    
    cout<<"  1 Erase Main Flash \n";    
    cout<<"  2 Read Main Flash \n";    
    cout<<"  3 Write Main Flash \n";    
    cout<<"  4 LOAD Main Flash \n";    
    cout<<"  5 Erase Channel Flash \n";    
    cout<<"  6 Read Channel Flash \n";    
    cout<<"  7 Write Channel Flash \n";    
    cout<<"  8 LOAD Channel Flash \n";
    cout<<"  9 Erase VME Flash \n";    
    cout<<" 10 Read VME Flash \n";    
    cout<<" 11 Write VME Flash \n";    
    cout<<" 12 LOAD VME Flash \n";    
    cout<<" 13 1-Sector Main Flash Test \n";    
    cout<<" 14 1-Sector Channel Flash Test \n";    
    cout<<" 15 1-Sector VME Flash Test \n";    
    cout<<" 16 1-Sector Channel Flash Read \n";    
    cout<<" 17 1-Sector Main Flash erase \n";    
    cout<<"make your choice :\n";   
    cin>>fun;
    switch(fun){
    case 1:
      eraseMainFlash();
      break;
    case 2: 
      {
	readMainFlash();
	break;
      }
    case 3:
      {
	std::string filename;
	std::cout<<"enter rbf filename\n";
	cin>>filename;
	writeMainFlash(filename);
	break;
      }
    case 4:
      vLUCROD->MainRegs.control |= 0x8;
      break;
    case 5:
      eraseChFlash();
      break;
    case 6:
      {
	std::cout<<"select channel pair (0-2-4-6)\n";
	unsigned short fpga = 0;
	std::string rbfFilename;
	cin >> fpga;
	std::cout<<"enter rbf filename\n";
        cin >>rbfFilename;
	  cout<<" Reading flash for FPGA "<<fpga<<" and "<<fpga+1<<"; comaring to"<<rbfFilename<<"\n";
	readChFlash(fpga, rbfFilename);
	break;
      }
    case 7:
      {
	std::string filename;
	std::cout<<"enter rbf filename\n";
	cin>>filename;
	writeChFlash(filename);
	break;
      }
    case 8:
      {
	std::cout<<"select channel pair (0-2-4-6)\n";
	unsigned short fpga = 0;
	cin >> fpga;
        loadChannelFlash(fpga);
	break;
      }
    case 9:
      eraseVMEFlash();
      break;
    case 10:
      {
	readVMEFlash();
	break;
      }
    case 11:
      {
	std::string filename;
	std::cout<<"enter rbf filename\n";
	cin>>filename;
	writeVMEFlash(filename);
	break;
      }
    case 12:
      vLUCROD->VMERegs.control |= 0x8;
      break;
    case 13:
      mainFlashTest();
      break;
    case 14:
      chFlashTest();
      break;
    case 15:
      VMEFlashTest();
      break;
    case 16:
      chFlashSectorRead();
    case 17:
      mainFlashSectorErase();
      break;
    case 18:
      chFlashSlowTest();
    default:
      break;
    }
  }
}
/*****************/
void loadChannelFlash(unsigned short fpga)
/*****************/
{ 
  std::cout<<"loading flash in fpga "<<fpga<<" and "<<fpga+1<<"\n";
  vLUCROD->CHRegs[fpga].control2 |= 1;
  vLUCROD->CHRegs[fpga].control |= 0x20;
  usleep(5000);
  vLUCROD->CHRegs[fpga].control2 = 0;  
}
/*****************/
void SetCHFlashAdd(unsigned int add, int fpga)
/*****************/
{ 
  //  cout <<"startAdd now  is 0x"<<HEX(add)<<"\n";
  vLUCROD->CHRegs[fpga].flashLSB = add & 0xffff;
  vLUCROD->CHRegs[fpga].flashMSB = add>>16; 
  usleep(2000);
  cout <<"LSB is 0x"<<HEX(vLUCROD->CHRegs[fpga].flashLSB)<<
      " MSB is 0x"<<HEX(vLUCROD->CHRegs[fpga].flashMSB)<<"\n";
}
/*****************/
void SetMainFlashAdd(unsigned int add)
/*****************/
{ 
  //  cout <<"startAdd now  is 0x"<<HEX(add)<<"\n";
  vLUCROD->MainRegs.flashLSB = add & 0xffff;
  vLUCROD->MainRegs.flashMSB = add>>16;
  usleep(1000);     
  cout <<"LSB is 0x"<<HEX(vLUCROD->MainRegs.flashLSB)<<
      " MSB is 0x"<<HEX(vLUCROD->MainRegs.flashMSB)<<"\n";
}
/*****************/
void SetVMEFlashAdd(unsigned int add)
/*****************/
{ 
  //  cout <<"startAdd now  is 0x"<<HEX(add)<<"\n";
  vLUCROD->VMERegs.flashLSB = add & 0xffff;
  vLUCROD->VMERegs.flashMSB = add>>16;
  usleep(1000);     
  cout <<"LSB is 0x"<<HEX(vLUCROD->VMERegs.flashLSB)<<
    " MSB is 0x"<<HEX(vLUCROD->VMERegs.flashMSB)<<"\n";
}
/*****************/
void eraseMainFlashSector(unsigned short sector)
/*****************/
{
  // sector address
  unsigned int add = sector<<16;
  SetMainFlashAdd(add);  
    //erase sector
  vLUCROD->MainRegs.flashData = 0x200;
  while((vLUCROD->MainRegs.flashStatus & 1) == 1)
    continue;
  cout<<" Flash sector "<< sector <<" has been deleted\n";
}
/*****************/
void mainFlashSectorErase()
/*****************/
{
  cout<<" select sector \n";
  unsigned int sector = 0;
  cin >> sector;
  cout <<"erasing main flash sector "<<sector<<"\n";
  eraseMainFlashSector(sector);
}
/*****************/
void eraseVMEFlashSector(unsigned int sector)
/*****************/
{
  // sector address
  unsigned int add = sector<<16;
  SetVMEFlashAdd(add);  
    //erase sector
  vLUCROD->VMERegs.flashData = 0x200;
  while((vLUCROD->VMERegs.flashStatus & 1) == 1)
    continue;
  cout<<" Flash sector "<< sector <<" has been deleted\n";
}
/*****************/
void eraseCHFlashSector(unsigned short sector, int fpga)
/*****************/
{
  // sector address
  unsigned int add = sector<<16;
  SetCHFlashAdd(add, fpga);  
  // erase sector
  vLUCROD->CHRegs[fpga].flashData = 0x200;
  while((vLUCROD->CHRegs[fpga].flashStatus & 1) == 1)
    continue;
  cout<<" Flash Sector "<<sector<<" has been deleted\n";
}
/*****************/
unsigned short LoadCHFlashChunk(int fpga)
/*****************/
{  
  //load flash data from fifo
  vLUCROD->CHRegs[fpga].flashData = 0x100;
  usleep(6000);

  //check status
  unsigned short val = vLUCROD->CHRegs[fpga].flashStatus;  
  while((val & 1) == 1)
     val = vLUCROD->CHRegs[fpga].flashStatus;
 
  return val; 
}
/*****************/
unsigned short LoadMainFlashChunk()
/*****************/
{ 
  //load flash data from fifo
  vLUCROD->MainRegs.flashData = 0x100;
  //check status
  unsigned short val = vLUCROD->MainRegs.flashStatus;
  while((val & 1) == 1)
    val = vLUCROD->MainRegs.flashStatus;  
  return val; 
}
/*****************/
unsigned int LoadVMEFlashChunk()
/*****************/
{ 
  //load flash data from fifo
  vLUCROD->VMERegs.flashData = 0x100;
  //check status  
  unsigned int val = vLUCROD->VMERegs.flashStatus;
  while((val & 1) == 1)
    val = vLUCROD->VMERegs.flashStatus;

  return val; 
}
/*****************/
void chFlashSectorRead()
/*****************/
{
  cout<<" select channel flash (0-2-4-6) \n";
  unsigned int fpga = 0;
  cin >> fpga;
  if( (fpga != 0) && (fpga != 2) && (fpga != 4) && (fpga != 6)){
    cout<<"wrong choice. Exiting...\n";
    return;
  }
  cout<<"Reading flash for fpga "<< fpga <<" in  in sector 0x40\n";

  cout<<" select sector \n";
  unsigned int sector = 0;
  cin >> sector;
  sector <<= 16;

  cout<<"Reading flash for fpga "<< fpga <<" in sector 0x"<<HEX(sector)<<"\n";
  cout<<"Enable bit swappig (1== YES)? \n";
  int opt = 0;

  cin >> opt;
  
  if(opt == 1)
    //enable bit swapping
    vLUCROD->CHRegs[fpga].control |= 0x10;
  else
    vLUCROD->CHRegs[fpga].control &= 0xffef;

  SetCHFlashAdd(sector, fpga);
 
  for(int i=0; i<512; ++i){
    unsigned short val = readChFlashDatum(fpga);
    cout<<i<<": reading from flash 0x "<<HEX(val)<<"\n";
  }

}
/*****************/
void chFlashSlowTest()
/*****************/
{
  cout<<" select channel flash (0-2-4-6) \n";
  unsigned int fpga = 0;
  cin >> fpga;
  if( (fpga != 0) && (fpga != 2) && (fpga != 4) && (fpga != 6)){
    cout<<"wrong choice. Exiting...\n";
    return;
  }
  cout<<"Testing write/read flash for fpga "<< fpga <<" in  in sector 0x40\n";

  unsigned int startAdd = 0x400000;
  SetCHFlashAdd(startAdd, fpga);

  //erase sector
  vLUCROD->CHRegs[fpga].flashData = 0x200;    
  while((vLUCROD->CHRegs[fpga].flashStatus & 1) == 1)
    continue;

  cout<<"Flash sector has been erased. \n";
  cout<<"Writing 200*256 bytes in sector 40\n";
    
  //enable bit swapping
  vLUCROD->CHRegs[fpga].control |= 0x10;
  
  //200 256-bytes cycles 
  for (int i = 0; i<200; ++i){
    for (int j = 0; i<256; ++i){
   
      // set  address
      SetCHFlashAdd(startAdd, fpga);   
    
      // wbytes to fifo 
      writeChFlashDatum(j, fpga);

      //load flash data from fifo
      vLUCROD->CHRegs[fpga].flashData = 0x100;

      //check status
      unsigned short val = vLUCROD->CHRegs[fpga].flashStatus;  
      while((val & 1) == 1)
	val = vLUCROD->CHRegs[fpga].flashStatus;
 
     ++startAdd;
    }    
  } 
  
  cout<<"Reading back 200*256 bytes from sector 40\n";

  //enable bit swapping
  vLUCROD->CHRegs[fpga].control |= 0x10;
 
  // set flash address
  startAdd = 0x400000;
  SetCHFlashAdd(startAdd, fpga);   

  //read&compare : 200*256 = 51200 bytes

  for (int i = 0; i<51200; ++i){

    unsigned short rbyte = readChFlashDatum(fpga); 
    unsigned short nloop = i/256;
    unsigned short wbyte = i-nloop*256;
    
    if(rbyte != wbyte){
      std::cout<<i<<":  Read  "<<HEX(rbyte)<<" from FLASH, while expecting "<<HEX(wbyte)<<"\n";
      cout<<" enter 0 to continue, 1 to break\n";
      int opt;
      cin>>opt;
      if( opt == 1)
	break;
    }
    if(i%100==0)
      cout<<i<<": reading from flash 0x "<<HEX(rbyte)<<"\n";
 
  }
  cout<<"All done\n";   
}
/*****************/
void chFlashTest()
/*****************/
{
  cout<<" select channel flash (0-2-4-6) \n";
  unsigned int fpga = 0;
  cin >> fpga;
  if( (fpga != 0) && (fpga != 2) && (fpga != 4) && (fpga != 6)){
    cout<<"wrong choice. Exiting...\n";
    return;
  }
  cout<<"Testing write/read flash for fpga "<< fpga <<" in  in sector 0x40\n";

  unsigned int startAdd = 0x400000;
  SetCHFlashAdd(startAdd, fpga);

  //erase sector
  vLUCROD->CHRegs[fpga].flashData = 0x200;    
  while((vLUCROD->CHRegs[fpga].flashStatus & 1) == 1)
    continue;

  cout<<"Flash sector has been erased. \n";
  cout<<"Writing 200*256 bytes in sector 40\n";
    
  //enable bit swapping
  vLUCROD->CHRegs[fpga].control |= 0x10;
  
  //200 256-bytes cycles
  for (int i = 0; i<200; ++i){
    
    // set  address
    SetCHFlashAdd(startAdd, fpga);   
    
    // 256 bytes to fifo 
    for (unsigned short j = 0; j<256; ++j){
      writeChFlashDatum(j, fpga);
    }
    
    //load fifo to flash
    unsigned short status = LoadCHFlashChunk(fpga);
    if( status != 0 ){
      cout<<" Problem loading chunck "<<i<<". Status is 0x"<<HEX(status)<<". Exiting...";
      return;
    }
    
    startAdd += 256;
  } 
  
  cout<<"Reading back 200*256 bytes from sector 40\n";

  //enable bit swapping
  vLUCROD->CHRegs[fpga].control |= 0x10;
 
  // set flash address
  startAdd = 0x400000;
  SetCHFlashAdd(startAdd, fpga);   

  //read&compare : 200*256 = 51200 bytes

  for (int i = 0; i<51200; ++i){

    unsigned short rbyte = readChFlashDatum(fpga); 
    unsigned short nloop = i/256;
    unsigned short wbyte = i-nloop*256;
    
    if(rbyte != wbyte){
      std::cout<<i<<":  Read  "<<HEX(rbyte)<<" from FLASH, while expecting "<<HEX(wbyte)<<"\n";
      cout<<" enter 0 to continue, 1 to break\n";
      int opt;
      cin>>opt;
      if( opt == 1)
	break;
    }
    if(i%100==0)
      cout<<i<<": reading from flash 0x "<<HEX(rbyte)<<"\n";
 
  }
  cout<<"All done\n";   

}
/*****************/
void VMEFlashTest()
/*****************/
{
  cout<<"Testing write/read for VME flash in sector 0x40\n";

  unsigned int startAdd = 0x400000;
  SetVMEFlashAdd(startAdd);

  //erase sector
  vLUCROD->VMERegs.flashData = 0x200;
  while((vLUCROD->VMERegs.flashStatus & 1) == 1)
    continue;

  cout<<"Flash sector has been erased. \n";
  cout<<"Writing 200*256 bytes in sector 40\n";
    
  //enable bit swapping
  vLUCROD->VMERegs.control |= 2;

  //200 256-bytes cycles
  for (int i = 0; i<200; ++i){

    // set  address
    SetVMEFlashAdd(startAdd);   
   
    // 256 bytes to fifo 
    for (int j = 0; j<256; ++j){
      writeVMEFlashDatum(j);
    }
    
    //load fifo to flash
    unsigned int status = LoadVMEFlashChunk();
    if( status != 0){
      cout<<" Problem loading chunck "<<i<<". Status is 0x"<<HEX(status)<<". Exiting...";
      return;
    }
    
    startAdd += 256;
  } 

  cout<<"Reading back 200*256 bytes from sector 40\n";
  cout<<"Enable byte swapping is bit 1 in 0x"<<HEX(vLUCROD->MainRegs.control)<<"\n";

  //enable bit swapping
  vLUCROD->VMERegs.control |= 2;

  // set flash address
  startAdd = 0x400000;
  SetVMEFlashAdd(startAdd);   

  //read&compare 
  for (int i = 0; i<51200; ++i){
    
    unsigned int rbyte = readVMEFlashDatum();
    unsigned short nloop = i/256;
    unsigned short wbyte = i-nloop*256;
    
    if(rbyte != wbyte){
      std::cout<<i<<":  Read  "<<HEX(rbyte)<<" from FLASH, while expecting "<<HEX(wbyte)<<"\n";
      cout<<" enter 0 to continue, 1 to break\n";
      int opt;
      cin>>opt;
      if( opt == 1)
	break;
    }
    if(i%32 == 0)
      cout<<i<<": reading from flash 0x "<<HEX(rbyte)<<"\n";
  }
  cout<<"All done\n";   
}
/*****************/
void mainFlashTest()
/*****************/
{
  cout<<"Testing write/read flash in sector 0x40\n";

  unsigned int startAdd = 0x400000;
  SetMainFlashAdd(startAdd);

  //erase sector
  vLUCROD->MainRegs.flashData = 0x200;     
  while((vLUCROD->MainRegs.flashStatus & 1) == 1)
    continue;

  cout<<"Flash sector has been erased. \n";
  cout<<"Writing 200*256 bytes in sector 40\n";
    
  //enable bit swapping
  vLUCROD->MainRegs.control |= 2;

  //200 256-bytes cycles
  for (int i = 0; i<200; ++i){

    // set  address
    SetMainFlashAdd(startAdd);   
   
    // 256 bytes to fifo 
    for (int j = 0; j<256; ++j){
      writeMainFlashDatum(j);
    }
    
    //load fifo to flash
    unsigned short status = LoadMainFlashChunk();
    if( status != 0){
      cout<<" Problem loading chunck "<<i<<". Status is 0x"<<HEX(status)<<". Exiting...";
      return;
    }
    
    startAdd += 256;
  } 

  cout<<"Reading back 200*256 bytes from sector 60\n";
  cout<<"Enable byte swapping is bit 1 in 0x"<<HEX(vLUCROD->MainRegs.control)<<"\n";

  //enable bit swapping
  vLUCROD->MainRegs.control |= 2;

  // set flash address
  startAdd = 0x400000;
  SetMainFlashAdd(startAdd);   

  //read&compare 
  for (int i = 0; i<51200; ++i){
    
    unsigned short rbyte = readMainFlashDatum();
    unsigned short nloop = i/256;
    unsigned short wbyte = i-nloop*256;
    
    if(rbyte != wbyte){
      std::cout<<i<<":  Read  "<<HEX(rbyte)<<" from FLASH, while expecting "<<HEX(wbyte)<<"\n";
      cout<<" enter 0 to continue, 1 to break\n";
      int opt;
      cin>>opt;
      if( opt == 1)
	break;
    }
    if(i%32==0)
      cout<<i<<": reading from flash 0x "<<HEX(rbyte)<<"\n";
  }
  cout<<"All done\n";   
}
/*****************/
void eraseChFlash()
/*****************/
{
  int ans;
  std::cout<<"Are you sure to delete channel FW from FLASH? (Yes = 1) \n";
  cin >> ans;
  if(ans != 1){
    cout<<" doing nothing \n";
    
  }
  else{
  
    std::cout<<"select fpga number, e.g.  0-2-4-6 \n";
    unsigned short fpga = 0;
    cin >> fpga;
    if( (fpga != 0) && (fpga != 2) && (fpga != 4) && (fpga != 6)){
      cout<<"wrong choice. Exiting...\n";
      return;
    }
    cout<<" erasing flash for FPGA "<<fpga<<" and "<<fpga+1<<"\n";
    vLUCROD->CHRegs[fpga].flashData = 0x400;
    while((vLUCROD->CHRegs[fpga].flashStatus & 1) == 1)
      continue;
    cout<<" Flash has been deleted\n";

  }
}
/*****************/
void eraseMainFlash()
/*****************/
{
  int ans;
  std::cout<<"Are you sure to delete MAIN FPGA FW from FLASH? (Yes = 1) \n";
  cin >> ans;
  if(ans != 1){
    cout<<" doing nothing \n";    
  }
  else{
    vLUCROD->MainRegs.flashData = 0x400;
    while((vLUCROD->MainRegs.flashStatus & 1) == 1)
      continue;
    cout<<" Flash has been deleted\n";
  }
}
/*****************/
void eraseVMEFlash()
/*****************/
{
  int ans;
  std::cout<<"Are you sure to delete VME FPGA FW from FLASH? (Yes = 1) \n";
  cin >> ans;
  if(ans != 1){
    cout<<" doing nothing \n";    
  }
  else{
    vLUCROD->VMERegs.flashData = 0x400;
    while((vLUCROD->VMERegs.flashStatus & 1) == 1)
      continue;
    cout<<" Flash has been deleted\n";
  }
}
/*****************/
std::vector<unsigned short> readChRBF(std::string filename)
/*****************/
{
  if(filename == " ") filename = "/det/lucid/fw/lucrod/ChFPGA.rbf";
  std::cout<<"comparing FLASH content to "<<filename<<"\n";
  std::vector<unsigned short> bytes = readRBF(filename);
  return bytes;
}
/*****************/
 std::vector<unsigned short> readVmeRBF( )
/*****************/
{
  std::string filename = "/det/lucid/fw/lucrod/VmeFPGA.rbf";
  std::cout<<"comparing FLASH content to "<<filename<<"\n";
  std::vector<unsigned short> bytes = readRBF(filename);
  return bytes;
}
/*****************/
 std::vector<unsigned short> readMainRBF( )
/*****************/
{
  std::string filename = "/det/lucid/fw/lucrod/MainFPGA.rbf";
  std::cout<<"comparing FLASH content to "<<filename<<"\n";
  std::vector<unsigned short> bytes = readRBF(filename);
  return bytes;
}
/*****************/
std::vector<unsigned short> readRBF(std::string filename )
/*****************/
{
  // load rbf file in memory
  std::vector<unsigned short> bytes;
  std::ifstream inFile(filename, ios::binary|ios::ate); 
  if(!inFile) {
    std::cout << "Could not open input file. Aborting." << endl;
    return bytes;
  }
  streampos size = inFile.tellg(); 
  std::cout<<" the file is "<< size <<" bytes long\n";
  inFile.seekg (0, ios::beg);
  for(int i = 0; i<size; ++i){
    unsigned short abyte;    
    inFile.read (reinterpret_cast<char*>(&abyte), 1);
    bytes.push_back(abyte&0xff);
  }
  inFile.close();  
  return bytes;
}
/*****************/
void writeChFlashDatum(unsigned short byte, int fpga)
/*****************/
{
  //  usleep(500);
  while((vLUCROD->CHRegs[fpga].flashStatus & 1) == 1)
    continue;
  vLUCROD->CHRegs[fpga].flashData = byte;
}
/*****************/
void writeMainFlashDatum(unsigned short byte)
/*****************/
{
  while((vLUCROD->MainRegs.flashStatus & 1) == 1)
    continue;
  vLUCROD->MainRegs.flashData = byte;
}
/*****************/
void writeVMEFlashDatum(unsigned int byte)
/*****************/
{
  while((vLUCROD->VMERegs.flashStatus & 1) == 1)
    continue;
  vLUCROD->VMERegs.flashData = byte;
}
/*****************/
unsigned short readChFlashDatum(int fpga)
/*****************/
{
  vLUCROD->CHRegs[fpga].flashData = 0x800;
  while((vLUCROD->CHRegs[fpga].flashStatus & 1) == 1)
    continue;
  unsigned short datum = vLUCROD->CHRegs[fpga].flashData;
  return datum;
}
/*****************/
unsigned short readMainFlashDatum()
/*****************/
{
  vLUCROD->MainRegs.flashData = 0x800;
  while((vLUCROD->MainRegs.flashStatus & 1) == 1)
    continue;
  unsigned short datum = vLUCROD->MainRegs.flashData;
  return datum;
}
/*****************/
unsigned int readVMEFlashDatum()
/*****************/
{
  vLUCROD->VMERegs.flashData = 0x800;
  while((vLUCROD->VMERegs.flashStatus & 1) == 1)
    continue;
  unsigned int datum = vLUCROD->VMERegs.flashData;
  return datum;
}
/*****************/
void writeChFlash(std::string filename)
/*****************/
{
  //load rbf file in memory
  std::vector<unsigned short> bytes = readRBF(filename);
  unsigned int size = bytes.size();
  if(size < 1) return;

  int opt;
  std::cout<<"Firmware read from file. Do you wish to write it (YES == 1)?\n";
  std::cin>>opt;
  if(!opt){
    std::cout<<"File will NOT be written. Exiting...\n";
    return;
  }

  // copy memory to flash in 256-long chuncks.
  unsigned int ncycles = (int)size/256;
  unsigned int lastWrite = (int)size - ncycles*256;
  std::cout<<"Writing "<< size <<" bytes in "<<ncycles+1 <<" chuncks\n";
  std::cout<<"Last chunck is "<< lastWrite <<" bytes\n";

  //select fpga
  std::cout<<"select channel pair (0-2-4-6)\n";
  int  fpga = 0;
  cin >> fpga;
  if( (fpga != 0) && (fpga != 2) && (fpga != 4) && (fpga != 6)){
    cout<<"wrong choice. Exiting...\n";
    return;
  }
  cout<<" Will write flash for FPGA "<<fpga<<" and "<<fpga+1<<"\n";


  // enable bit swapping 
  vLUCROD->CHRegs[fpga].control |= 0x10;

  unsigned int startAdd = 0;
 
  if( ncycles > 0 ){    //repeat for each 256 bytes chunk

    for (unsigned int i = 0; i<ncycles; ++i){

      // write flash address
      SetCHFlashAdd(startAdd, fpga);

      // write 256 bytes to flash fifo 
      for (int j = 0; j<256; ++j){
        unsigned short byte = bytes[j+i*256];
        writeChFlashDatum(byte, fpga);
      }

      //load flash
      unsigned short status = LoadCHFlashChunk(fpga);

      //check status
      if( status != 0){
	cout<<" Problem writing chunck "<<i<<". Status is 0x"<<HEX(status)<<". Exiting...";
	return;
      }

      //increment address
      startAdd += 256;
    }
  } 

  // last chunck address  
  SetCHFlashAdd(startAdd, fpga);
  
  //write last chunck
  for (unsigned int j = 0; j<lastWrite; ++j){
    unsigned short byte = bytes[ncycles*256+j];
    writeChFlashDatum(byte, fpga);
  }
  
  // load last chunck
  unsigned short status = LoadCHFlashChunk(fpga);
  
  //check status
  if( status != 0){
    cout<<" Problem writing last chunck. Status is 0x"<<HEX(status)<<". Exiting...";
    return;
  }
  else
    cout<<" All done\n";     
}

/*****************/
void writeMainFlash(std::string filename)
/*****************/
{
  //load rbf file in memory
  std::vector<unsigned short> bytes = readRBF(filename);
  unsigned int size = bytes.size();
  if(size < 1) return;

  int opt;
  std::cout<<"Firmware read from file. Do you wish to write it (YES == 1)?\n";
  std::cin>>opt;
  if(!opt){
    std::cout<<"File will NOT be loaded. Exiting...\n";
    return;
  }

  // copy memory to flash in 256-long chuncks.
  unsigned int ncycles = (int)size/256;
  unsigned int lastWrite = (int)size - ncycles*256;
  std::cout<<"Writing "<< size <<" bytes in "<<ncycles+1 <<" chuncks\n";
  std::cout<<"Last chunck is "<< lastWrite <<" bytes\n";

  // enable bit swappping 
  vLUCROD->MainRegs.control |= 2;

  unsigned int startAdd = 0;
 
  if( ncycles > 0 ){    //repeat for each 256 bytes chunck

    for (unsigned int i = 0; i<ncycles; ++i){

      // set address
      SetMainFlashAdd(startAdd);   

      // 256 bytes to fifo 
      for (int j = 0; j<256; ++j){
        unsigned short byte = bytes[j+i*256];
        writeMainFlashDatum(byte);
      }

      //load fifo to flash
      unsigned short status = LoadMainFlashChunk();
      if( status != 0){
	cout<<" Problem loading chunck "<<i<<". Status is 0x"<<HEX(status)<<". Exiting...";
	return;
      }
      
      //increment address
      startAdd += 256;
      if(i%100 == 0)cout<<" chunck "<<i<<"has been written\n"; 
     }
  } 

  // last chunck:
 
  //set address
  SetMainFlashAdd(startAdd);   

  //write fifo
  for (unsigned int j = 0; j<lastWrite; ++j){
    unsigned short byte = bytes[ncycles*256+j];
    writeMainFlashDatum(byte);
  }
 
  //load fifo
  unsigned short status = LoadMainFlashChunk();
  if( status != 0){
    cout<<" Problem writing last chunck. Status is 0x"<<HEX(status)<<". Exiting...";
    return;
  }
  cout<<" All done\n";

}
/*****************/
void writeVMEFlash(std::string filename)
/*****************/
{
  //load rbf file in memory
  std::vector<unsigned short> bytes = readRBF(filename);
  unsigned int size = bytes.size();
  if(size < 1) return;

  int opt;
  std::cout<<"Firmware read from file. Do you wish to write it (YES == 1)?\n";
  std::cin>>opt;
  if(!opt){
    std::cout<<"File will NOT be written. Exiting...\n";
    return;
  }

  // copy memory to flash in 256-long chuncks.
  unsigned int ncycles = (int)size/256;
  unsigned int lastWrite = (int)size - ncycles*256;
  std::cout<<"Writing "<< size <<" bytes in "<<ncycles+1 <<" chuncks\n";
  std::cout<<"Last chunck is "<< lastWrite <<" bytes\n";

  // enable bit swappping 
  vLUCROD->VMERegs.control |= 2;

  unsigned int startAdd = 0;
 
  if( ncycles > 0 ){    //repeat for each 256 bytes chunck

    for (unsigned int i = 0; i<ncycles; ++i){

      // set address
      SetVMEFlashAdd(startAdd);   

      // 256 bytes to fifo 
      for (int j = 0; j<256; ++j){
        unsigned int byte = (unsigned int)bytes[j+i*256];
        writeVMEFlashDatum(byte);
      }

      //load fifo to flash
      unsigned short status = LoadVMEFlashChunk();
      if( status != 0){
	cout<<" Problem writing chunck "<<i<<". Status is 0x"<<HEX(status)<<". Exiting...";
	return;
      }
      
      //increment address
      startAdd += 256;
      if(i%100 == 0)cout<<" chunck "<<i<<"has been written\n"; 
     }
  } 

  // last chunck:
 
  //set address
  SetVMEFlashAdd(startAdd);   

  //write fifo
  for (unsigned int j = 0; j<lastWrite; ++j){
    unsigned short byte = bytes[ncycles*256+j];
    writeVMEFlashDatum(byte);
  }
 
  //load fifo
  unsigned short status = LoadVMEFlashChunk();
  if( status != 0){
    cout<<" Problem writing last chunck. Status is 0x"<<HEX(status)<<". Exiting...";
    return;
  }
  cout<<" All done\n";

}
/*****************/
int readMainFlash()
/*****************/
{
  int success = 0;
  std::string filename = "/det/lucid/fw/lucrod/MainFPGA.rbf";
  //load rbf file in memory
  std::vector<unsigned short> bytes = readRBF(filename);
  unsigned int size = bytes.size();
  if(size < 1) return -1;

  //enable bit swapping
  vLUCROD->MainRegs.control |= 0x2;

  //start address
  SetMainFlashAdd(0);   

  //compare reading to file content
  for(unsigned int i=0; i<size; ++i){
    unsigned short rbyte = readMainFlashDatum();
    if(rbyte != bytes[i]){
      success = 1;
      std::cout<<i<<":  Read  "<<HEX(rbyte)<<" from FLASH, while expecting "<<HEX(bytes[i])<<" from file\n";
      cout<<" enter 0 to continue, 1 to break\n";
      int opt;
      cin>>opt;
      if( opt == 1)
        break;
    }
    if(size%500 == 0)
      cout<<" byte "<<i<<" reads 0x"<<HEX(rbyte)<<"\n"; 
  }  
  cout<<" All done! \n";
  return success;
}
/*****************/
int readVMEFlash()
/*****************/
{
  int success = 0;
  std::string filename = "/det/lucid/fw/lucrod/VmeFPGA.rbf";
  //load rbf file in memory
  std::vector<unsigned short> bytes = readRBF(filename);
  unsigned int size = bytes.size();
  if(size < 1) return -1;

  //enable bit swapping
  vLUCROD->VMERegs.control |= 0x2;

  //start address
  SetVMEFlashAdd(0);   

  //compare reading to file content
  for(unsigned int i=0; i<size; ++i){    
    unsigned short rbyte = readVMEFlashDatum();
    if(rbyte != bytes[i]){
      success = 1;
      std::cout<<i<<":  Read  "<<HEX(rbyte)<<" from FLASH, while expecting "<<HEX(bytes[i])<<" from file\n";
      cout<<" enter 0 to continue, 1 to break\n";
      int opt;
      cin>>opt;
      if( opt == 1)
        break;
    }
    if(size%100 == 0)
      cout<<" byte "<<i<<" reads 0x"<<HEX(rbyte)<<"\n"; 
  }  
  cout<<" All done! \n";
  return success;
}
/*****************/
int readChFlash( unsigned short fpga, std::string rbfFilename)
/*****************/
{
  int success = 0;

  //load rbf file to compare to in memory
  std::vector<unsigned short> bytes = readChRBF(rbfFilename);
  unsigned int size = bytes.size();
  if(size < 1) return -1;

  //enable bit swap
  vLUCROD->CHRegs[fpga].control |= 0x10;

  //set read address 
  SetCHFlashAdd(0, fpga);

  //compare reading to file content
  for(unsigned int i=0; i<size; ++i){    
    unsigned short rbyte = readChFlashDatum(fpga);
    if(rbyte != bytes[i]){
      success = 1;
      std::cout<<i<<":  Read  "<<HEX(rbyte)<<" from FLASH, while expecting "<<HEX(bytes[i])<<" from file\n";
      cout<<" enter 0 to continue, 1 to break\n";
      int opt;
      cin>>opt;
      if( opt == 1)
        break;
    }
    //    if(i%1000 == 0)
    //      cout<<"byte "<<i<<" reads 0x"<<HEX(rbyte)<<"\n";
  }  
  cout<<"all done\n"; 
  return success;
}
/*****************/
void VMEMonitor()
/*****************/
{
  int nwait = 0;
  //freeze fifo
  vLUCROD->VMERegs.control |= 0x4;
  //wait for freeze
  while( (vLUCROD->VMERegs.status & 1) != 1){
    ++nwait;
    if(nwait > 10000) break;
  }
  cout<<"Fifo Freezed after "<<nwait<<" checks \n";

  //Read buffer
  for(int i = 0; i<32; ++i){
    unsigned int mma = vLUCROD->VMERegs.vmeMMA;
    unsigned short add = mma & 0xfff;
    unsigned short as = (mma>>12)&1;
    unsigned short am = (mma>>13)&1;
    unsigned short ds0 = (mma>>14)&1;
    unsigned short ds1 = (mma>>15)&1;
    unsigned short wwrite = (mma>>16)&1;
    unsigned short lword = (mma>>17)&1;
    unsigned short bsl = (mma>>18)&1;
    unsigned short bsh = (mma>>19)&1;
    unsigned short berr = (mma>>20)&1;
    unsigned short evEnd = (mma>>21)&1;
    unsigned short valid = (mma>>22)&1;

    cout<<"   WORD "<<i<<"\n";
    cout<<" Address 0x"<<HEX(add)<<" VME_AS="<<as<<" VME_AM="<<am<<
      " VME_DS0="<<ds0<<" VME_DS1="<<ds1<<" VME_write="<<wwrite<<
      " VME_lword="<<lword<<" VME_BSL="<<bsl<<" VME_BSH="<<bsh<<
      " VME_BERR="<<berr<<" VME_EvEnd="<<evEnd<<" VME_valid="<<valid<<"\n";
  }
  for(int i = 0; i<32; ++i){
    unsigned int mmd = vLUCROD->VMERegs.vmeMMD;
    cout<<"   WORD "<<i<<"\n";
    cout<<" datum 0x"<<HEX(mmd)<<"\n";
  }
  //release freeze
  vLUCROD->VMERegs.control &= 0xfffffffb;

  //restart monitor
  vLUCROD->VMERegs.control |= 0x1;  
  vLUCROD->VMERegs.control &= 0xfffffffe;

}
/*****************/
void shiftChClockPhase()
/*****************/
{
  
  std::cout<<"select ifpga (0-7)\n";
  unsigned short ifpga = 0;
  cin >> ifpga;
  cout<<"Shifting clock for FPGA "<<ifpga<<"\n";

  unsigned int NSTEP = 0;
  cout<<" ENTER INITIAL NSTEP\n";
  cin>> NSTEP;

  int ntrials = 0;
  int goon = 1;
  while(goon == 1){
    NSTEP +=5;
    unsigned short datum = ((ntrials%2) == 0) ? 0x16 : 0x1E; 
    cout<<"Shifting clock "<<NSTEP<<" times by 0x"<<HEX(datum)<<"\n"; 
    for(unsigned int nloop = 0; nloop <NSTEP; ++nloop){
      vLUCROD->CHRegs[ifpga].clock_adjust = datum;
    }
    chFlashTest();
    ntrials++;
    cout<<"continue ? stop = 0 \n";
    cin>>goon;
  }
}
/*****************/
void setSimuHit2Lumat()
/*****************/
{
  unsigned short opt;
  //simulator Threshold
  vLUCROD->MainRegs.lnkRW0 = 0x3000    ;
  vLUCROD->MainRegs.lnkRW4 = 0x3ff;

  //beam pattern
  cout<<" Enter number of BCID in first train \n";
  cin >> opt;
  unsigned short train1 = opt & 0xff;
  cout<<" Enter number of BCID in second train \n";
  cin >> opt;
  unsigned short train2 = opt & 0xff;
  cout<<" Enter number of BCID in third train \n";
  cin >> opt;
  unsigned short train3 = opt & 0xff;
 
  cout<<" Enter number of BCID in first gap \n";
  cin >> opt;
  unsigned short gap1 = opt & 0xff;
  cout<<" Enter number of BCID in second gap \n";
  cin >> opt;
  unsigned short gap2 = opt & 0xff;
  cout<<" Enter number of BCID in third gap \n";
  cin >> opt;
  unsigned short gap3 = opt & 0xff;

  cout<<" Enter number pattern repetitions \n";
  cin >> opt;
  unsigned short rep = opt & 0xff;

  vLUCROD->MainRegs.lnkRW1 = rep | (train1 << 16);
  vLUCROD->MainRegs.lnkRW2 = gap1 | (train2 << 16);
  vLUCROD->MainRegs.lnkRW3 = gap2 | (train3 << 16);
  unsigned short bit = (vLUCROD->MainRegs.lnkRW5 & 0x200)>>9;
  vLUCROD->MainRegs.lnkRW5 = gap3 | (bit<<9);

}
/*****************/
void setCh2Lumat()
/*****************/
{
  // enabled channels to output 
  unsigned short opt;

  cout<<" Enter enabled pattern mask 0x\n";
  cin >> std::hex >> opt >> std::dec ;
  cout<<" selected mask is 0x"<<HEX(opt)<<"; dec = "<<opt<<"\n";
  unsigned short enabled[8];
  int nch = 0;
  for (int i=0; i<16; ++i){
    bool val = (opt >> i) & 0x1;
    if(val){
      cout<<"adding channel "<<i<<"\n";
      enabled[nch] = i;
      ++nch;      
    }
    if(nch > 7) break;
  }
  cout<<" Number of enabled channels is "<<nch + 1<<"\n"; 
  cout<<" List of enabled channels is: ";
  for (int i=0; i<nch; ++i){
    cout<<enabled[i]<<" ";
  }
  cout<<"\n ";
  unsigned short word1 =  enabled[0] | (enabled[1]<<4) | (enabled[2]<<8) | (enabled[3]<<12);
  unsigned short word2 =  enabled[4] | (enabled[5]<<4) | (enabled[6]<<8) | (enabled[7]<<12);
  cout<<" final words are 0x " <<HEX(word1)<<" and 0x"<<HEX(word2)<<"\n"; 
  // at last,  write values in registers
  vLUCROD->MainRegs.lnkRW6 = word1;
  vLUCROD->MainRegs.lnkRW7 = word2;

}
 /*****************/
void setLemoOutput()
/*****************/
{
  cout<<" Select signal to be sent to US0\n";
  cout<<"   0 = TTCrq orbit (default) \n";
  cout<<"   1 = clock_40\n";
  cout<<"   2 = used clock (according to clock selected)\n";
  cout<<"   6 = TTcrq event count reset\n";
  cout<<"   7 = TTCrq L1 Accept\n";
  cout<<" Remember that US1 carries the orbit for FPGACH, and US2 the orbit for the lumat\n";
  unsigned short opt;
  cin >> opt;
  cout<<" Selected option:"<<opt<<"\n";
  vLUCROD->MainRegs.debug = opt;
}

/*****************/
void VMEFPGA_dump()
/*****************/
{
  cout<<"  VME FPGA register content: \n";
  unsigned int version = vLUCROD->VMERegs.version;     
  unsigned int debug = vLUCROD->VMERegs.debug;       
  unsigned int control = vLUCROD->VMERegs.control;        
  unsigned int flashStatus = vLUCROD->VMERegs.flashStatus;
  unsigned int status = vLUCROD->VMERegs.status;
  cout<<" version is 0x"<<std::hex<<version<<"\n";
  cout<<" debug  is 0x"<<std::hex<<debug<<"\n";
  cout<<" control is 0x"<<std::hex<<control<<"\n";
  cout<<" flashstatus is 0x"<<std::hex<<flashStatus<<"\n";
  cout<<" status is 0x"<<std::hex<<status<<"\n";
}
/*****************/
void mainFPGA_dump()
/*****************/
{
  cout<<"  Main FPGA register content: \n";
  unsigned short val = vLUCROD->MainRegs.version;
  cout<<" firmware version is 0x"<<std::hex<<val<<"\n";
  val = vLUCROD->MainRegs.debug;
  cout<<" debug is 0x"<<std::hex<<val<<"\n";
  val = vLUCROD->MainRegs.control;
  ((val&1) == 1) ?  cout<<" Local clock is selected\n" :  cout<<" TTCrq clock is selected\n" ;
  val = vLUCROD->MainRegs.LumatOrbitDelay;
  cout<<" Orbit delay for hits to lumat is 0x"<<std::hex<<val<<"\n";
  val = vLUCROD->MainRegs.ChOrbitDelay;
  cout<<" Orbit delay for channel FPGA 0x"<<std::hex<<val<<"\n";
  val = (vLUCROD->MainRegs.lnkRW5 & 0x200)>>9;
  cout<<" Bit 8 of lnkRW5 is 0x"<<std::hex<<val <<"\n";
  val = vLUCROD->MainRegs.lnkRW5;
  cout<<" lnkRW5 is 0x"<<std::hex<<val <<"\n";
  val = vLUCROD->MainRegs.status;
  cout<<" TTCrq status is 0x"<<std::hex<<val <<"\n";
  val = vLUCROD->MainRegs.TTCrqControl;
  cout<<" TTCrq control is 0x"<<std::hex<<val<<std::dec<<"\n";
}

/*****************/
void showSelectedClock()
/*****************/
{
  unsigned short val = (vLUCROD->MainRegs.status>>1 ) & 0x1;
  (val == 0) ? cout<<" Selected clock from TTCrq\n" : cout<<" Selected clock internal \n"; 
}
/*****************/
void setClock()
/*****************/
{
  unsigned short opt = 0;
  unsigned short one = 1;
  cout<<" Enter 0 to select TTCrq clock, 1 to select internal\n";
  cin>>opt;
  if (opt == 1)
    vLUCROD->MainRegs.control |= one;
  else
    vLUCROD->MainRegs.control &= ~one;
  usleep(100);
  opt = (vLUCROD->MainRegs.status>>1) & 1;
  (opt == 0) ? cout<<" running clock is the one from TTCrq\n" : cout<<" running clock is the internal one\n"; 
  
}
/*****************/
void getFirmwareVersion()
/*****************/
{
  cout<<" VME FPGA firmware version is 0x"<<std::hex<<vLUCROD->VMERegs.version<<"\n";
  cout<<" Main FPGA firmware version is 0x"<<std::hex<<vLUCROD->MainRegs.version<<"\n";
  for(int i = 0; i<8; ++i){
    volatile unsigned short fw = vLUCROD->CHRegs[i].version; 
    cout<<"    channel "<<i<<" FPGA firmware version is 0x"<<std::hex<< fw <<std::dec<<"\n";
  }
}

/*****************/
void dumpChSettings()
/*****************/
{
  for(int i=0; i<7; ++i){
    unsigned short control = vLUCROD->CHRegs[i].control;
    cout<<"FPGA number "<<i<<" control word is 0x"<<HEX(control)<<"\n";
    unsigned short test = control & 1;
    (test == 1 ) ? cout<<"   Acquisition mode ON\n" :  cout<<"  Acquisition mode OFF\n";
    test = (control & 8)>>3;
    (test == 1 ) ? cout<<"   Baeline subtraction OFF\n" :  cout<<" Baseline subtraction ON\n";
    test = (control & 4)>>2;
    (test == 1 ) ? cout<<"   Writing to FIFO 1\n" :  cout<<" Writing to FIFO 2\n";
    test = (control & 0x80)>>7;
    (test == 1 ) ? cout<<"   1-orbit Q acquisition ON\n" :  cout<<" 1-orbit Q acquisition OFF\n";
    test = (control & 0x8000)>>15;
    (test == 1 ) ? cout<<"   scope function ENABLED \n" :  cout<<" scope function DISABLED\n";   
  }
}
/*****************/
void dumpChOffsets()
/*****************/
{
  for(int i = 0; i<8; ++i){
    volatile unsigned short val = vLUCROD->CHRegs[i].ch0FadcOffset; 
    cout<<"    channel "<<i*2+1<<" FADC offset is 0x"<<std::hex<< val <<std::dec<<"\n";
    val = vLUCROD->CHRegs[i].ch1FadcOffset; 
    cout<<"    channel "<<i*2<<" FADC offset is 0x"<<std::hex<< val <<std::dec<<"\n";
  }
}
/*****************/
void setChOffset()
/*****************/
{
  int ch;
  unsigned int offset;
  std::cout<<"enter channel to be modified \n";
  cin>>ch;
  std::cout<<"enter new offset value: 0x \n";
  cin>>std::hex>>offset>>std::dec;
  std::cout<<"Modifying channel "<<ch<<": new offset value is 0x"<<std::hex<<offset<<std::dec<<"\n";

  if(ch%2 == 0)
    vLUCROD->CHRegs[ch/2].ch1FadcOffset = offset;
  else
    vLUCROD->CHRegs[ch/2].ch0FadcOffset = offset;

  std::cout<<"Reading the values back :\n";
  
  volatile unsigned short val = vLUCROD->CHRegs[ch/2].ch1FadcOffset; 
  cout<<"    channel "<<ch<<" FADC offset is 0x"<<std::hex<< val <<std::dec<<"\n";
  val = vLUCROD->CHRegs[ch/2].ch0FadcOffset; 
  cout<<"    channel "<<ch+1<<" FADC offset is 0x"<<std::hex<< val <<std::dec<<"\n";
}

/*****************/
void dumpChGains()
/*****************/
{
  for(int i = 0; i<8; ++i){
    volatile unsigned short val = vLUCROD->CHRegs[i].ch1FadcGain; 
    cout<<"    channel "<<i*2<<" FADC gain is 0x"<<std::hex<< val <<std::dec<<"\n";
    val = vLUCROD->CHRegs[i].ch0FadcGain; 
    cout<<"    channel "<<i*2+1<<" FADC gain is 0x"<<std::hex<< val <<std::dec<<"\n";
  }
}
/*****************/
void setChGain()
/*****************/
{

  int ch;
  unsigned int gain;
  std::cout<<"enter channel to be modified \n";
  cin>>ch;
  std::cout<<"enter new offset value: 0x \n";
  cin>>std::hex>>gain>>std::dec;
  std::cout<<"Modifying channel "<<ch<<": new gain value is 0x"<<std::hex<<gain<<std::dec<<"\n";

  if(ch%2 == 0)
    vLUCROD->CHRegs[ch/2].ch1FadcGain = gain;
  else
    vLUCROD->CHRegs[ch/2].ch0FadcGain = gain;

  std::cout<<"Reading the values back :\n";
  
  volatile unsigned short val = vLUCROD->CHRegs[ch/2].ch1FadcGain; 
  cout<<"    channel "<<ch<<" FADC gain is 0x"<<std::hex<< val <<std::dec<<"\n";
  val = vLUCROD->CHRegs[ch/2].ch0FadcGain; 
  cout<<"    channel "<<ch+1<<" FADC gain is 0x"<<std::hex<< val <<std::dec<<"\n";

}
/*****************/
void dumpChThresholds()
/*****************/
{
  for(int i = 0; i<8; ++i){
    volatile unsigned short val = vLUCROD->CHRegs[i].ch1FadcThreshold; 
    cout<<"    channel "<<i*2<<" FADC threshold is "<< val <<"\n";
    val = vLUCROD->CHRegs[i].ch0FadcThreshold; 
    cout<<"    channel "<<i*2+1<<" FADC threshold is "<< val <<"\n";
  }
}
/*****************/
void setChThreshold()
/*****************/
{
  int ch;
  unsigned int thresh;
  std::cout<<"enter channel to be modified \n";
  cin>>ch;
  std::cout<<"enter new threshold value: 0x \n";
  cin>>thresh;
  std::cout<<"Modifying channel "<<ch<<": new threshold value is "<<thresh<<"\n";

  if(ch%2 == 0)
    vLUCROD->CHRegs[ch/2].ch1FadcThreshold = thresh;
  else
    vLUCROD->CHRegs[ch/2].ch0FadcThreshold = thresh;

  std::cout<<"Reading the values back :\n";

  volatile unsigned short val = vLUCROD->CHRegs[ch].ch1FadcThreshold; 
  cout<<"    channel "<<ch<<" FADC threshold is "<< val <<"\n";
  val = vLUCROD->CHRegs[ch].ch0FadcThreshold; 
  cout<<"    channel "<<ch+1<<" FADC threshold is "<< val <<"\n";

}
/*****************/
void dumpOutCopyGains()
/*****************/
{
  for(int i = 0; i<8; ++i){
    volatile unsigned short val = vLUCROD->CHRegs[i].ch1OutGain; 
    cout<<"    channel "<<i*2<<" Output Copy gain is 0x"<<std::hex<< val <<std::dec<<"\n";
    val = vLUCROD->CHRegs[i].ch0OutGain; 
    cout<<"    channel "<<i*2+1<<" Output Copy gain is 0x"<<std::hex<< val <<std::dec<<"\n";
  }
}
/*****************/
void setOutCopyGain()
/*****************/
{
  int ch;
  unsigned int gain;
  std::cout<<"enter channel to be modified \n";
  cin>>ch;
  std::cout<<"enter new gain value: 0x \n";
  cin>>std::hex>>gain>>std::dec;
  std::cout<<"Modifying channel "<<ch<<": new gain value is 0x"<<std::hex<<gain<<std::dec<<"\n";

  if(ch%2 == 0)
    vLUCROD->CHRegs[ch/2].ch1OutGain = gain;
  else
    vLUCROD->CHRegs[ch/2].ch0OutGain = gain;

  std::cout<<"Reading the values back :\n";

  volatile unsigned short val = vLUCROD->CHRegs[ch/2].ch1OutGain; 
  cout<<"    channel "<<ch<<" Output Copy gain is 0x"<<std::hex<< val <<std::dec<<"\n";
  val = vLUCROD->CHRegs[ch/2].ch0OutGain; 
  cout<<"    channel "<<ch+1<<" Output Copy gain is 0x"<<std::hex<< val <<std::dec<<"\n";

}
/*****************/
void dumpOutCopyOffsets()
/*****************/
{
  for(int i = 0; i<8; ++i){
    volatile unsigned short val = vLUCROD->CHRegs[i].ch1OutOffset; 
    cout<<"    channel "<<i*2<<" Output Copy offset is 0x"<<std::hex<< val <<std::dec<<"\n";
    val = vLUCROD->CHRegs[i].ch0OutOffset; 
    cout<<"    channel "<<i*2+1<<" Output Copy Offset is 0x"<<std::hex<< val <<std::dec<<"\n";
  }
}
/*****************/
void setOutCopyOffset()
/*****************/
{
  int ch;
  unsigned int offset;
  std::cout<<"enter channel to be modified \n";
  cin>>ch;
  std::cout<<"enter new offset value: 0x \n";
  cin>>std::hex>>offset>>std::dec;
  std::cout<<"Modifying channel "<<ch<<": new gain value is 0x"<<std::hex<<offset<<std::dec<<"\n";

  if(ch%2 == 0)
    vLUCROD->CHRegs[ch/2].ch1OutOffset = offset;
  else
    vLUCROD->CHRegs[ch/2].ch0OutOffset = offset;

  std::cout<<"Reading the values back :\n";

  volatile unsigned short val = vLUCROD->CHRegs[ch].ch1OutOffset; 
  cout<<"    channel "<<ch<<" Output Copy offset is 0x"<<std::hex<< val <<std::dec<<"\n";
  val = vLUCROD->CHRegs[ch].ch0OutOffset; 
  cout<<"    channel "<<ch+1<<" Output Copy Offset is 0x"<<std::hex<< val <<std::dec<<"\n";

}
/*****************/
void getRegister()
/*****************/
{
  cout<<"to be implemented \n";
}
/*****************/
void setRegister()
/*****************/
{
  cout<<"to be implemented \n";
}
/*****************/
int ttcrq()
/*****************/
{
  unsigned short status = vLUCROD->MainRegs.status;
  cout<<" TTcrq status is 0x"<<std::hex<<status<<std::dec<<"\n"; 
  status >>= 6;
  cout<<" TTcrq status is now "<<status<<"\n"; 

  if(status == 3){
    cout<<"TTCrq locked and ready : nothing to do\n"; 
    return 0;
  }
  else{
    if(status> 3) 
      cout<<"TTCrq is IN ERROR state. Check hawdware\n";
    if(status == 1)
      cout<<"TTCrq is locked but not ready: trying to recover. ....\n";
    else if(status == 2)
      cout<<"TTCrq is ready but not locked; trying to recover....\n";

    vLUCROD->MainRegs.TTCrqControl = 0x180;
    usleep(200000);
    vLUCROD->MainRegs.TTCrqControl =0x1e0;
    cout<<" please check TTCrq again\n";
    return 1;
  } 
}
/*****************/
void globalCommand(unsigned short code)
/*****************/
{
  vLUCROD->MainRegs.globalCMD = code;
  vLUCROD->MainRegs.globalCMD = 0;
}
/*****************/
void readFifo()
/*****************/
{
  std::cout<<" an equivalent full-orbit length will be read in sequences of 8 bcid (64samples);\n";
  unsigned short ifpga;
  std::cout<<"Select fpga (0-7)"<<std::endl;
  cin >> ifpga;  
  std::cout<<"selected fpga is"<<ifpga<<std::endl;

  unsigned short maxval0 = 0;
  unsigned short maxval1 = 0;
  int maxbcid0 = -1;
  int maxbcid1 = -1;
  std::vector<unsigned short> fifo0;
  std::vector<unsigned short> fifo1;

  unsigned short  bcid = 0;
 
  int nLOOP = 445;  // number of waveform in one orbit: 3564/8 = nLOOP
  int nSAMPLES = 64;
 
  for (int nloop = 0; nloop<nLOOP; ++nloop){

    vLUCROD->CHRegs[ifpga].waveFormBcid = bcid&0xfff;
 
    //clear waveform fifos
    globalCommand(6);
 
    //start acquisition of waveform fifos & accum1 (from version d+7)
    globalCommand(1);
    usleep(100);  //wait one orbit (avoid checking status)

    for(int i=0; i<nSAMPLES; ++i){
      unsigned short val0 = vLUCROD->CHRegs[ifpga].waveForm0_fifo;
      unsigned short val1 = vLUCROD->CHRegs[ifpga].waveForm1_fifo;
      unsigned short abit0 = (val0 & 0x8000) >> 15;
      unsigned short abit1 = (val1 & 0x8000) >> 15;
      val0 &= 0xfff;
      val1 &= 0xfff;

      if(abit0 == 1){
	std::cout<<" Data not valid (FIFO0 empty?): first bcid="<<bcid<<"  sample="<<i<<std::endl;
	val0 = 0;
      }
      else{
	if(val0 > maxval0){
	  maxval0 = val0;
	  maxbcid0 = bcid;
	}
      }
      fifo0.push_back(val0);

      if(abit1 == 1){
	std::cout<<" Data not valid (FIFO1 empty?): first bcid="<<bcid<<"  sample="<<i<<std::endl;
	val1 = 0;
      }
      else{
	if(val1 > maxval1){
	  maxval1 = val1;
	  maxbcid1 = bcid;
	}
      }
      fifo1.push_back(val1);
    }
    bcid += 8;
    //    cout<<"BCID is now "<<bcid<<"\n";
  }
  
  //print 100 readings around the maximum (if max in close to boundary, first of last 100 readings)

  int index0 = maxbcid0*8-50;
  if(index0 <0) index0 = 0;
  if( (maxbcid0*8 + 50) > 28511) index0 = 28411;
  cout<<"max bcid is "<<maxbcid0<<" fifo0 size is "<<fifo0.size()<<" start from index "<<index0<<"\n";

  int index1 = maxbcid1*8-50;
  if(index1 <0) index1 = 0;
  if( (maxbcid1*8 + 50) > 28511) index1 = 28411;
  cout<<"max bcid is "<<maxbcid1<<" fifo1 size is "<<fifo1.size()<<" start from index "<<index1<<"\n";

  for(int i=0; i<100; ++i){
    cout<<i<<"  "<<fifo0[index0+i]<<"  "<<fifo1[index1+i]<<"\n";
  }
 
}
/*****************/
void readWaveForm()
/*****************/
{
  unsigned short ifpga;
  std::cout<<"Select fpga (0-7)"<<std::endl;
  cin >> ifpga;  
  std::cout<<"selected fpga is"<<ifpga<<std::endl;

  unsigned short val0 =  vLUCROD->CHRegs[ifpga].status & 1;
  unsigned short val1 = (vLUCROD->CHRegs[ifpga].status>>2) & 1;

  std::cout<<" EMPTY status bit before clear: waveaff: val0="<<val0<<"; val1="<<val1<<"\n";

  //clear fifo
  globalCommand(6);
  usleep(5);

  val0 =  vLUCROD->CHRegs[ifpga].status & 1;
  val1 = (vLUCROD->CHRegs[ifpga].status>>2) & 1;
  std::cout<<" EMPTY status bit after clear: waveaff: val0="<<val0<<"; val1="<<val1<<"\n";

  //arm fifo
  globalCommand(1);
 
  // wait for non-empty FIFO
  val0 =  vLUCROD->CHRegs[ifpga].status & 1;
  val1 = (vLUCROD->CHRegs[ifpga].status>>2) & 1;

  unsigned int timeout = 0;
  while ( (val0 == 1) || (val1 == 1)){
    ++timeout;
    if(timeout > 1000000){
      std::cout<<" timeout while waitig for non-empty waveaff: val0="<<val0<<"; val1="<<val1<<"\n";
      return;
    }
    val0 =  vLUCROD->CHRegs[ifpga].status & 1;
    val1 = (vLUCROD->CHRegs[ifpga].status>>2) & 1;    
  }
 
  std::cout<<" found event in fifo after "<<timeout <<" loops: val0, val1 = "<<val0<<", "<<val1<<"\n";
 
  //READ waveforms
  unsigned int nSAMPLES = 64;
  unsigned int BCStart = vLUCROD->CHRegs[ifpga].waveFormBcid & 0xfff;
  cout<<"Reading "<<nSAMPLES<<" from waveaff fifos. Initial bcid is "<< BCStart <<" \n";

  //read waveform 0
  unsigned short rc0 = 0;

  for(unsigned int i=0; i<nSAMPLES; ++i){
    unsigned short val = vLUCROD->CHRegs[ifpga].waveForm0_fifo;
    if( ((val >> 15) & 1) == 1){
      ++rc0;
      val = 0;
    }
    else{
      val &= 0xfff;
      cout<<" fifo 0 (lucrod odd channel in the selected fpga) sample "<<i<<" = "<<val<<"\n";
    }
  }

  val0 =  vLUCROD->CHRegs[ifpga].status & 1;
  timeout = 0;
  while( val0 != 1){
    unsigned short val = vLUCROD->CHRegs[ifpga].waveForm0_fifo;
    ++timeout;
    std::cout<<" EMPY status not set but new reading is 0x"<<HEX(val)<<"\n";
    if(timeout > 10)break;
    val0 =  vLUCROD->CHRegs[ifpga].status & 1;
  }

  
  //read waveform 1
  unsigned short rc1 = 0;
  for(unsigned int i=0; i<nSAMPLES; ++i){
    unsigned short val = vLUCROD->CHRegs[ifpga].waveForm1_fifo;
    if( ((val >> 15) & 1) == 1){
      ++rc1;
      val = 0;
    }
    else{
      val &= 0xfff;
      cout<<" fifo 1 (lucrod even channel in the selected fpga) sample "<<i<<" = "<<val<<"\n";
    }
  }
  timeout = 0;
  val1 = (vLUCROD->CHRegs[ifpga].status>>2) & 1; 
  while( val1 != 1){
    unsigned short val = vLUCROD->CHRegs[ifpga].waveForm1_fifo;
    ++timeout;
    std::cout<<" Empty status not set but new reading is 0x"<<HEX(val)<<"\n";
    if(timeout > 20)break;
    val1 = (vLUCROD->CHRegs[ifpga].status>>2) & 1; 
  }

  unsigned int rc = rc0 + rc1;
  if(rc != 0){
    cout <<":: Found "<< rc <<" not-valid data when reading waveform for CHFPGA "<<ifpga<<"\n";
  }
  
  sleep(1);
  unsigned short status =  vLUCROD->CHRegs[ifpga].status;
  unsigned short wf0 = status & 1;
  unsigned short wf1 = (status>>2)& 1;

  std::cout <<" waveaff fifo 0 emtpy status bit is "<< wf0 <<"; fifo 1 empty status bit is "<<wf1<<"\n";
}
/*****************/
void readChHits()
/*****************/
{
  for(int i=0; i<8; ++i){
    readOneFpgaHits(i);
  }
}
/*****************/
void readOneFpgaHits(int ifpga)
/*****************/
{

  unsigned int hit0[3564];
  unsigned int hit1[3564];

  unsigned int min0 = 10000;
  unsigned int max0 = 0;;
  unsigned int min1 = 1000;
  unsigned int max1 = 0;
//int min0bcid = 0;
//int min1bcid = 0;
  int max0bcid = 0;
  int max1bcid = 0;

  unsigned int nMissMark0 = 0;
  unsigned int nMissMark1 = 0;

  unsigned int OrbLSW = vLUCROD->CHRegs[ifpga].orbitLSW;
  unsigned int OrbMSW = vLUCROD->CHRegs[ifpga].orbitMSW;
  unsigned int OrbC = OrbLSW | (OrbMSW<<16);    

  cout<<" \n  ***  FPGA "<< ifpga <<" orbitcounts="<< OrbC <<"\n";

  for(int i=0; i<3564; ++i){

    unsigned int H0LSW= vLUCROD->CHRegs[ifpga].hitsCh0_fifo;
    unsigned int H0MSW= vLUCROD->CHRegs[ifpga].hitsCh0_fifo;
      
    unsigned int H1LSW= vLUCROD->CHRegs[ifpga].hitsCh1_fifo;
    unsigned int H1MSW= vLUCROD->CHRegs[ifpga].hitsCh1_fifo;
      
    if((H1MSW>>15) != 1) ++nMissMark0;
    if((H0MSW>>15) != 1) ++nMissMark1;
      
    hit0[i] = H0LSW | ((H0MSW & 0x7f)<<16);
    hit1[i] = H1LSW | ((H1MSW & 0x7f)<<16);

    if(hit0[i] > max0){
      max0 = hit0[i];
      max0bcid = i;
    }
    if(hit0[i] < min0){
      min0 = hit0[i];
    //min0bcid = i;
    }
    
    if(hit1[i] > max1){
      max1 = hit1[i];
      max1bcid = i;
    }
    if(hit1[i] < min1){
      min1 = hit1[i];
    //min1bcid = i;
    }
  }

  if( (nMissMark0>0) || (nMissMark1>0))
    cout<<" missing marker ch0 = "<<nMissMark0<< "  ch1 = "<<nMissMark1<<"\n";
  cout<<" hits around max (BCID "<<max0bcid <<") for ch 0 are \n";
  int start = (max0bcid > 8) ? max0bcid - 8 : 0;
  int end = ((3563-max0bcid) > 8) ? max0bcid + 8 : 3564;
  for(int i = start; i<end; ++i)
    std::cout<<hit0[i]<<" ";
  cout<<"\n";    
  cout<<"hits around max (BCID "<<max1bcid<<")for ch 1 are \n";
  start = (max1bcid > 8) ? max1bcid - 8 : 0;
  end = ((3563-max1bcid) > 8) ? max1bcid + 8 : 3564;
  for(int i = start; i<end; ++i)
    std::cout<<hit1[i]<<" ";
  cout<<"\n";

}  
/*****************/
void  handleLBchange()
/*****************/
{
  tstamp ts1, ts2;
  ts_clock(&ts1); 

  readChHits();
  readChCharge();
  readHitOR();
  readHitSum();
  readTotalCharge();

  ts_clock(&ts2); 
  float delta = ts_duration(ts1, ts2);
  cout<<"Time to read LB data = "<<delta*1000000 << " usecs\n";   
}
/*****************/
void changeFifo()
/*****************/
{
  if(nFifoChanges%2 == 0) {
    globalCommand(2);
    cout<<"writing to fifo 1\n";
  }else{
    globalCommand(3);
    cout<<"writing to fifo 2\n";
  }
  nFifoChanges++;
  usleep(100);
}  
/*****************/
void read1ChCharge(int ifpga)
/*****************/
{

  //now read
  double mincharge = 1000000000.;
  double maxcharge = -1.;
  int maxbcid = 0;
  int minbcid = 0;
  double totCharge = 0;

  unsigned short rc = 0;
  unsigned short ov = 0;
  
  std::vector <double> chargeVec;

  unsigned int OrbLSW = vLUCROD->CHRegs[ifpga].orbitLSW;
  unsigned int OrbMSW = vLUCROD->CHRegs[ifpga].orbitMSW;
  unsigned int OrbC = OrbLSW | (OrbMSW<<16);    
  cout<<" \n  ***  FPGA "<< ifpga <<" orbitcounts="<< OrbC <<"\n";


  for(int i=0; i<3564; ++i){    

    bool overflow = true;

    unsigned short  val0 = vLUCROD->CHRegs[ifpga].lumiBlockCharge;
    unsigned short  val1 = vLUCROD->CHRegs[ifpga].lumiBlockCharge;
    unsigned short  val2 = vLUCROD->CHRegs[ifpga].lumiBlockCharge;

    if( (val0 != 0xffff)  || (val1 != 0xffff) || (val2 != 0xffff) ) overflow = false;

    unsigned short marker = (val2 & 0x8000)>>15;
    if( marker != 1){ 
      ++rc ;
    }
    else if(overflow){ //check overflow
      ++ov;
    }
    val2&= 0xf;

    long long BCval = val0 | ((static_cast<unsigned long long>(val1))<<16) |
      ((static_cast<unsigned long long>(val2))<<32);

    double charge = double(BCval);
    totCharge += charge;
    chargeVec.push_back(charge);
    if(maxcharge < charge){
      maxcharge = charge;
      maxbcid = i;
    }
    if(mincharge > charge){
      mincharge = charge;
      minbcid = i;
    }
  }

  if(rc!=0){
    std::cout << ov<<" overflows and "<<rc<<" missing end marker in fpga "<<ifpga<<" charge accumulator\n";
  }

  std::cout<<" LB totCharge (sum over BCID) is "<<totCharge<<"\n";
  std::cout<<" max charge is "<<maxcharge<<" found in bcid "<<maxbcid<<"\n";
  std::cout<<" min charge is "<<mincharge<<" found in bcid "<<minbcid<<"\n";

  double b0 = OrbC*8.*vLUCROD->CHRegs[ifpga].ch0Baseline;
  double b1 = OrbC*8.*vLUCROD->CHRegs[ifpga].ch1Baseline;

  std::cout<<" LB charges in BCID without signals (expected from baselines are): "<<b0<<", "<<b1<<" ( baselines: "<<vLUCROD->CHRegs[ifpga].ch0Baseline <<","<<vLUCROD->CHRegs[ifpga].ch1Baseline<<")\n";
}
/*****************/
void readChCharge()
/*****************/
{
  for(int i=0; i<8; ++i){
    read1ChCharge(i);
  }
}
/*****************/
void selfTriggerFPGA()
/*****************/
{
  int ifpga;
  std::cout<<"Select lucrod FPGA "<<std::endl;
  cin>>ifpga ;
  std::cout<<"selected fpga is "<<ifpga<<")"<<"\n";

  //enable triggers in both channels
  vLUCROD->CHRegs[ifpga].control |= 3 ;

  std::cout<< "CONTR1 register reads 0x" <<std::hex<<vLUCROD->CHRegs[ifpga].control<<std::dec<<"\n";

  //channel 0 
  std::cout<< "  Channel 0 \n"; 
  int timeout = 0;
  unsigned short value = vLUCROD->CHRegs[ifpga].selfTrig & 1 ;
  std::cout<<"trigger bit is "<<value<<"\n";
  while (value == 0){
    timeout++;     
    if (timeout == 1000000){ 
      std::cout << "DataChannelLUCRODscope::getNextFragment: Time out "<<timeout<<"  when waiting for DREADY on fpga " 
		<< ifpga <<", channel 0 \n";
      return;  
     }  
    value = vLUCROD->CHRegs[ifpga].selfTrig & 0x1;
  }
  std::cout<< "STATUS2 register now reads 0x" <<std::hex<<vLUCROD->CHRegs[ifpga].selfTrig<<std::dec<<"\n";
  std::cout<<"trigger bit is "<<value<<"\n";
  //block the buffer and  read it
  vLUCROD->CHRegs[ifpga].control &=  0xfffe;

  //read buffer (64 words fixed depth )
  std::cout<<" data found after "<<timeout<<" loops. Readings: \n";
  for(int i=0; i<64; ++i){
    std::cout<<"sample "<<i<<" = "<<vLUCROD->CHRegs[ifpga].scope0_fifo<<"\n";  
  }

  //channel 1 
  std::cout<< "  Channel 1 \n"; 
  timeout = 0;
  value = (vLUCROD->CHRegs[ifpga].selfTrig & 2)>>1 ;
  std::cout<<"trigger bit is "<<value<<"\n";
  while (value == 0){
    timeout++;     
    if (timeout == 1000000){ 
      std::cout << "DataChannelLUCRODscope::getNextFragment: Time out "<<timeout<<"  when waiting for DREADY on fpga " 
		<< ifpga <<", channel 1 \n";
      return;  
     }  
    value = (vLUCROD->CHRegs[ifpga].selfTrig & 0x2)>>1;
  }
  std::cout<< "STATUS2 register now reads 0x" <<std::hex<<vLUCROD->CHRegs[ifpga].selfTrig<<std::dec<<"\n";
  std::cout<<"trigger bit is "<<value<<"\n";
  //block the buffer and  read it
  vLUCROD->CHRegs[ifpga].control &=  0xfffd;

  //read buffer (64 words fixed depth )
  std::cout<<" data found after "<<timeout<<" loops. Readings: \n";
  for(int i=0; i<64; ++i){
    std::cout<<"sample "<<i<<" = "<<vLUCROD->CHRegs[ifpga].scope1_fifo<<"\n";  
  }


}
/*****************/
void selfTrigger()
/*****************/
{

  int ch;
  std::cout<<"Select lucrod channel "<<std::endl;
  cin >> ch;  
  int ifpga = ch/2;
  std::cout<<"selected channel is "<<ch<<" (fpga "<<ifpga<<")"<<"\n";

  //disable both
  //vLUCROD->CHRegs[ifpga].control & ~3;

  unsigned short fifo = (ch%2 == 0) ?  1 : 0;
  std::cout<< "Selected FPGA fifo " <<fifo<<"\n";
  
  //enable triggers (ch0 in fpga correspond to odd lucrod channel)
  vLUCROD->CHRegs[ifpga].control |= (fifo == 0) ? 0x1 : 0x2 ;
  std::cout<< "CONTR1 register reads 0x" <<std::hex<<vLUCROD->CHRegs[ifpga].control<<std::dec<<"\n";

  //vLUCROD->CHRegs[ifpga].control &= (fifo == 0) ? ~1 : ~2 ;
  //std::cout<< "CONTR1 register reads 0x" <<std::hex<<vLUCROD->CHRegs[ifpga].control<<std::dec<<"\n";

  std::cout<< "STATUS2 register reads 0x" <<std::hex<<vLUCROD->CHRegs[ifpga].selfTrig<<std::dec<<"\n";

  int timeout = 0;
  unsigned short value = (fifo == 0) ? vLUCROD->CHRegs[ifpga].selfTrig & 1 : (vLUCROD->CHRegs[ifpga].selfTrig & 2)>>1 ;
  std::cout<<"trigger bit is "<<value<<"\n";
  while (value == 0){
    timeout++;     
    if (timeout == 1000000){ 
      std::cout << "DataChannelLUCRODscope::getNextFragment: Time out "<<timeout<<"  when waiting for DREADY on fpga " 
		<< ifpga <<" channel "<<ch<<"\n";
      return;  
     }  
    value = (fifo == 0) ? (vLUCROD->CHRegs[ifpga].selfTrig) & 0x1 : (vLUCROD->CHRegs[ifpga].selfTrig & 2)>>1;
  }

  std::cout<< "STATUS2 register now reads 0x" <<std::hex<<vLUCROD->CHRegs[ifpga].selfTrig<<std::dec<<"\n";
  std::cout<<"trigger bit is "<<value<<"\n";

  //block the buffer and  read it
  vLUCROD->CHRegs[ifpga].control &= (fifo == 0)? 0xfffe : 0xfffd;

  //read buffer (64 words fixed depth )
  std::cout<<" data found after "<<timeout<<" loops. Readings: \n";
  if( fifo == 0){
    for(int i=0; i<64; ++i){
      std::cout<<"sample "<<i<<" = "<<vLUCROD->CHRegs[ifpga].scope0_fifo<<"\n";  
    }
  } else {
    for(int i=0; i<64; ++i){
      std::cout<<"sample "<<i<<" = "<<vLUCROD->CHRegs[ifpga].scope1_fifo<<"\n";  
    }        
  }
}
/*****************/
void readOneOrbitCharge()
/*****************/
{
  unsigned short ifpga;
  std::cout<<"Select fpga (0-7)"<<std::endl;
  cin >> ifpga;  
  std::cout<<"selected fpga is"<<ifpga<<std::endl;
 
  //clear fifo
  globalCommand(6);
  //arm fifo
  globalCommand(1);
  usleep(200);

  unsigned short maxQ = 0;
  int maxpos = -1;
  std::vector<unsigned short> charge; 

  unsigned short rc = 0;
  for(int i=0; i<3564; ++i){
    unsigned short val = vLUCROD->CHRegs[ifpga].oneOrbitCharge_fifo;
    unsigned short abit = (val & 0x8000) >> 1;
    if( abit == 1){
      std::cout<<" BCID "<<i<<". Data not valid (FIFO empty?): 0x"<<HEX(val)<<" abit ="<<abit<<std::endl;
      ++rc;
    }
    else{
      val &= 0x7fff;
      if(val > maxQ){
	maxQ = val;
	maxpos = i;
      }
      charge.push_back(val);
    }
  } 

  if(rc != 0){
    cout <<"Found "<< rc <<" not-valid data when reading 1-orbit charge for CHFPGA "<<ifpga<<"\n";
  }

  std::cout<<" max position is "<<maxpos<<", max val is "<<maxQ<<"\n";
  if((maxpos != -1) && (rc == 0)){
    int start = 0;
    if(maxpos>50)
      start = maxpos-50;
    for (int i = start; i<start+100; ++i){
      std::cout<<i<<"  "<<charge[i]<<"\n";
    }
  }

}
/*****************/
void dumpStatusWords()
/*****************/
{
  cout<<"FPGACH status words: \n";
  for(int i = 0; i<8; ++i){
    unsigned short status = vLUCROD->CHRegs[i].status;
    cout<<"  FPGA "<<i<<":\n";
    unsigned short waveaff0empty = status & 1;
    unsigned short waveaff1empty = (status>>2) & 1;
    unsigned short accum1empty = (status>>1) & 1;
    unsigned short waveaff0full = (status>>3) & 1;
    unsigned short waveaff1full = (status>>5) & 1;
    //    unsigned short accum1full = (status>>4) & 1;
    unsigned short fifo1acq = (status>>6) & 1;
    unsigned short fifo2acq = (status>>7) & 1;
    unsigned short globalcomm = (status>>8) & 0xf;
    unsigned short fpgaId = (status>>12) & 0x7;
    (waveaff0empty == 1) ? cout<<"    Waveaff 0 empty\n" : cout<<"    Waveaff 0 NOT empty\n"; 
    (waveaff1empty == 1) ? cout<<"    Waveaff 1 empty\n" : cout<<"    Waveaff 1 NOT empty\n";
    (waveaff0full == 1) ? cout<<"    Waveaff 0 full\n" : cout<<"    Waveaff 0 NOT full\n"; 
    (waveaff1full == 1) ? cout<<"    Waveaff 1 full\n" : cout<<"    Waveaff 1  NOT full\n"; 
    (accum1empty == 1) ? cout<<"    Accum1 empty\n" : cout<<"   Accum1  NOT empty\n"; 
    if (fifo1acq == 1) cout<<"    fifo 1 is acquiring \n"; 
    if (fifo2acq == 1) cout<<"    fifo 2 is acquiring \n"; 
    cout<<"   Last global command is "<<globalcomm<<"\n";
    cout<<"   Fpgaid is "<<fpgaId<<"\n";
  }
  cout<<"\n FPGAM status words: \n";
  unsigned int status = vLUCROD->VMERegs.status;
  unsigned short globalcomm = (status>>4) & 0xf;
  unsigned short  VMEMonFreezed = status & 0x1;
  unsigned short fifo1 = (status>>1) & 0x1;
  unsigned short fifo2 = (status>>2) & 0x1;
  (VMEMonFreezed == 1) ? cout<<"VME monitor freezed \n" : cout<<"VME monitor NOT freezed \n";
  if (fifo1 == 1) cout<<"    fifo 1 is acquiring \n"; 
  if (fifo2 == 1) cout<<"    fifo 2 is acquiring \n"; 
  cout<<"   Last global command is "<<globalcomm<<"\n";

  cout<<"\n FPGAV status words: \n";
  unsigned short ififo = (vLUCROD->MainRegs.status>>2) & 0x3;
  if(ififo == 1)
    cout<<"  cumulating hitOr/Sum i fifo1\n";
  else if(ififo == 2)
    cout<<"  cumulating hitOr/Sum i fifo2\n";
  else
    cout<<"  something WRONG - cumulatig in both fifo?!?!?!?\n";
  
}
