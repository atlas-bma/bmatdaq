/********************************************************/
/*							*/
/* Date: 15 October 2014				*/ 
/* Author: C. Sbarra    	       			*/
/*							*/
/* 18 July 2016 handle FW reload in FPGA (SEU)          */
/*                                                      */
/*** C 2007 - The software with that certain something **/
/*                                                      */
/********************************************************/

#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "DFSubSystemItem/ConfigException.h"
#include "ROSModules/ModulesException.h"

#include "rcd_LUCRODBMA/LucrodBoard.h" 

#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"

#include <sys/types.h>
#include <sys/socket.h>

// Variable to be used for tracing and debugging, for example: 
enum {  DFDB_RCDDETECTOR = 30000 };

static int const NBCID = 3564;

using namespace ROS;
using namespace std;

/**********************/
LucrodBoard::LucrodBoard() 
/**********************/
{
  ERS_LOG("LucrodBoard::constructor Entered");

  //initialize error flags
  m_ignoreChError.fill(false);
  m_ignoreVmeError  = false;
  m_ignoreMainError = false;
  m_nFifoChanges    = 0;
  m_vLUCROD         = 0;
  m_running         = false;

  ERS_LOG("LucrodBoard::constructor Done");
}
/******************************************/
LucrodBoard::~LucrodBoard() 
/*******************************************/
{
  ERS_LOG("LucrodBoard::destructor Entered");
  ERS_LOG("LucrodBoard::destructor Done");
}
/***********************************************************************/
void LucrodBoard::setup(DFCountedPointer<Config> configuration, int latencyValue) 
/************************************************************************/ 
{
  m_configuration = configuration;

  //get VMEbus parameters for the LUCROD MODULE
  m_vmeAddress         = m_configuration->getUInt  ("VMEbusAddress");
  m_vmeWindowSize      = m_configuration->getUInt  ("VMEbusWindowSize");
  m_vmeAddressModifier = m_configuration->getUInt  ("VMEbusAddressModifier");
  m_boardName          = m_configuration->getString("BoardName");
  m_inputNames         = m_configuration->getVector<std::string>("InputNames");
  m_chFpgaFwVersion    = m_configuration->getUInt  ("ChFpgaFwVersion") ; 
  m_mainFpgaFwVersion  = m_configuration->getUInt  ("MainFpgaFwVersion"); 
  m_chOrbitDelay       = m_configuration->getUInt  ("ChOrbitDelay");

  if (latencyValue != 0) {

    ERS_LOG(m_boardName << "::setup updating global delays with latencyValue of " << latencyValue);
    
    m_chOrbitDelay += latencyValue;
  }

  m_internalClock = m_configuration->getBool("InternalClock");

  // common channel settings
  m_outCopyGain       = m_configuration->getUInt("OutSignalCopyGain");
  m_outCopyOffset     = m_configuration->getUInt("OutSignalCopyOffset");
  m_waveFormBcid      = m_configuration->getUInt("BC2WaveFormFifo");
  m_baselineBcidStart = m_configuration->getUInt("BaselineRangeStart");
  m_baselineLength    = m_configuration->getUInt("BaselineLength");
  m_postTrigSamples   = m_configuration->getUInt("PostTriggerSamples");

  // channel-specific settings
  for (int i=0; i<NLUCRODIN; ++i) {
    
    std::ostringstream name; 

    name.str(""); name << "Ch" << i << "FadcOffset";   m_fadcOffsets    [i] = m_configuration->getUInt(name.str().c_str());
    name.str(""); name << "Ch" << i << "Thresh";       m_fadcThresholds [i] = m_configuration->getUInt(name.str().c_str());        
    name.str(""); name << "Ch" << i << "FadcBaseline"; m_channelBaseline[i] = m_configuration->getUInt(name.str().c_str());    
    name.str(""); name << "Ch" << i << "HitDelayFine"; m_hitDelayFine   [i] = m_configuration->getUInt(name.str().c_str());     

    ERS_LOG(m_boardName << "::setup Threshold for channel " << i << " from DB is " << m_fadcThresholds[i]);
  }
  
  for (int i=0; i<NCHFPGA; ++i) {

    std::ostringstream name; 
    
    name.str(""); name << "Fpga" << i << "AccDelayFine"; m_accDelayFine[i] = m_configuration->getUInt(name.str().c_str());    
    name.str(""); name << "Fpga" << i << "CoarseDelays"; m_coarseDelays[i] = m_configuration->getUInt(name.str().c_str());     
    name.str(""); name << "Fpga" << i << "Gain";         m_fadcGains   [i] = m_configuration->getUInt(name.str().c_str());     

    ERS_LOG(m_boardName << "::setup FadcGain for fpga " << i << " from DB is 0x" << HEX(m_fadcGains[i]));
  }
  
  // determine data channel type
  m_scopeEnabled  = m_configuration->getUInt("ScopeEnabled");
  m_internalClock = m_configuration->getBool("InternalClock");
  m_resetBoard    = m_configuration->getBool("resetBoard");

  ERS_LOG(m_boardName << "::setup Done");
}

/**********************************************/
void LucrodBoard::configure(unsigned short const lumiEnabledChannelMask, unsigned short const scopeEnabledChannelMask, unsigned short const fpgaChannelMask) {
/**********************************************/
  
  ERS_LOG(m_boardName << "::configure Entered.");
  ERS_LOG(m_boardName << "::configure scopeChannelMask 0x" << std::hex << scopeEnabledChannelMask);
  ERS_LOG(m_boardName << "::configure lumiChannelMask 0x"  << std::hex << lumiEnabledChannelMask);
  ERS_LOG(m_boardName << "::configure fpga mask 0x"        << std::hex << fpgaChannelMask);
  
  // VME mapping
  err_type ret;
  err_str  rcc_err_str;

  ret = VME_Open();

  if (ret != VME_SUCCESS) {

    ERS_LOG(m_boardName << "::configure Error from VME_Open");
    
    rcc_error_string(rcc_err_str, ret);
    ERS_REPORT_IMPL(ers::fatal, ers::Message, rcc_err_str,);
  }

  ERS_LOG(m_boardName << "::configure VME_Open returned no errors");
  
  VME_MasterMap_t master_map = {m_vmeAddress, m_vmeWindowSize, m_vmeAddressModifier, 0};
  
  ret = VME_MasterMap(&master_map, &m_vmeScHandle);

  if (ret != VME_SUCCESS){
   
    ERS_LOG(m_boardName << "::configure Error from VME_MasterMap");
    
    rcc_error_string(rcc_err_str, ret);
    ERS_REPORT_IMPL(ers::fatal, ers::Message, rcc_err_str,);
  }

  ERS_LOG(m_boardName << "::configure VME_MasterMap returned no errors");

  ret = VME_MasterMapVirtualLongAddress(m_vmeScHandle, &m_vmeVirtualPtr);

  if (ret != VME_SUCCESS) {

    ERS_LOG(m_boardName << "::configure Error from VME_MasterMapVirtualLongAddress");

    rcc_error_string(rcc_err_str, ret);
    ERS_REPORT_IMPL(ers::fatal, ers::Message, rcc_err_str,);
  } 
  
  ERS_LOG(m_boardName << "::configure vmeScHandle is 0x"        << HEX(m_vmeScHandle));
  ERS_LOG(m_boardName << "::configure vmeVirtual pointer is 0x" << HEX(m_vmeVirtualPtr));
  
  m_vLUCROD = reinterpret_cast<LUCRODBoard_regs_t*>(m_vmeVirtualPtr);  

  if (m_resetBoard) {

    m_vLUCROD->MainRegs.control |= 0x4;  // reset of all registers to default

    sleep(5);
  }
  else resetTTCrq();

  //always start from internal clock (move to external at connect!)
  m_vLUCROD->MainRegs.control |= 1;  

  sleep(3);

  for(int i=0; i<NCHFPGA; ++i) // configure all FPGAs 
    configureFpgaCh(i, scopeEnabledChannelMask, lumiEnabledChannelMask);
  
  configureFpgaV(fpgaChannelMask);
}

/******************************************/
void LucrodBoard::resetTTCrq()
/******************************************/
{
  ERS_INFO(m_boardName << "::resetTTCrq (prevent timing issues after power cycle)");

  m_vLUCROD->MainRegs.TTCrqControl = 0x60;

  usleep(200000); // bits 7 and 8 low

  m_vLUCROD->MainRegs.TTCrqControl = 0x1e0; 

  usleep(1000);
}

/******************************************/
void LucrodBoard::configureFpgaV(unsigned short const fpgaChannelMask)
/******************************************/
{
  ERS_LOG(m_boardName << "::configureFpgaV configuring Main FPGA - fw version: 0x" << HEX(m_vLUCROD->MainRegs.version));
  
  //always start from internal clock?
  //m_vLUCROD->MainRegs.control |= 1;
  //sleep(1);

  if (m_mainFpgaFwVersion != m_vLUCROD->MainRegs.version) {
    
    std::ostringstream msg;
    msg << m_boardName << "::configureFpgaV Main FPGA fw version not consistent with DB: 0x" << HEX(m_mainFpgaFwVersion);
    ERS_REPORT_IMPL(ers::error, ers::Message, msg.str().c_str(),);
  }

  // select groups of FPGA for  hit OR/Sums in FPGAV, and trigger (output sento to USA15): 
  // group 1 is Bi, group 2 is BiM

  // uncomment to have first 4 FPGACH in first group and the others in the second group
  m_vLUCROD->MainRegs.control &= ~(0x10);    
  m_hitAndChargeGroups = "FirstLast";
  // uncomment to have even FPGACH in first group and off FPGAs in the second one
  //m_vLUCROD->MainRegs.control |= 0x10; 
  //m_hitAndChargeGroups = "EvenOdd";

  //orbit delay for hits
  m_vLUCROD->MainRegs.ChOrbitDelay = m_chOrbitDelay;

  //set bit 6 to 1 (SEU monitoring) -- only available from main Fpga FW version 0x1B !!!
  m_vLUCROD->MainRegs.control |= 0x40;  
}  

/******************************************/
void LucrodBoard::configureFpgaCh(unsigned short fpga, unsigned short scopeEnabledChannelMask, unsigned short lumiEnabledChannelMask)
/******************************************/
{
  ERS_LOG(m_boardName << "::configureFpgaCh configuring channel FPGA " << fpga << " - fw version: 0x" << HEX(m_vLUCROD->CHRegs[fpga].version));

  if (m_chFpgaFwVersion != m_vLUCROD->CHRegs[fpga].version) {

    std::ostringstream msg;
    msg << m_boardName << "::configureFpgaCh Channel " << fpga << " FPGA firmware version not consistent with DB: 0x" << HEX(m_chFpgaFwVersion);
    ERS_REPORT_IMPL(ers::error, ers::Message, msg.str().c_str(),);
  }
  
  // settings common to the two channels in each FPGA
  m_vLUCROD->CHRegs[fpga].control         = 0;
  m_vLUCROD->CHRegs[fpga].postTrigSamples = m_postTrigSamples;    
  m_vLUCROD->CHRegs[fpga].baselineDef     = (m_baselineBcidStart & 0xfff) | ((m_baselineLength & 0xf)<<12);             
  m_vLUCROD->CHRegs[fpga].ch0OutOffset    = m_outCopyOffset;    
  m_vLUCROD->CHRegs[fpga].ch1OutOffset    = m_outCopyOffset;
  m_vLUCROD->CHRegs[fpga].ch0FadcGain     = m_fadcGains[fpga];
  m_vLUCROD->CHRegs[fpga].ch1FadcGain     = m_fadcGains[fpga];
  m_vLUCROD->CHRegs[fpga].ch0OutGain      = m_outCopyGain;    
  m_vLUCROD->CHRegs[fpga].ch1OutGain      = m_outCopyGain;
  m_vLUCROD->CHRegs[fpga].waveFormBcid    = m_waveFormBcid;
  m_vLUCROD->CHRegs[fpga].coarseDelays    = m_coarseDelays[fpga];    
  m_vLUCROD->CHRegs[fpga].accDelayFine    = m_accDelayFine[fpga];      

  // FPGA-channel specific settings   
  m_vLUCROD->CHRegs[fpga].fixBaselineCh0 = m_channelBaseline[2*fpga+1];    
  m_vLUCROD->CHRegs[fpga].fixBaselineCh1 = m_channelBaseline[2*fpga];    
  m_vLUCROD->CHRegs[fpga].ch0FadcOffset  = m_fadcOffsets    [2*fpga+1];
  m_vLUCROD->CHRegs[fpga].ch1FadcOffset  = m_fadcOffsets    [2*fpga];     

  setChThreshold (fpga, (scopeEnabledChannelMask >>(2*fpga)) & 3); // threshold setting must be dynamic --> use public function. Set to max for unused channels
  setScopeChannel(fpga, (scopeEnabledChannelMask >>(2*fpga)) & 3); // enable scope function for selected channel
  setLumiChannel (fpga, (lumiEnabledChannelMask  >>(2*fpga)) & 3); // select channel for charge

  control_bit_on(fpga, 6); // SEU monitoring: control bit 6 ON 
}

/******************************************/
void LucrodBoard::setScopeChannel(unsigned short fpga, unsigned short channelMask)
/******************************************/
{
  std::ostringstream msg1;
  msg1 << m_boardName << "::setScopeChannel fpga " << fpga 
       << " control word initial value 0x" << std::hex << m_vLUCROD->CHRegs[fpga].control
       << " fpga channel mask = 0x"        << channelMask;
  
  ERS_LOG(msg1.str());
  
  ( channelMask & 1)       ? control_bit_on(fpga, 1) : control_bit_off(fpga, 1);
  ((channelMask & 2) >> 1) ? control_bit_on(fpga, 0) : control_bit_off(fpga, 0);
  
  std::ostringstream msg2;
  msg2 << m_boardName << "::setScopeChannel fpga " << fpga 
       << " control word is now 0x" << std::hex << m_vLUCROD->CHRegs[fpga].control;
  
  ERS_LOG(msg2.str());
}

/******************************************/
void LucrodBoard::setLumiChannel(unsigned short fpga, unsigned short channelMask)
/******************************************/
{
  if      ((channelMask == 1) || (channelMask == 0)) control_bit_on (fpga, 12);
  else if ( channelMask == 2)                        control_bit_off(fpga, 12);
  else {     
    
    std::ostringstream msg;
    msg << m_boardName << "::setLumiChannel fpga " << fpga << ". Both channels enabled for lumi! Charge will be meaningless";
    
    ERS_REPORT_IMPL(ers::warning, ers::Message, msg.str().c_str(),);
  //ERS_INFO(msg.str());
  }
}

/******************************************/
void LucrodBoard::setChThreshold(unsigned short fpga, unsigned short channelMask)
/******************************************/
{
  unsigned short Thresh0 = ((channelMask & 2)>>1) ? m_fadcThresholds[2*fpga+1] : 4095; 
  unsigned short Thresh1 = ( channelMask & 1)     ? m_fadcThresholds[2*fpga]   : 4095; 

  m_vLUCROD->CHRegs[fpga].ch0FadcThreshold = ((m_hitDelayFine[2*fpga+1] & 0xf) << 12) | (Thresh0 & 0xfff);
  m_vLUCROD->CHRegs[fpga].ch1FadcThreshold = ((m_hitDelayFine[2*fpga]   & 0xf) << 12) | (Thresh1 & 0xfff);
}

/******************************************/
void LucrodBoard::setBcid4Waveform(unsigned int bcid)
/******************************************/
{
  for (int i=0; i!=NCHFPGA; ++i)
    m_vLUCROD->CHRegs[i].waveFormBcid = bcid;
}

/******************************************/
void LucrodBoard::setFadcGain(unsigned short fpga, unsigned short gain) {
/******************************************/

  if (m_vLUCROD != NULL) 
    m_vLUCROD->CHRegs[fpga].setFadcGain(gain);
}

/******************************************/
void LucrodBoard::cleanupAndRestartFpgaV(void)
/******************************************/
{
  ERS_INFO(m_boardName << "::cleanupAndRestartFpgaV");
  
  //Clear all accumulators
  globalCommand(4); // stop accumulators 
  globalCommand(5); // clear hits, local charge and charge sum accumulators

  usleep(1000);

  globalCommand(6); // clear waveaff and 1-orbit charge fifo
  
  usleep(1000);

  globalCommand(1);
}

/******************************************/
void LucrodBoard::cleanupAndRestartFpgaCh(unsigned short fpga)
/******************************************/
{
  //following FW reload and reconfig, need to cleanup and restart accumulators

  ERS_INFO(m_boardName << "::cleanupAndRestartFpgaCh fpga " << fpga);

  control_bit_on(fpga, 15);  
}

/******************************************/
void LucrodBoard::unconfigure(void)
/******************************************/
{  
  ERS_LOG(m_boardName << "::unconfigure Entered");

  for (int i=0; i<NCHFPGA; ++i) 
    m_vLUCROD->CHRegs[i].control = 0;

  err_type ret;
  err_str rcc_err_str;

  ret = VME_MasterUnmap(m_vmeScHandle);  

  if (ret != VME_SUCCESS) {
  
    std::ostringstream msg;
    msg << m_boardName << "::unconfigure Error from VME_MasterUnmap";    
    ERS_LOG(msg.str().c_str());
    
    rcc_error_string(rcc_err_str, ret);
    ERS_REPORT_IMPL(ers::warning, ers::Message, rcc_err_str,);
  }

  ret = VME_Close();

  if (ret != VME_SUCCESS) {

    std::ostringstream msg;
    msg << m_boardName << "::unconfigure Error from VME_Close";    
    ERS_LOG(msg.str().c_str());
    
    rcc_error_string(rcc_err_str, ret);
    ERS_REPORT_IMPL(ers::warning, ers::Message, rcc_err_str,);
  }
}

/***********************************************/
unsigned short LucrodBoard::move2ExternalClock()
/***********************************************/
{  
  unsigned short rc = checkTTCrq(); // TTCrq must be locked and ready

  if (rc != 0) {

    std::ostringstream msg; msg << m_boardName << "::move2ExternalClock TTCrq status not as expected. Err code =" << rc << " waiting 1 sec ...";    

    ERS_REPORT_IMPL(ers::warning, ers::Message, msg.str().c_str(),);  

    sleep(1);
  }

  m_vLUCROD->MainRegs.control &= 0xfffe; // set Lucrod clock on TTCrq clock (the orbit must be present in TTCrq!) 

  usleep(2000);

  unsigned short val = (m_vLUCROD->MainRegs.status>>1) & 1;

  if (val != 0) {

    usleep(20000); // wait and check again

    val = (m_vLUCROD->MainRegs.status>>1) & 1;

    if (val != 0) {

      rc = val;

      std::ostringstream msg; msg << m_boardName << "::move2externalclock still on internal clock.";    

      ERS_REPORT_IMPL(ers::warning, ers::Message, msg.str().c_str(),);
    }
  }
  
  return rc;
}

/**************************************/
void LucrodBoard::connect(void)
/**************************************/
{
  ERS_LOG(m_boardName << "::connect Entered");

  checkBusError();

  usleep(1000); // wait for signals from TTC system in USA15

  if (!m_internalClock) {

    unsigned short rc = move2ExternalClock();

    if (rc != 0) {

      std::ostringstream msg; msg << m_boardName << "::connect TTCrq status not as expected. Err code = " << rc;    

      ERS_REPORT_IMPL(ers::warning, ers::Message, msg.str().c_str(),);  
    } 
    
    unsigned short val = (m_vLUCROD->MainRegs.status>>1) & 1;
    
    if (val != 0) {

      usleep(2000); // wait and check again
      
      val = (m_vLUCROD->MainRegs.status>>1) & 1;

      if (val != 0) {
	
	std::ostringstream msg; msg << m_boardName << "::connect still on internal clock: no orbits from TTC system yet?";    
	
	ERS_REPORT_IMPL(ers::error, ers::Message, msg.str().c_str(),);
      }
    }
  }
  
  ERS_LOG(m_boardName << "::connect Done");
}

/*****************************************/
void LucrodBoard::disconnect(void)
/*****************************************/
{
  ERS_LOG(m_boardName << "::disconnect Entered");
  ERS_LOG(m_boardName << "::disconnect Done");
}    

/********************************************/    
void LucrodBoard::prepareForRun()
/********************************************/
{    
  ERS_LOG(m_boardName << "::prepareForRun Entered");
  
  m_running = true;

  if (m_scopeEnabled == 0) waveFormEnable(); // global enable for waveaff (trigger on specific BCID)

  sleep(1);

  for (int i=0; i<NLUCRODIN; ++i)
    ERS_INFO(m_boardName << "::prepareForRun Channel " << i << " Baseline " << getChBaseline(i) << " Threshold " << getChThreshold(i) << " " << m_inputNames[i]);
}

/**************************************/
void LucrodBoard::stopDC(void)
/**************************************/
{
  ERS_LOG(m_boardName << "::stopDC Entered");
  
  m_running = false;
    
  globalCommand(4); // stop accumulators on all fpga (hits and charge)
  globalCommand(5);

  usleep(1000);
  
  globalCommand(6); // clear waveaff and 1-orbit charge fifo

  usleep(1000);

  ERS_LOG(m_boardName << "::stopDC Done");
}

/***********************************************/
void LucrodBoard::reloadFpgaChFW(unsigned short fpga)
/***********************************************/
{
  ERS_REPORT_IMPL(ers::warning, ers::Message, m_boardName << "::reloadFpgaChFW reloading CHANNEL FPGA FW " << fpga << "-" << fpga+1,);  
  
  m_ignoreChError[fpga] = true;

  m_vLUCROD->CHRegs[fpga].control2 |= 1;
  m_vLUCROD->CHRegs[fpga].control  |= 0x20;

  usleep(1000);

  m_vLUCROD->CHRegs[fpga].control2 &= 0xfffe;   
  m_vLUCROD->CHRegs[fpga].control  &= 0xffdf;
}
 
/***********************************************/
void LucrodBoard::reloadFpgaVFW()
/***********************************************/
{
  ERS_REPORT_IMPL(ers::warning, ers::Message, m_boardName << "::reloadFpgaVFW reloading MAIN FPGA FW",);  

  m_ignoreMainError = true;

  m_vLUCROD->MainRegs.control |= 0x8; 

  sleep(2);
} 

/***********************************************/
bool LucrodBoard::FpgaChSEU(unsigned short fpga)
/***********************************************/
{ 
  bool SEU = false;

  unsigned short val = m_vLUCROD->CHRegs[fpga].control;

  val = (val >> 6)&1; // check bit 6 of control word

  if (val == 0) {

    SEU = true;

    m_ignoreChError[fpga] = true; // ignore further errors until fpga is reconfigured
  }

  return SEU;
}

/***********************************************/
bool LucrodBoard::FpgaVSEU()
/***********************************************/
{
  bool SEU = false;

  unsigned int val = m_vLUCROD->MainRegs.control;

  val = (val >> 6)&1; // check bit 6 of control word

  if (val == 0) {

    SEU = true;

    m_ignoreMainError = true;
  }

  return SEU;
}

/***********************************************/
void LucrodBoard::dumpFADC(unsigned int ich)
/***********************************************/
{
  unsigned short fadc   = m_vLUCROD->CHRegs[ich/2].fadc_read;

  ERS_INFO(m_boardName << "::dumpFADC *** Channel " << ich << ": FADC direcly read value is " << fadc);
}

/***********************************************/
void LucrodBoard::checkClock()
/***********************************************/
{
  unsigned short val = (m_vLUCROD->MainRegs.status>>1) & 1;

  if (val != 0) {

    std::ostringstream msg; msg << m_boardName << "::checkClock using internal clock. trying to pass to LHC one";    

    ERS_REPORT_IMPL(ers::error, ers::Message, msg.str().c_str(),);   

    move2ExternalClock();
  }

  unsigned short orbit = m_vLUCROD->MainRegs.status & 1;

  if (orbit != 0) {
    
    std::ostringstream msg; msg << m_boardName << "::checkClock No orbit from TTCrq. Using local orbit!!!";    

    ERS_REPORT_IMPL(ers::error, ers::Message, msg.str().c_str(),);   
  }
  else {
   
    unsigned short BCcount = m_vLUCROD->MainRegs.bcCountCheck;
    unsigned short orboff  = m_vLUCROD->MainRegs.ChOrbitDelay;

    if ((orboff-BCcount) != 1) {      
      if (orboff!=0 || BCcount!=0xDEB) {

	std::ostringstream msg;

	msg << m_boardName << "::checkClock Disagreament between BCcount and ORBOFF:"
	    << " BCCount = " << val
	    << " ORBOFF = "  << m_vLUCROD->MainRegs.ChOrbitDelay;    

	ERS_REPORT_IMPL(ers::warning, ers::Message, msg.str().c_str(),);   
      }
    }
  }
}

/***********************************************/
void LucrodBoard::checkBusError()
/***********************************************/
{
  unsigned short busError = m_vLUCROD->MainRegs.busErrorCode;

  if (busError != 0) {

    unsigned short add            =  busError & 0xfff;
    bool           reading        = (busError & 0x1000)>>12;
    bool           externalError  = (busError & 0x2000)>>13;
    bool           badAddModifier = (busError & 0x4000)>>14;
    bool           crateFailure   = (busError & 0x8000)>>15;
 
    std::ostringstream msg; msg << m_boardName << "::checkBusError at address 0x" << HEX(add);

    (reading)        ? msg << " while reading "            : msg << " while writing ";
    (externalError)  ? msg << " a different board "        : msg << " this board ";
    (badAddModifier) ? msg << " with bad addressmodifier " : msg << " ";
    (crateFailure)   ? msg << " error on VME crate "       : msg << " error is NOT on VME crate";
    
  //ERS_REPORT_IMPL(ers::warning, ers::Message, msg.str(),);  
    ERS_INFO(msg.str());  

    m_vLUCROD->MainRegs.control |= 0x20; // clear register for Bus error Code
    m_vLUCROD->MainRegs.control &= 0xffdf;
  }
}

/***********************************************/
unsigned short LucrodBoard::checkTTCrq()
/***********************************************/
{
  unsigned short status = (m_vLUCROD->MainRegs.status)>>6;

  ERS_LOG(m_boardName << "::checkTTCrq status reads " << status);   
  
  if (status != 3) {

    resetTTCrq(); // try to recover
    
    status = (m_vLUCROD->MainRegs.status)>>6; // check status again
    
    if      (status == 3) return 0;
    else if (status >  3) { ERS_INFO(m_boardName << "::checkTTCrq TTCrq is in error");          return 2; }
    else if (status == 1) { ERS_INFO(m_boardName << "::checkTTCrq TTCrq not ready");            return 4; }
    else if (status == 2) { ERS_INFO(m_boardName << "::checkTTCrq TTCrq not locked");           return 8; }
    else if (status == 0) { ERS_INFO(m_boardName << "::checkTTCrq TTCrq not ready nor locked"); return 0x10; }
  }
  
  return 0;
}

/***********************************************/
void LucrodBoard::fillISInfo(std::string name, ModuleLucrodBMAInfoNamed & is_entry)
/***********************************************/
{ 
//ERS_LOG("fill IsInfo has been called");

  is_entry.LucrodName             = name;
  is_entry.MainFpgaFW             = m_vLUCROD->MainRegs.version; 
  is_entry.LumiEnabledChannelMask = m_vLUCROD->MainRegs.ChOrSumEnabledMask;
  is_entry.InputGroups            = m_hitAndChargeGroups;
  is_entry.Bcid2WaveFormFifo      = m_vLUCROD->CHRegs[0].waveFormBcid;
  is_entry.BaselineFirstBcid      = m_vLUCROD->CHRegs[0].baselineDef & 0xfff;
  is_entry.BaselineLength         = pow(2, (m_vLUCROD->CHRegs[0].baselineDef & 0xf000)>>12);
  
  std::vector<unsigned short> ChFpgaFW;
  std::vector<unsigned short> FADCgains;
  std::vector<unsigned short> FADCoffsets;
  std::vector<unsigned short> AnalogOUToffsets;
  std::vector<unsigned short> AnalogOUTgains;
  std::vector<unsigned short> ChannelThresholds;
  std::vector<unsigned short> ChannelBaselines;          
  std::vector<unsigned short> ChannelHitFineDelay;          
 
  for (int i=0; i<NCHFPGA; ++i) {
    
    unsigned short val = 0;

    val = m_vLUCROD->CHRegs[i].version; ChFpgaFW.push_back(val);
    
    val = m_vLUCROD->CHRegs[i].ch1FadcGain; FADCgains.push_back(val);
    val = m_vLUCROD->CHRegs[i].ch0FadcGain; FADCgains.push_back(val);

    val = m_vLUCROD->CHRegs[i].ch1FadcOffset; FADCoffsets.push_back(val);
    val = m_vLUCROD->CHRegs[i].ch0FadcOffset; FADCoffsets.push_back(val);

    val = m_vLUCROD->CHRegs[i].ch1OutGain; AnalogOUTgains.push_back(val);
    val = m_vLUCROD->CHRegs[i].ch0OutGain; AnalogOUTgains.push_back(val);

    val = m_vLUCROD->CHRegs[i].ch1OutOffset; AnalogOUToffsets.push_back(val);
    val = m_vLUCROD->CHRegs[i].ch0OutOffset; AnalogOUToffsets.push_back(val);

    val = m_vLUCROD->CHRegs[i].ch1FadcThreshold & 0xfff; ChannelThresholds.push_back(val);
    val = m_vLUCROD->CHRegs[i].ch0FadcThreshold & 0xfff; ChannelThresholds.push_back(val);

    val = (m_vLUCROD->CHRegs[i].ch1FadcThreshold & 0xf000)>>12; ChannelHitFineDelay.push_back(val);
    val = (m_vLUCROD->CHRegs[i].ch0FadcThreshold & 0xf000)>>12; ChannelHitFineDelay.push_back(val);

    val = m_vLUCROD->CHRegs[i].ch1Baseline; ChannelBaselines.push_back(val);
    val = m_vLUCROD->CHRegs[i].ch0Baseline; ChannelBaselines.push_back(val); 
  }
  
  for (int i=0; i<NLUCRODIN; ++i)
    ChannelThresholds[i] -= ChannelBaselines[i];
  
  is_entry.ChFpgaFW            = ChFpgaFW;
  is_entry.FADCgains           = FADCgains;
  is_entry.FADCoffsets         = FADCoffsets;
  is_entry.AnalogOUTgains      = AnalogOUTgains;
  is_entry.AnalogOUToffsets    = AnalogOUToffsets;
  is_entry.ChannelThresholds   = ChannelThresholds;
  is_entry.ChannelBaselines    = ChannelBaselines;   
  is_entry.ChannelHitFineDelay = ChannelHitFineDelay;   
}

/***********************************************/
void LucrodBoard::waveFormEnable()
/***********************************************/
{ 
  globalCommand(1);
}

/***********************************************/
void LucrodBoard::changeAccFifo()
/***********************************************/
{ 
  unsigned short cmd = (m_nFifoChanges%2 == 0) ? 2 : 3;

  globalCommand(cmd);

  m_nFifoChanges++;
}

/*******************************************************/
void LucrodBoard::testGlobalCommand(unsigned short code)
/*******************************************************/
{
  for (int i=0; i<NCHFPGA; ++i) {
    
    unsigned short status = m_vLUCROD->CHRegs[i].status;
    unsigned short cmd = (status >> 8) & 0xf;
    unsigned short id  =  status >> 12;

    if (cmd != code) {
      
      std::ostringstream text; 
      text << m_boardName << "::testGlobalCommand Problems receiving global commad in fpgach " << i 
	   << ": sent = "  << code
	   << ", found = " << cmd 
	   << ", id = "    << id;

      ERS_REPORT_IMPL(ers::warning, ers::Message, text.str(),);

      status = m_vLUCROD->CHRegs[i].status; // try again

      cmd = (status >> 8) & 0xf;
      
      if (cmd != code) { // reload fpgach FW

	int badfpgach = (i%2 == 0) ? i : i-1;

	ERS_REPORT_IMPL(ers::warning, ers::Message, m_boardName << "::testGlobalCommand Trying to reload FW in FPGACH " << badfpgach,);

	reloadFpgaChFW(badfpgach);       
      }  
    }    
  }
}

/*******************************************************/
void LucrodBoard::globalCommand(unsigned short code)
/*******************************************************/
{
  m_vLUCROD->MainRegs.globalCMD = code;

  testGlobalCommand(code);

  m_vLUCROD->MainRegs.globalCMD = 0;

  testGlobalCommand(0);
}  

//***********************************************/
void LucrodBoard::setThresholds(unsigned short value) // only used with user command for special tests  
/***********************************************/
{
  for(int i=0; i<NCHFPGA; ++i) {

    unsigned short base1 = m_vLUCROD->CHRegs[i].ch1Baseline;
    unsigned short base0 = m_vLUCROD->CHRegs[i].ch0Baseline;

    m_vLUCROD->CHRegs[i].ch0FadcThreshold &= 0xf000; // keep the MSB unchanged for the delay
    m_vLUCROD->CHRegs[i].ch0FadcThreshold |= ((base0+value)&0xfff);
    m_vLUCROD->CHRegs[i].ch1FadcThreshold &= 0xf000; // keep the MSB unchanged for the delay
    m_vLUCROD->CHRegs[i].ch1FadcThreshold |= ((base1+value)&0xfff);
  }
}

/***********************************************/
long long LucrodBoard::getChannelHits(unsigned short ch, std::array<uint32_t, NBCID> & fifo)
/***********************************************/
{
  int                ifpga   = ch/2;
  uint32_t           HitLSW  = 0;
  uint32_t           HitMSW  = 0;
  unsigned long long totHits = 0;
  unsigned short     nerr    = 0;
  unsigned short     nswap   = 0;

  for (int i=0; i<NBCID; ++i) {
  
    uint32_t nhits = 0;

    if (ch%2 == 0) { // channel 0 in selected FPGA

      HitLSW = m_vLUCROD->CHRegs[ifpga].hitsCh1_fifo & 0xffff;
      HitMSW = m_vLUCROD->CHRegs[ifpga].hitsCh1_fifo & 0xffff;     
    }
    else { // channel 1 in selected FPGA

      HitLSW = m_vLUCROD->CHRegs[ifpga].hitsCh0_fifo & 0xffff;
      HitMSW = m_vLUCROD->CHRegs[ifpga].hitsCh0_fifo & 0xffff;      
    }

    if ((HitMSW>>15) != 1) {
      if((HitLSW>>15) == 1) ++nswap;
      else                  ++nerr;
    }
    else {

      if ((HitLSW == 0xffff) && (HitMSW == 0xffff)) {

        std::ostringstream msg; msg << m_boardName << "::getChannelHits Overflow for BCID " << i << " channel " << ch;

        ERS_REPORT_IMPL(ers::warning, ers::Message, msg.str(),);
      }

      HitMSW &= 0x7f;

      nhits = HitLSW | (HitMSW<<16); 
    }

    fifo[i]  = nhits;
    totHits += nhits;
  }

  if (nerr != 0) {

    unsigned short status = m_vLUCROD->CHRegs[ifpga].status;
    unsigned short fifo1 = (status>>6) & 1;
    unsigned short fifo2 = (status>>7) & 1;

    std::ostringstream msg;
    msg << m_boardName << "::getChannelHits Missing marker in " << nerr << " BCIDs when reading hits for channel " << ch
	<< ". Writing to fifo1 = " << fifo1
	<< ". Writing to fifo2 = " << fifo2;

  //ERS_REPORT_IMPL(ers::warning, ers::Message, msg.str(),);
    ERS_INFO(msg.str());
  }

  if (nswap != 0){

    unsigned short status = m_vLUCROD->CHRegs[ifpga].status;
    unsigned short fifo1 = (status>>6) & 1;
    unsigned short fifo2 = (status>>7) & 1;
    
    std::ostringstream msg;
    msg << m_boardName << "::getChannelHits Swapped WORDS in " << nswap << " BCIDs when reading hits for channel " << ch
	<< ". Writing to fifo1 = " << fifo1
	<< ". Writing to fifo2 = " << fifo2;
    
    ERS_REPORT_IMPL(ers::warning, ers::Message, msg.str(),);
  }

  return totHits;
}

/***********************************************/     
unsigned short LucrodBoard::getChGain(int ch) 
/***********************************************/
{     
  if (ch%2 == 0) return m_vLUCROD->CHRegs[ch/2].ch1FadcGain;
  else           return m_vLUCROD->CHRegs[ch/2].ch0FadcGain;
}

/***********************************************/     
unsigned short LucrodBoard::getChBaseline(int ch) 
/***********************************************/
{     
  if (ch%2 == 0) return m_vLUCROD->CHRegs[ch/2].ch1Baseline;
  else           return m_vLUCROD->CHRegs[ch/2].ch0Baseline;
}

/***********************************************/     
unsigned short LucrodBoard::getChThreshold(int ch) 
/***********************************************/
{     
  if (ch%2 == 0) return m_vLUCROD->CHRegs[ch/2].ch1FadcThreshold&0xfff;
  else           return m_vLUCROD->CHRegs[ch/2].ch0FadcThreshold&0xfff;
}

/***********************************************/     
void LucrodBoard::setChHitFineDelay(unsigned short ch, unsigned short value) 
/***********************************************/
{     
  if (ch%2 == 0) {

    unsigned short currentVal =  m_vLUCROD->CHRegs[ch/2].ch1FadcThreshold&0xfff;

    m_vLUCROD->CHRegs[ch/2].ch1FadcThreshold = currentVal | ((value&0xf)<<12);
  }
  else {

    unsigned short currentVal =  m_vLUCROD->CHRegs[ch/2].ch0FadcThreshold&0xfff;
    
    m_vLUCROD->CHRegs[ch/2].ch0FadcThreshold = currentVal | ((value&0xf)<<12);
  }
}

/***********************************************/     
unsigned short LucrodBoard::getChHitFineDelay(unsigned short ch) 
/***********************************************/
{     
  if (ch%2 == 0) return (m_vLUCROD->CHRegs[ch/2].ch1FadcThreshold&0xf000)>>12;
  else           return (m_vLUCROD->CHRegs[ch/2].ch0FadcThreshold&0xf000)>>12;
}

/***********************************************/     
void LucrodBoard::setFineDelay(unsigned short fpga, unsigned short value) 
/***********************************************/
{     
  m_vLUCROD->CHRegs[fpga].accDelayFine = value;
}

/***********************************************/     
unsigned short LucrodBoard::getFineDelay(unsigned short fpga) 
/***********************************************/
{     
  return m_vLUCROD->CHRegs[fpga].accDelayFine;
}

/***********************************************/     
void LucrodBoard::setChOrbitDelay(unsigned short delay) 
/***********************************************/
{     
  m_vLUCROD->MainRegs.ChOrbitDelay = delay;
}

/***********************************************/
void LucrodBoard::control_bit_on( u_short ifpga, u_short pos)
/***********************************************/
{
  unsigned short val = (1<<pos);

//ERS_LOG("control bit off for bit " << pos << " ANDing with 0x" << HEX(val));

  m_vLUCROD->CHRegs[ifpga].control |= val;

  usleep(1);
}

/***********************************************/
void LucrodBoard::control_bit_off( u_short ifpga, u_short pos)
/***********************************************/
{
  unsigned short val = ~(1<<pos);

//ERS_LOG("control bit off for bit " << pos << " ANDing with 0x" << HEX(val));

  m_vLUCROD->CHRegs[ifpga].control &= val;

  usleep(1);
}

/***********************************************/
unsigned int LucrodBoard::getOrbitCounts(unsigned int &status1, unsigned int &status2)
/***********************************************/
{
  unsigned int vOrbit = m_vLUCROD->MainRegs.orbCountLSW; // FPGAV  

  vOrbit |= (m_vLUCROD->MainRegs.orbCountMSW)<<16;

  if (vOrbit == 0 && m_running) { // if main FPGA is screwed up, no way we can use data

    std::ostringstream txt; txt << m_boardName << "::getOrbitCounts Found 0 Orbits in Main FPGA for latest LB. m_ignoreMainError = " << m_ignoreMainError;     

    ERS_REPORT_IMPL(ers::warning, ers::Message, txt.str(),);
    
    status1 = 1;
    status2 = 1;
    
    if (!m_ignoreMainError) { // should we reload the FW?

      ERS_REPORT_IMPL(ers::warning, ers::Message, m_boardName << "::getOrbitCounts Reloading FW",);
      
      reloadFpgaVFW();
    } 
  }
  else m_ignoreMainError = false;

  unsigned int diff = 0;
  unsigned int chOrbit[NCHFPGA];

  for (int i=0; i<NCHFPGA; ++i) { // FPGACH

    unsigned int msw = (unsigned int)m_vLUCROD->CHRegs[i].orbitMSW;
    
    chOrbit[i] = (msw<<16) |  m_vLUCROD->CHRegs[i].orbitLSW;
    
    if (chOrbit[i] != vOrbit) {
      
      unsigned int thisdiff = (chOrbit[i] > vOrbit) ? (chOrbit[i]-vOrbit) : (vOrbit-chOrbit[i]);
      
      diff = std::max(diff, thisdiff);
    }
  }
  
  if (diff > 1) {
    
    std::ostringstream txt;
    txt << m_boardName << "::getOrbitCounts Inconsistencies between Orbits in Main and CH FPGA:"
	<< " Main = " << vOrbit << " CH = " 
	<< chOrbit[0] << ", " << chOrbit[1] << ", " << chOrbit[2] << ", " << chOrbit[3] << ", " 
	<< chOrbit[4] << ", " << chOrbit[5] << ", " << chOrbit[6] << ", " << chOrbit[7];     

    ERS_REPORT_IMPL(ers::warning, ers::Message, txt.str(),);   
    
    for (int i=0; i<NCHFPGA; ++i) { 
      
      if (chOrbit[i] == 0 && m_running) { // if the number of orbits in some FPGA is actually 0, we probably need a FW reload           
	
	//set bad status
	(i%2 == 0) ? status1 = 1 : status2 = 1; // LUCROD-PMT board
	
	if (!m_ignoreChError[i]) { // reload FW
	  
	  int badfpga = (i%2 == 0) ? i : i-1;
	  
	  ERS_REPORT_IMPL(ers::warning, ers::Message, m_boardName << "::getOrbitCounts Trying to reload FpgaCH FW",);
	  
	  reloadFpgaChFW(badfpga);
	}	  
      }      
    }      
  }
  else m_ignoreChError.fill(false); // reset flags after recovery
  
  return vOrbit;
}
