#ifndef MLUCRODISDATA_H
#define MLUCRODISDATA_H

struct LucrodIsData{

  unsigned int orbitsInLB;         
  
  std::array< std::array<uint32_t, NBCID>, NLUCRODIN > chHit;
  std::array<uint32_t, NBCID> hitOR1;
  std::array<uint32_t, NBCID> hitOR2;
  std::array<uint32_t, NBCID> hitSum1;
  std::array<uint32_t, NBCID> hitSum2;
  std::array<double,   NBCID> chargeSum1;
  std::array<double,   NBCID> chargeSum2;
  std::array<double,   NBCID> chargeSum1_noPed;
  std::array<double,   NBCID> chargeSum2_noPed;
  std::array< std::array<double,NBCID>, NCHFPGA> fpgaCharge;
};
#endif // MLUCRODISDATA_H
