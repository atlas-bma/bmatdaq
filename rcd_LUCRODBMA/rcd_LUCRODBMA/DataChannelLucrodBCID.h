/********************************************************/
/*							*/
/* Date: October 2014   				*/ 
/* Author: Carla Sbarra					*/
/*							*/
/***                                                   **/

#ifndef DATACHANNELLUCRODBCID_H
#define DATACHANNELLUCRODBCID_H

#include "ROSCore/SingleFragmentDataChannel.h"
#include "rcd_LUCRODBMA/SampleCollection.h"
#include <boost/thread/mutex.hpp>

namespace ROS 
{
  class DataChannelLucrodBCID : public SingleFragmentDataChannel 
  {

  public:    

    DataChannelLucrodBCID(u_int id, 
			  u_int configId, 
			  u_int rolPhysicalAddress, 
			  u_long vmeBaseVA, 
			  DFCountedPointer<Config> configuration, 
			  boost::mutex& mutex, 
			  sampleCollection& collection, 
			  unsigned short channelMask);

    virtual ~DataChannelLucrodBCID() ;
    virtual int getNextFragment(u_int* buffer, int max_size, u_int* status, unsigned long pciAddress = 0);
    virtual DFCountedPointer < Config > getInfo();
      
  private:

    volatile LUCRODBoard_regs_t* m_vLUCROD;

    u_int m_max_wait;
    u_int m_min_wait;
    u_int m_channelId;
    u_int m_channel_number;  

    bool m_read1[NCHFPGA];
    bool m_read0[NCHFPGA];

    boost::mutex& m_mutex;
    sampleCollection& m_sampleCollection;

    u_int   m_nchannels;  
    u_short m_enabledChannelMask;  

    enum Statuswords 
    {
      S_OK       = 0,
      S_TIMEOUT  = 1,
      S_OVERFLOW = 2,
      S_BADDATA  = 3,
      S_NODATA   = 4
    };
  };
}
#endif //DATACHANNELLUCRODBCID_H
