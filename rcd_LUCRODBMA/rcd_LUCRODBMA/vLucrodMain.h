#ifndef _VLUCRODMAIN_H
#define _VLUCRODMAIN_H

//
// MAIN-FPGA register map
//

struct LUCRODMain_regs_t {
  unsigned short version;                    // base + 0x00   R/W
  unsigned short debug;                      // base + 0x02   R/W
  unsigned short control;                    // base + 0x04   R/W
  unsigned short LumatOrbitDelay;            // base + 0x06
  unsigned short status;                     // base + 0x08   R
  unsigned short flashLSB;                   // base + 0x0a   R/W
  unsigned short flashMSB;                   // base + 0x0c   R/W
  unsigned short flashData;                  // base + 0x0e   R/W
  unsigned short flashStatus;                // base + 0x10   R/W
  unsigned short busErrorCode;               // base + 0x12   R
  unsigned short phasck;                     // base + 0x14   R/W
  unsigned short globalCMD;                  // base + 0x16   R/W
  unsigned short ChOrbitDelay;               // base + 0x18   R/W
  unsigned short ChOrSumEnabledMask;         // base + 0x1a   R/W
  unsigned short HitOrSumOrbitDelay;         // base + 0x1c   R/W
  unsigned short bcCountCheck;               // base + 0x1e
  unsigned short dummy1[2];                  // base + 0x20-0x22
  unsigned short TTCrqControl;               // base + 0x24   R/W
  unsigned short dummy2;                     // base + 0x26
  unsigned short lnkRW0;                     // base + 0x28   R/W
  unsigned short lnkRW1;                     // base + 0x2a   R/W
  unsigned short lnkRW2;                     // base + 0x2c   R/W
  unsigned short lnkRW3;                     // base + 0x2e   R/W
  unsigned short lnkRW4;                     // base + 0x30   R/W
  unsigned short lnkRW5;                     // base + 0x32   R/W
  unsigned short lnkRW6;                     // base + 0x34   R/W
  unsigned short lnkRW7;                     // base + 0x36   R/W
  unsigned short lnkRO0;                     // base + 0x38   R/W
  unsigned short lnkRO1;                     // base + 0x3a   R/W
  unsigned short lnkRO2;                     // base + 0x3c   R/W
  unsigned short lnkRO3;                     // base + 0x3e   R/W
  unsigned short lnkRO4;                     // base + 0x40   R/W
  unsigned short lnkRO5;                     // base + 0x42   R/W
  unsigned short lnkRO6;                     // base + 0x44   R/W
  unsigned short lnkRO7;                     // base + 0x46   R/W
  unsigned short grp1hitOR;                  // base + 0x48   R/W
  unsigned short orbCountLSW;                // base + 0x4a   R/W
  unsigned short orbCountMSW;                // base + 0x4c   R/W
  unsigned short dummy3;                     // base + 0x4e 
  unsigned short grp2hitOR;                  // base + 0x50   R/W
  unsigned short dummy4[3];                  // base + 0x52 
  unsigned short grp1hitSum;                 // base + 0x58   R/W
  unsigned short dummy5[3];                  // base + 0x5a 
  unsigned short grp2hitSum;                 // base + 0x60   R/W
};

#endif
