/********************************************************/
/*							*/
/********************************************************/
#ifndef LUCRODBOARD_H
#define LUCRODBOARD_H

#include <string>
#include <vector>

#include "ROSCore/ReadoutModule.h"
#include "rcd_LUCRODBMA/vLucrod.h"
#include "rcd_LUCRODBMA/ModuleLucrodBMAInfoNamed.h"

namespace ROS 
{
  class LucrodBoard {

    public:

      void setup        (DFCountedPointer<Config> configuration, int latencyValue);
      void configure    (unsigned short const lumiEnabledChannelMask, unsigned short const scopeEnabledChannelMask, unsigned short const fpgaChannelMask);
      void prepareForRun(void);
      void stopDC       (void);
      void unconfigure  (void);
      void connect      (void);
      void disconnect   (void);

      LucrodBoard();
      virtual ~LucrodBoard();

      void           resetTTCrq();
      unsigned short checkTTCrq();
      void           changeAccFifo();

      void fillISInfo(std::string name, ModuleLucrodBMAInfoNamed & is_entry);

      unsigned short getChGain     (int ch);
      unsigned short getChBaseline (int ch);
      unsigned short getChThreshold(int ch);
      u_long         getVmeVirtualPtr()   const { return m_vmeVirtualPtr; }; 
      unsigned short getChOrbitDelay()    const { return m_chOrbitDelay; };
      unsigned short getFineDelay     (unsigned short fpga);
      unsigned short getChHitFineDelay(unsigned short ch);
      long long      getChannelHits(u_short ch, std::array<uint32_t,3564> & chHits);      
      unsigned int   getOrbitCounts(unsigned int &status1, unsigned int &status2);

      void setBcid4Waveform  (unsigned int   bcid);
      void setChOrbitDelay   (unsigned short delay);
      void setThresholds     (unsigned short value);
      void setFineDelay      (unsigned short fpga, unsigned short value);
      void setChHitFineDelay (unsigned short ch,   unsigned short value) ;
      void setChThreshold    (unsigned short fpga, unsigned short channelMask);
      void setScopeChannel   (unsigned short fpga, unsigned short channelMask);
      void setLumiChannel    (unsigned short fpga, unsigned short channelMask);
      void setFadcGain       (unsigned short fpga, unsigned short gain);

      void checkBusError();
      void checkClock();
      void dumpFADC(unsigned int ich);
      void waveFormEnable();

      unsigned short move2ExternalClock();

      bool FpgaChSEU(unsigned short fpga);
      bool FpgaVSEU();
      void cleanupAndRestartFpgaCh(unsigned short fpga);
      void cleanupAndRestartFpgaV();
      void reloadFpgaChFW(unsigned short chfpga);
      void reloadFpgaVFW();
      void configureFpgaCh(unsigned short const ifpga, unsigned short scopeEnabledChannelMask, unsigned short lumiEnabledChannelMask);
      void configureFpgaV (unsigned short const fpgaChannelMask);

    private:

      DFCountedPointer<Config> m_configuration;

      void control_bit_on (u_short ifpga, u_short pos);
      void control_bit_off(u_short ifpga, u_short pos);
 
      // VMEbus parameters of a module
      unsigned int m_vmeAddress;
      unsigned int m_vmeWindowSize;
      unsigned int m_vmeAddressModifier;
      u_long       m_vmeVirtualPtr;
      int          m_vmeScHandle;

      volatile LUCRODBoard_regs_t * m_vLUCROD;

      bool m_running;

      std::string                m_boardName;    
      std::vector< std::string > m_inputNames;
      std::string                m_hitAndChargeGroups;        

      unsigned int   m_nFifoChanges;
      unsigned short m_chOrbitDelay;
      unsigned short m_waveFormBcid;
      unsigned short m_chFpgaFwVersion; 
      unsigned short m_mainFpgaFwVersion; 
      bool           m_internalClock;
      bool           m_resetBoard;
      unsigned short m_postTrigSamples;
      unsigned short m_outCopyGain;      
      unsigned short m_outCopyOffset;      
      unsigned short m_baselineBcidStart;    
      unsigned short m_baselineLength;        
     
      unsigned short m_fadcOffsets    [NLUCRODIN];      
      unsigned short m_fadcThresholds [NLUCRODIN];      
      unsigned short m_channelBaseline[NLUCRODIN];      
      unsigned short m_hitDelayFine   [NLUCRODIN];      
      unsigned short m_accDelayFine   [NCHFPGA];      
      unsigned short m_coarseDelays   [NCHFPGA];      
      unsigned short m_fadcGains      [NCHFPGA];      
      unsigned short m_scopeEnabled; 

      void globalCommand    (unsigned short code);
      void testGlobalCommand(unsigned short code);
      
      std::array<bool, NCHFPGA> m_ignoreChError;
      bool m_ignoreVmeError;
      bool m_ignoreMainError;
    };
}
#endif // LUCRODBOARD_H
