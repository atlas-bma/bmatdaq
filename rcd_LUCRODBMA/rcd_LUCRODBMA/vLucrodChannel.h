#ifndef _VLUCRODCH_H
#define _VLUCRODCH_H

//
// channel-FPGA register map
//

struct LUCRODChannel_regs_t {
  unsigned short flashLSB;              //base + 0x000
  unsigned short flashMSB;              //base + 0x002
  unsigned short flashData;             //base + 0x004
  unsigned short version;               //base + 0x006  R/W
  unsigned short status;                //base + 0x008  R
  unsigned short waveForm0_fifo;        //base + 0x00a  R
  unsigned short control;               //base + 0x0c  RW
  unsigned short oneOrbitCharge_fifo;   //base + 0x0e  R
  unsigned short debug;                 //base + 0x10  R
  unsigned short fadc_read;             //base + 0x12  R
  unsigned short flashStatus;           //base + 0x14  R/W
  unsigned short waveForm1_fifo;        //base + 0x16  R/W
  unsigned short waveFormBcid;          //base + 0x18  R/W
  unsigned short coarseDelays;          //base + 0x1a  R/W
  unsigned short clock_adjust;          //base + 0x1c  R/W
  unsigned short oneBCIDCharge;         //base + 0x1e  R/W
  unsigned short accDelayFine;          //base + 0x20  R/W
  unsigned short lumiBlockCharge;       //base + 0x22  R
  unsigned short control2;              //base + 0x24  R/W
  unsigned short orbitLSW;              //base + 0x26  R
  unsigned short orbitMSW;              //base + 0x28  R   
  unsigned short baselineDef;           //base + 0x2A  R/W
  unsigned short ch0Baseline;           //base + 0x2c  R
  unsigned short ch1Baseline;           //base + 0x2E  R
  unsigned short hitsCh0_fifo;          //base + 0x30  R
  unsigned short ch0FadcThreshold;      //base + 0x32  R/W
  unsigned short hitsCh1_fifo;          //base + 0x34  R
  unsigned short ch1FadcThreshold;      //base + 0x36  R/W 
  unsigned short selfTrigger0Time;      //base + 0x38
  unsigned short fixBaselineCh0;        //base + 0x3a
  unsigned short selfTrig;              //base + 0x3c
  unsigned short postTrigSamples;       //base + 0x3e
  unsigned short scope0_fifo;           //base + 0x40
  unsigned short fixBaselineCh1;        //base + 0x42
  unsigned short scope1_fifo;           //base + 0x44
  unsigned short selfTrigger1Time;      //base + 0x46  
  unsigned short dummy5[4];             //base + 0x44
  unsigned short ch0FadcOffset;         //base + 0x50  R/W
  unsigned short ch0FadcGain;           //base + 0x52  R/W
  unsigned short ch0OutOffset;          //base + 0x54  R/W
  unsigned short ch0OutGain;            //base + 0x56  R/W
  unsigned short ch1FadcOffset;         //base + 0x58  R/W
  unsigned short ch1FadcGain;           //base + 0x5a  R/W
  unsigned short ch1OutOffset;          //base + 0x5c  R/W
  unsigned short ch1OutGain;            //base + 0x5e  R/W
  unsigned short LumatHitDelay;         //base + 0x60  R/W
  unsigned short dynThreshOffset;       //base + 0x62  R/W
  unsigned short instEvtLSB;            //base + 0x64  R/W
  unsigned short instEvtMSB;            //base + 0x66  R/W
  unsigned short instOrbitLSB;          //base + 0x68  R/W
  unsigned short instOrbitMSB;          //base + 0x6a  R/W
  unsigned short dummy6[74];            //Base + 0x62-0xff  R/W

  void setFadcGain(unsigned short gain) volatile {

    ch0FadcGain = gain & 0xffff;
    ch1FadcGain = gain & 0xffff;
  };
  
};

#endif
