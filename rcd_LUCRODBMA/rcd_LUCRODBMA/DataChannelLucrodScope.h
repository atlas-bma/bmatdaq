/********************************************************/
/*							*/
/* Date: October 2014   				*/ 
/* Author: Carla Sbarra					*/
/*							*/
/***                                                   **/

#ifndef DATACHANNELLUCRODSCOPE_H
#define DATACHANNELLUCRODSCOPE_H

#include "rcd_LUCRODBMA/SampleCollection.h"
#include "ROSCore/SingleFragmentDataChannel.h"
//#include <boost/thread/mutex.hpp>

namespace ROS 
{
  class DataChannelLucrodScope : public SingleFragmentDataChannel 
  {
  public:    
    DataChannelLucrodScope(u_int id, u_int configId, u_int rolPhysicalAddress, u_long vmeBaseVA, 
			   DFCountedPointer<Config> configuration, 
			   sampleCollection& collection, u_short enabledChannelMask );    
    virtual ~DataChannelLucrodScope() ;
    virtual int getNextFragment(u_int* buffer, int max_size, u_int* status, unsigned long pciAddress = 0);
    virtual DFCountedPointer < Config > getInfo();
      
  private:
    volatile LUCRODBoard_regs_t * m_vLUCROD;
    u_int m_max_wait;
    u_int m_min_wait;
    u_int m_channelId;
    u_int m_channel_number;  
    sampleCollection& m_sampleCollection;

    u_int   m_nchannels;  
    u_short m_enabledChannelMask;  

    enum Statuswords 
    {
      S_OK      = 0,
      S_TIMEOUT = 1,
      S_OVERFLOW,
      S_NODATA
    };
  };
}
#endif //DATACHANNELLUCRODSCOPE_H
