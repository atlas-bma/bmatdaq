#ifndef _VLUCRODVME_H
#define _VLUCRODVME_H

//
// VME-FPGA register map
//

struct LUCRODVme_regs_t {
  unsigned int version;               // base + 0x00
  unsigned int debug;                 // base + 0x04
  unsigned int control;               // base + 0x08
  unsigned int dummy1[2];             // base + 0x0c
  unsigned int flashLSB;              // base + 0x14
  unsigned int flashMSB;              // base + 0x18
  unsigned int flashData;             // base + 0x1c
  unsigned int flashStatus;           // base + 0x20
  unsigned int phasck;                // base + 0x24
  unsigned int vmeMMA;                // base + 0x28
  unsigned int vmeMMD;                // base + 0x2c
  unsigned int status;                // base + 0x30
  unsigned int orbitCount1;           // base + 0x34
  unsigned int chargeSum1;            // base + 0x38
  unsigned int chargeDbg1;            // base + 0x3c
  unsigned int serialDebug0;          // base + 0x40  
  unsigned int serialDebug1;          // base + 0x44
  unsigned int serialDebug2;          // base + 0x48
  unsigned int serialDebug3;          // base + 0x4c
  unsigned int serialDebug4;          // base + 0x50
  unsigned int serialDebug5;          // base + 0x54
  unsigned int serialDebug6;          // base + 0x58
  unsigned int serialDebug7;          // base + 0x5c
  unsigned int chargeSum2;            // base + 0x60
  unsigned int orbitCount2;           // base + 0x64
  unsigned int chargeDbg2;            // base + 0x68
  unsigned int orbitDelay;            // base + 0x6c
  unsigned int localQSet;             // base + 0x70
  unsigned int localLB;               // base + 0x74
  unsigned int localQ1LSB;             // base + 0x78
  unsigned int localQ1MSB;             // base + 0x7C
  unsigned int localQ2LSB;             // base + 0x80
  unsigned int localQ2MSB;             // base + 0x84

  unsigned int dummy2[30];            // base + 0x88-0xfc
};

#endif
