/********************************************************/
/*							*/
/* Date:   7 February 2008				*/ 
/* Author: C. Sbarra    				*/
/*							*/
#ifndef MODULELUCRODBMA_H
#define MODULELUCRODBMA_H

#include <string>
#include <vector>
#include <boost/thread/mutex.hpp>

#include "ROSCore/ReadoutModule.h"
#include "rc/RunParams.h"
#include "TTCInfo/LumiBlock.h"

// IPC Headers
#include <ipc/core.h>
#include <ipc/partition.h>

// IS headers
#include <is/info.h>
#include <is/inforeceiver.h>

// OH histograms
#include <oh/OHRootProvider.h>

#include "rcd_LUCRODBMA/LucrodBoard.h"
#include "rcd_LUCRODBMA/LucrodHistos.h"
#include "rcd_LUCRODBMA/SampleCollection.h"
#include "rcd_LUCRODBMA/LucrodISData.h"

namespace ROS 
{
  class ReadoutModuleLucrodBMA : public ReadoutModule
    {
    public:
      virtual void setup(DFCountedPointer<Config> configuration);

      virtual void configure    (const daq::rc::TransitionCmd&);
      virtual void prepareForRun(const daq::rc::TransitionCmd&);
      virtual void stopDC       (const daq::rc::TransitionCmd&);
      virtual void unconfigure  (const daq::rc::TransitionCmd&);
      virtual void connect      (const daq::rc::TransitionCmd&);
      virtual void disconnect   (const daq::rc::TransitionCmd&);
      virtual void publish();
      virtual void clearInfo() {};

      virtual void user(const daq::rc::UserCmd& command);
      virtual DFCountedPointer < Config > getInfo();

      ReadoutModuleLucrodBMA();
      virtual ~ReadoutModuleLucrodBMA() noexcept;

      virtual const std::vector<DataChannel *> *channels(); 

    private:

      std::vector<DataChannel *> m_dataChannels; 
      DFCountedPointer<Config>   m_configuration;

      //deduce masks from inputs
      void setEnabledChannelMask();
      void setFpgaChannelMask();
      void storeChannelInEachGroup();

      //detect and handle SEU 
      void checkSEU();

      // LB change callbacks
      void callback_LB  (ISCallbackInfo* iscb);    
      void callback_ATLASLB  (ISCallbackInfo* iscb);    
      void handleEndOfLB(u_int LumiBlock);    

      // IS and OH
      void initializeIsData();
      std::vector<double> arrayToVector(std::array<double,   NBCID> const & data);
      std::vector<double> arrayToVector(std::array<uint32_t, NBCID> const & data);
      void publishBaselineInfo();
      void prepareSensorInfo();
      void publishLucrodInfo();
      void publishRawLumiPerBunchInfo(u_int LB);
      void handleRawLumiPerBunchInfo(unsigned int lumiBlock, std::string IsInfoName, std::vector<double> const& counts, unsigned int status, unsigned int numberOfSensors);

      // Board identity and inputs
      std::string                 m_boardName;
      std::vector< std::string >  m_inputNames;
      std::vector< unsigned int > m_inputGroupIndeces;

      std::vector< std::string > m_Group1Names;
      std::vector< std::string > m_Group2Names;

      std::vector<int > m_Group1Fpga;
      std::vector<int > m_Group2Fpga;

      unsigned short m_Bi207Mask;
      unsigned short m_lumiEnabledChannelMask;
      unsigned short m_scopeEnabledChannelMask;
      unsigned short m_fpgaChannelMask;
      unsigned short m_waveFormBcid;      
      unsigned short m_scopeEnabled;

      std::vector<int> m_grp1Ch;
      std::vector<int> m_grp2Ch;

      //Latency checks
      bool m_internalClock;

      //classes
      LucrodBoard  m_lucrodBoard;
      LucrodHistos m_lucrodHistos;
 
      // IS and OH servers
      std::string m_histo_server;
      std::string m_is_server;

      //DataChannel, if any
      unsigned int m_id;
      unsigned int m_rolPhysicalAddress;

      // Comunication Interfaces
      std::string  m_partitionName;
      std::string  m_LumiBlockProviderName;
      IPCPartition    m_ipcpartition;
      ISInfoReceiver* m_is_receiver;
      ISInfoReceiver* m_is_receiverInitial;
      ISInfoReceiver* m_is_receiverAtlas;
      OHRootProvider* m_oh_provider;
 
      // Record Statuses for IS publications 
      unsigned int m_fpgaVSEU;      
      unsigned int m_fpgaChSEU[8];
      unsigned int m_group1Status;
      unsigned int m_group2Status;
      
       // miscellanea
      unsigned int m_runNumber;
      unsigned int m_lumiBlock;
      unsigned int m_atlasLumiBlock;
      std::string m_runType;
      bool m_setupDone;
      bool m_configured;
      bool m_connected;
      bool m_runStarted;
      bool m_AtlasAlive;
      boost::mutex m_mutex;
      sampleCollection m_sampleCollection;

      // LB-data containers, to be published to IS
      LucrodIsData m_isData;
    };

  inline const std::vector<DataChannel *> *ReadoutModuleLucrodBMA::channels() { return &m_dataChannels; }
}
#endif // READOUTMODULELUCRODBMA_H
