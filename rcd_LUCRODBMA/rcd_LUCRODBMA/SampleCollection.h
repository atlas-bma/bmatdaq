#ifndef MSAMPLECOLLECTION_H
#define MSAMPLECOLLECTION_H

#define NLUCRODIN 16
#define NHDATA    64
#define NBCID     3564

struct sampleCollection{
  double sum [NLUCRODIN][NHDATA];
  double sum2[NLUCRODIN][NHDATA];
  int numberOfEvents;
};

#endif // MSAMPLECOLLECTION_H
