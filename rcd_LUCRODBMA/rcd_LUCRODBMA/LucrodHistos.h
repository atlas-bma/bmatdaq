#ifndef LUCRODHISTOS_H
#define LUCRODHISTOS_H

#include <string>
#include <vector>

#include "ROSCore/ReadoutModule.h"
#include "rc/RunParams.h"
#include "rcd_LUCRODBMA/vLucrod.h"
#include "rcd_LUCRODBMA/SampleCollection.h"

// IPC Headers
#include <ipc/core.h>
#include <ipc/partition.h>

// OH histograms
#include "oh/OHRootProvider.h"
#include <TH1D.h>

namespace ROS 
{
  class LucrodHistos 
    {
    public:

      LucrodHistos(sampleCollection& m_sampleCollection);
      virtual ~LucrodHistos();

      void setup(DFCountedPointer<Config> configuration, unsigned short chLumiMask, unsigned short chWaveformMask, unsigned short fpgaMask);
      void connect(OHRootProvider* oh_provider);
      void prepareForRun();

      // histogram operations
      void bookHistos();
      void resetAccumulators();       
      void resetSoRHistos();       
      void resetTimeHistos();       
      void resetLBHistos();       
      void resetLocalHistos();       

      void fillChWaveFormHisto         (int ich, unsigned short* data0, unsigned short* data1);   
      void fillOrbitHisto              (int ibin, unsigned int orbit);
      void fillChLBHitHistos           (int ich, std::array<uint32_t, NBCID> const & counts);
      void fillChTimeHitHistos         (int ich, int ibin, long long totHits);

      void fillLBtimeHisto      (double time);
      void fillChBaselineHistos (int ich, double base);
      void fillChThresholdHistos(int ich, double thresh);
      void fillDataChannelHistos();
 
      void publishLBSumHistos (u_int lbid, std::string name, TH1D* histo);
      void publishLBHistos    (u_int lbid);
      void publishTimeHistos  (int version);
      void publishExpertHistos();

      double getBaseMean(int ich) { return hChBaseline[ich]->GetMean(); }
      double getBaseRms (int ich) { return hChBaseline[ich]->GetRMS();  }

      void updateGroupIndeces(unsigned short chMask, unsigned short fpgaMask, std::vector<unsigned int> indeces);

    private:

      DFCountedPointer<Config> m_configuration;

      std::unique_ptr<TH1D> bookTimeHisto(std::string name, std::string label);
      std::unique_ptr<TH1D> bookBcidHisto(std::string name, std::string label);

      unsigned short m_scopeEnabledChannelMask;
      unsigned short m_lumiEnabledChannelMask;
      unsigned short m_fpgaMask;

      // Board identity and inputs
      std::string                 m_boardName;
      std::vector< std::string >  m_inputNames;
      std::vector< unsigned int > m_inputGroupIndeces;

      // Comunication Interfaces
      OHRootProvider* m_oh_provider;

      // channel histos
      std::unique_ptr<TH1D> hChBcidHits  [NLUCRODIN];
      std::unique_ptr<TH1D> hChHits      [NLUCRODIN];
      std::unique_ptr<TH1D> hChBaseline  [NLUCRODIN];
      std::unique_ptr<TH1D> hDataChannel [NLUCRODIN];
      std::unique_ptr<TH1D> hChThresholds[NLUCRODIN];
      
      // Common histos 
      std::unique_ptr<TH1D> hOrbit;
      std::unique_ptr<TH1D> hLBtime;
      
      //accumulators for DataChannelHistos
      sampleCollection& m_sampleCollection;

      // private methods
      int               getFpgaChannel(int ifpga);
      std::string const getFpgaGroup  (int ifpga);
    };
}
#endif // LUCRODHISTOS_H
