#ifndef _VLUCROD_H
#define _VLUCROD_H

#include "rcd_LUCRODBMA/vLucrodChannel.h"
#include "rcd_LUCRODBMA/vLucrodMain.h"
#include "rcd_LUCRODBMA/vLucrodVME.h"

// There are 8 FPGA, each interfaced with a pair of input channels;
// the VME addresses are 0x000-0xff, 
//                       0x100-0x1ff, 
//                       0x200-0x2ff, 
//                       0x300-0x3ff, 
//                       0x400-0x4ff, 
//                       0x500-0x5ff, 
//                       0x600-0x6ff, 
//                       0x700 0x7ff 
// Than an FPGA for VME transfer at 32 bits, address 0xE00-0xEff
// The main FPGA receiving data from all input channels at address 0xF00

const int NCHFPGA   = 8;
const int NLUCRODIN = 16;
const int NHDATA    = 64;

struct LUCRODBoard_regs_t{
  // channels
  LUCRODChannel_regs_t CHRegs[NCHFPGA];
  // filler  0x800-0xdfc
  unsigned int dummy[384];
  // 0xE00 - fpga for 32-bit VME transfer
  LUCRODVme_regs_t VMERegs;
  //0xF00 - main fpga
  LUCRODMain_regs_t MainRegs;
};

#endif
