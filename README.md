**Software to handle BMA TDAQ: custom VME board configuration, monitoring of local stream**
<pre>
ssh -Y atlasgw.cern.ch
source /det/tdaq/scripts/setup_devel_11-02-01.sh
git clone ssh://git@gitlab.cern.ch:7999/atlas-bma/bmatdaq.git
cd bmatdaq
cmake_config
cd x86_64-el9-gcc13-opt
make install
</pre>
